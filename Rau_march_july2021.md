1. In the budget of 2021-22, Government of India has put forward the proposal of establishing National Bank for Financing Infrastructure and Development (NBFID). The capital base of NBFID will be Rs. 20,000 crores and will have a lending target of Rs. 5 lakh crores over the next three years. ([00:00](https://youtube.com/watch?v=9Kiq5d8tMHY&t=0))

#DevelopmentBank #Budget2021 #CurrentAffairs



Video - https://youtu.be/KfmZdTpd3RA

Presented by: Jatin Bhardwaj,  

#UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner. 

►DOWNLOAD NOTES
http://bit.ly/IE-explained-APR-W3-Word
http://bit.ly/IE-explained-APR-W3-PDF 

►WEEKLY EXPLAINED ANALYSIS
1. How big a concern is Facebook data leak? – national security and international affairs – ([01:20](https://youtube.com/watch?v=z8T44YKW1VI&t=80))
2. Where's the economy headed? – Indian economy – ([10:06](https://youtube.com/watch?v=z8T44YKW1VI&t=606)) 
3. On refugees and illegal immigrants, how India's stance changes with circumstances – International affairs and National security – ([20:30](https://youtube.com/watch?v=z8T44YKW1VI&t=1230)) 
4. Rupee hits 9-month low: why is it falling, what to expect in near future – Indian economy – ([30:45](https://youtube.com/watch?v=z8T44YKW1VI&t=1845))


#Facebookissue #dataleak #nationalsecurity #GDP #refugeecrises #illegalimmigrants #rohingyas #UN #nonrefoulment #foreigneract #foreignexchange #depreciation



Video - https://youtu.be/KfmZdTpd3RA

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


  


Presented by: Jatin Bhardwaj,  
#UPSC​​ #Currentaffairs​​ #IndianExpressExplained​​ #ExplainedAnalysis​​
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.  


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass:    
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

► EXPLAINED NOTES 
PDF - http://bit.ly/IE-EXPLAINED-APRIL-WEEK1-PDF 
WORD - http://bit.ly/IE-EXPLAINED-APRIL-WEEK1-WORD

►WEEKLY EXPLAINED ANALYSIS
1. India-Pakistan trade tussle – (international relation and trade)- ([0:26](https://youtube.com/watch?v=jMKBSETm5MY&t=26))
2. Chinese encroachment in Philippines EEZ - (international relation) – ([5:13](https://youtube.com/watch?v=jMKBSETm5MY&t=313))
3. Frequent forest fires in India – (geography and environment) – ([8:38](https://youtube.com/watch?v=jMKBSETm5MY&t=518))
4. Relevance of Article 244 (A) in Assam – (Environment) – ([14:58](https://youtube.com/watch?v=jMKBSETm5MY&t=898))
#indiapakistantradetussle #chinainphilippineseez #indiaforestfire #article244A 



Video - https://youtu.be/KfmZdTpd3RA

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


  


Presented by: Faizan Khan,  

#UPSC​ #Currentaffairs​ #IndianExpressExplained​ #ExplainedAnalysis​
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.      

►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-...​
►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy...​

►DOWNLOAD DNS NOTES

PDF - http://bit.ly/IE-EXPLAINED-APRIL-WEEK...​
WORD - http://bit.ly/IE-EXPLAINED-APRIL-W2-WORD​

►WEEKLY EXPLAINED ANALYSIS

1. Pre-pack: Insolvency resolution for MSMEs – (Indian Economy) – ([00:37](https://youtube.com/watch?v=l3WztnrQTi4&t=37))
2. Securities acquisition plan for market boost - (Indian Economy) – (18:02​​)
3. Biden’s radical tax proposal – (International Relation) + Economy) – (23:18​​)
4. Speed Test - (29:37​)

# Pre-packInsolvancyResolution #G-SAP​ #CorporateTax



Video - https://youtu.be/KfmZdTpd3RA​

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1​
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2​
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3​
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4​

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-...​


​  



​

Presented by: Jatin Bhardwaj,  
#UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.  

PDF - http://bit.ly/IE-Explained-APR-W4-PDF
Word - http://bit.ly/IE-Explained-APR-W4-Word

►WEEKLY EXPLAINED ANALYSIS
1. Why did Perseverance produce oxygen on Mars? - (Science and Technology) - ([00:47](https://youtube.com/watch?v=-r0MIaCq4Vg&t=47))
2. In Assam earthquake, reminder of seismic hazard along HFT Fault-line - (Environment and Geography) - ([14:23](https://youtube.com/watch?v=-r0MIaCq4Vg&t=863))



Video - https://youtu.be/KfmZdTpd3RA

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS
GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


  


Presented by: Faizan Khan,  

#UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.      

►WEEKLY EXPLAINED ANALYSIS

1. What is NITI Aayog’s draft national policy on migrant workers? – (Polity & Governance) – ([00:35](https://youtube.com/watch?v=j0QicnpdAys&t=35))
2. What rise in bond yield means for investors and government - (Economy) – ([19:18](https://youtube.com/watch?v=j0QicnpdAys&t=1158))
3. Imran Khan in Sri Lanka: Multiple facets of a relationship India is watching
 – (International Relation) – ([31:24](https://youtube.com/watch?v=j0QicnpdAys&t=1884))
4. The caracal, a favorite of royals, now critically endangered – Risks, Solutions – (Environment & Ecology) – ([35:44](https://youtube.com/watch?v=j0QicnpdAys&t=2144))
5. What is COVAX, the scheme to distribute Covid-19 vaccines around the world?  – (International Relation) – ([43:54](https://youtube.com/watch?v=j0QicnpdAys&t=2634))

# NationalPolicyOnMigrant workers #BondYield #Pak&SriLanka #Caracal #COVAX



Video - https://youtu.be/KfmZdTpd3RA

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


  


Presented by: Jatin Bhardwaj,  
#UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.  

►DOWNLOAD NOTES
WORD: https://bit.ly/Explained-July-week1-Word
PDF: https://bit.ly/Explained-July-week1-PDF

►WEEKLY EXPLAINED ANALYSIS
1. Why fuel prices are high – (Indian economy and trade) – ([00:41](https://youtube.com/watch?v=UTvz6_tnGj8&t=41))
2. Political aspirations in Leh and Kargil – (Indian polity and governance) – ([13:28](https://youtube.com/watch?v=UTvz6_tnGj8&t=808))
3. Pregnancy and Pandemic cure – (Science and technology) – ([26:42](https://youtube.com/watch?v=UTvz6_tnGj8&t=1602))


#highfuelprices #petrolprices #dieselprices #fuel@100 #Leh #ladakh #kargil #article35 #article370 #unionterritoryofladakh #pregnancyandpandemic #vaccinationtopregnantwome




Video - https://youtu.be/KfmZdTpd3RA

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


  


Presented by: Mr Faizan Khan,  #UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis 

In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner. 

►WEEKLY EXPLAINED NOTES 
PDF - http://bit.ly/Explained-June-Week1-pdf
Word - http://bit.ly/Explained-June-Week1-Word

►WEEKLY EXPLAINED ANALYSIS 
1. What are the two missions that NASA has selected for Venus Exploration? – (Science & technology) – ([00:41](https://youtube.com/watch?v=PM7aAk_wnjw&t=41)) 
2. What puts Lions and Tigers at coronavirus risk?  – (Science & technology) – ([24:05](https://youtube.com/watch?v=PM7aAk_wnjw&t=1445))  
3. How Indians see the economy   – (Economy) – ([30:22](https://youtube.com/watch?v=PM7aAk_wnjw&t=1822)) 
4. Speed Test - ([35:25](https://youtube.com/watch?v=PM7aAk_wnjw&t=2125))

#ACE-2 protein and Coronavirus risk #Consumer Confidence Survey   #Venus Exploration 

/IAS - ELEARN



 

, we will take up important articles from Indian Express Explained section in a weekly manner. 

►WEEKLY EXPLAINED NOTES 
PDF - http://bit.ly/EXPLAINED-May-Week4-PDF
Word - http://bit.ly/EXPLAINED-May-Week4-Word

►WEEKLY EXPLAINED ANALYSIS 
1. Central Bank Digital currency - various associated issues – (Economy) – ([02:35](https://youtube.com/watch?v=PM7aAk_wnjw&t=155)) 
2. Social Media and Safe Harbour – (Polity & Governance) – ([36:15](https://youtube.com/watch?v=PM7aAk_wnjw&t=2175))  
3. Rising prices of edible oil – (Economy) – ([44:47](https://youtube.com/watch?v=PM7aAk_wnjw&t=2687)) 

#DigitalCurrency #Blockchain #crptocurrency #Safeharbour #edibeloil #vegetableoil #CentralBanks #Distributedledger #Money  

/IAS - ELEARN



 

Presented by: Jatin Bhardwaj,  
#UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.  

►DOWNLOAD NOTES
PDF - http://bit.ly/EXPLAINED-JUNE-WEEK2-PDF
Word - http://bit.ly/EXPLAINED-JUNE-WEEK2-WORD

►WEEKLY EXPLAINED ANALYSIS
1. New drug for Alzheimer’s disease– (Health and Biotechnology) – ([00:25](https://youtube.com/watch?v=KUGWjEEdc_E&t=25))
2. What to watch out for at G7 meet – (International relations) – ([07:00](https://youtube.com/watch?v=KUGWjEEdc_E&t=420))
3. Tigray region of Ethiopia – (Geography and Maps) – ([15:43](https://youtube.com/watch?v=KUGWjEEdc_E&t=943))
4. How indicative is GDP – (Economy) – ([21:58](https://youtube.com/watch?v=KUGWjEEdc_E&t=1318))


#Alzheimer #dementia #G7 #historyofG7 #indiaandG7 #Tigray #ethiopia #gdpbenefits #gdpdata #gdpofindia #mapofafrica #mapofasia #hornofafrica




Video - https://youtu.be/KfmZdTpd3RA

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


  


Presented by: Basava Uppin,  

#UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.  

►DOWNLOAD NOTES
PDF Link: https://bit.ly/Explained-June-Week3-PDF
Word Link: https://bit.ly/Explained-June-Week3-Word

►WEEKLY EXPLAINED ANALYSIS
1. Impact of US Fed Bank's Policy on Indian Economy - Economy ([0:29](https://youtube.com/watch?v=BZtlvORWzTk&t=29))
   a. Factors affecting Bond Yields
   b. Reasons for increasing Yields and implications
2. Trends in production of Pulses - Economy ([23:35](https://youtube.com/watch?v=BZtlvORWzTk&t=1415))
3.  Tulu: Demand for Official language status - Polity & Governance ([34:13](https://youtube.com/watch?v=BZtlvORWzTk&t=2053))
4.  Election Petitions: What, Why and How? - Polity & Governance ([40:18](https://youtube.com/watch?v=BZtlvORWzTk&t=2418))

#BondYields #Pulses #OperationTwist #Tulu #ElectionPetitions 



Video - https://youtu.be/KfmZdTpd3RA

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS
GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


  


Presented by: Faizan Khan, 

#UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner. 


►DOWNLOAD NOTES 
PDF Link: https://bit.ly/Explained-June-Week4-PDF
Word Link: https://bit.ly/Explained-June-Week4-Word


►WEEKLY EXPLAINED ANALYSIS 
1. Countries that allow gender self-identification, and the law in India – Social Justice - ([0:39](https://youtube.com/watch?v=HXvZRzFNwh8&t=39)) 
2. How the pandemic has impacted household savings, deposits and debt - Economy - ([13:54](https://youtube.com/watch?v=HXvZRzFNwh8&t=834)) 
3. Will food become costlier - Economy - ([21:29](https://youtube.com/watch?v=HXvZRzFNwh8&t=1289)) 
4. Can a drone attack be prevented? – Security- ([27:10](https://youtube.com/watch?v=HXvZRzFNwh8&t=1630))

 #Self-determinationOfGenderIdentity #FinancialSaving #FoodInflation #AntiDroneSystems



Video - https://youtu.be/KfmZdTpd3RA 


►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS 
GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1 
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4 

/IAS – ELEARN



▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-... 



 



 

Presented by: Faizan Khan,  
#UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.      

PDF - http://bit.ly/IE-Explained-Week1-March-2021-PDF
WORD - http://bit.ly/IE-EXPLAINED-WEEK1-2021-WORD

►WEEKLY EXPLAINED ANALYSIS

1. ‘Lateral entry’ into bureaucracy: reason, process and the controversy – (Polity & Governance) – ([00:37](https://youtube.com/watch?v=IXDGcKVYEYo&t=37))
2. Haryana’s quota law comes as jobless curve in state rises and govt pie shrinks - (Polity & Governance) – ([16:55](https://youtube.com/watch?v=IXDGcKVYEYo&t=1015))
3. The debate over a Chinese divorce court ordering ‘housework compensation’ for a woman – (Polity & Governance) – ([31:00](https://youtube.com/watch?v=IXDGcKVYEYo&t=1860))
4. Pixxel India’s delayed flight to new space frontier – (Science & Technology) – ([43:22](https://youtube.com/watch?v=IXDGcKVYEYo&t=2602))

# LateralEntryInGovernance #LocalReservationInJobs #HouseholdWorkCompensation #PixxelIndia #PrivateParticipationInSpaceTechnology



Video - https://youtu.be/KfmZdTpd3RA

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


  


Presented by: Vaibhav Mishra,  

#UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.  

PDF Link: http://bit.ly/IE-Explained-Week2-March-2021-PDF
Word link: http://bit.ly/IE-Explained-Week2-March-2021-Word



►Pre–Pareekshan Registration Link - https://elearn.rauias.com/home-page/


*Q. Highlight the Central Asian and Greco-Bactrian elements in the Gandhara Art (10 Marks)*
https://www.youtube.com/watch?v=QWwAcojvNHc

►WEEKLY EXPLAINED ANALYSIS
1. What is 2001 FO32, the largest asteroid passing by Earth this year? - (Science and Technology) - ([00:47](https://youtube.com/watch?v=yz6fZY6xGCs&t=47))
2. What was the significance of Mahatma Gandhi’s Dandi March?  - (History, Art and Culture) - ([07:24](https://youtube.com/watch?v=yz6fZY6xGCs&t=444))
3. The legacy and return of the Bamiyan Buddhas, virtually - (History, Art and Culture) - ([21:07](https://youtube.com/watch?v=yz6fZY6xGCs&t=1267))
4. INS Karanj, the Scorpene-class submarine inducted into service - (Security) - ([25:50](https://youtube.com/watch?v=yz6fZY6xGCs&t=1550))
5. Why Himachal Pradesh wants to start seabuck thorn plantations - (Environment) - ([31:15](https://youtube.com/watch?v=yz6fZY6xGCs&t=1875))
6. Why the EU has been declared an ‘LGBTIQ Freedom Zone’ (Social Issues) - ([34:38](https://youtube.com/watch?v=yz6fZY6xGCs&t=2078))


#Asteroid #Dandi march #INS KARANJ #Seabuckthorn #LGBTIQFREEDOMZONE



Video - https://youtu.be/KfmZdTpd3RA

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


  


Presented by: Basava Uppin,  

#UPSC​ #Currentaffairs​ #IndianExpressExplained​ #ExplainedAnalysis​
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.  

PDF Link: http://bit.ly/IE-Explained-Week3-March-PDF
Word Link: http://bit.ly/IE-Explained-March-Week3-Word



►Pre–Pareekshan Registration Link - https://elearn.rauias.com/home-page/​


*Q. Highlight the Central Asian and Greco-Bactrian elements in the Gandhara Art (10 Marks)*
https://www.youtube.com/watch?v=QWwAc...​

►WEEKLY EXPLAINED ANALYSIS
1. SEBI relaxes valuation norms for AT1 bonds: reasons why, and likely impact  - (Economy) - ([00:27](https://youtube.com/watch?v=UnKN1TkyDLo&t=27))
2. Delhi’s ambitious plan to switch to electric vehicles - (Polity and Governance) - ([17:41](https://youtube.com/watch?v=UnKN1TkyDLo&t=1061))
3. Why House panel wants govt to implement one farm law despite protests - (Polity and governance) - ([25:18](https://youtube.com/watch?v=UnKN1TkyDLo&t=1518))
4. Anti-defection law: When a nominated MP loses Rajya Sabha membership March - (Polity and Governance) - ([33:58](https://youtube.com/watch?v=UnKN1TkyDLo&t=2038))
5. Centre versus state in Delhi – what is the latest issue? - (Polity and Governance) - ([40:20](https://youtube.com/watch?v=UnKN1TkyDLo&t=2420))



Video - https://youtu.be/KfmZdTpd3RA​

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1​
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2​
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3​
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4​

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-...​


​  



​

Presented by: Naweed Akhter,  

#UPSC​ #Currentaffairs​ #IndianExpressExplained​ #ExplainedAnalysis​
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.  

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass:    
http://bit.ly/PrelimsCompass-Budget-Survey2021

►PDF Link: http://bit.ly/Explained-Week4-March-PDF
►Word Link: http://bit.ly/Explained-Week4-March-Word 

►WEEKLY EXPLAINED ANALYSIS
1. Suez-Canal – (International Event) – ([00:48](https://youtube.com/watch?v=HSaBj3GzeD4&t=48))
(History of its construction, Suez War, 1956, Israel, Britain, France & Egypt)
2. EC’s new rule for Polling Agents – (Polity & Governance) – ([15:54](https://youtube.com/watch?v=HSaBj3GzeD4&t=954))
(Polling Agents, Duties and Responsibilities, Presiding Officer, Returning Officer)
3. Special Bond of Mizoram with Chin Community of Myanmar – (International Relations) – ([25:01](https://youtube.com/watch?v=HSaBj3GzeD4&t=1501))
(Chin Community, UN Refugee Convention 1951, Free Movement Regime)
4. Labour Force Participation Rate for Women declined – (Social Issues) – ([33:07](https://youtube.com/watch?v=HSaBj3GzeD4&t=1987))
(LFPR, NSO, MOSPI, Factors for decline, Initiatives of government)
5. Legacy of Anangpal II in Delhi History – History & Culture – ([41:01](https://youtube.com/watch?v=HSaBj3GzeD4&t=2461))



Video - https://youtu.be/KfmZdTpd3RA​

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1​
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2​
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3​
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4​

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-...​


​  



​

Presented by: Jatin Bhardwaj,  

#UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.  

►DOWNLOADS
http://bit.ly/IE-Explained-MAY-W1-2021-WORD
http://bit.ly/IE-Explained-MAY-W1-2021-PDF

►WEEKLY EXPLAINED ANALYSIS
1. Behind the clashes at Jerusalem’s Al-Aqsa – (International relation and History & Culture) - ([02:21](https://youtube.com/watch?v=Zwh06UN2oWI&t=141))
2. Why RBI wants moderate bond yields, and what it means for investors. (RBI & Economy) - ([13:14](https://youtube.com/watch?v=Zwh06UN2oWI&t=794))
3. As NASA’s OSIRIS-REx begins journey back from asteroid, the significance of its mission – (Science and Technology) - ([26:20](https://youtube.com/watch?v=Zwh06UN2oWI&t=1580))
4. ‘Black fungus’ in patients: what is the disease, treatment – (Public health, science and technology) - ([36:00](https://youtube.com/watch?v=Zwh06UN2oWI&t=2160))

#Jerusalem #Israel #Palestine #AlAQSA #Arabconflict #RBI #bondyield #corporatebonds #governmentbonds #governmentsecurities #OSIRISREx #Bennu #astroids #solarsystem #Blackfungus #fungus #NASA



Video - https://youtu.be/KfmZdTpd3RA

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


  


Presented by: Vaibhav Mishra,  
#UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner.  


►DOWNLOAD EXPLAINED NOTES
PDF: http://bit.ly/EXPLAINEDMAYWEEK2PDF
WORD: http://bit.ly/Explained-May-Week2-WORD

►WEEKLY EXPLAINED ANALYSIS
1.  PM-KISAN – (Economy) – ([02:23](https://youtube.com/watch?v=mbI6m0SgeLE&t=143))
2. Vienna Convention on Diplomatic Relations – (International Relations) – ([07:45](https://youtube.com/watch?v=mbI6m0SgeLE&t=465))
3. Israel's Iron dome – (Security) – ([15:57](https://youtube.com/watch?v=mbI6m0SgeLE&t=957))
4. 2-deoxy-D-glucose (2DG) drug – (Science and Technology) – ([21:42](https://youtube.com/watch?v=mbI6m0SgeLE&t=1302))

#PM-KISAN #Vienna #DIPLOAMTIC #IMMUNITY #IRON DOME #PALESTINE #ISRAEL 



Video - https://youtu.be/KfmZdTpd3RA

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


  


Presented by: Naweed Akhter,  #UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis 
In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner. 


►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD EXPLAINED NOTES 
PDF - http://bit.ly/EXPLAINED-May-Week3-PDF
WORD - http://bit.ly/Explained-May-Week3-Word
►WEEKLY EXPLAINED ANALYSIS 
1. E-way Bill Integration with FASTag – (Economy) – ([02:31](https://youtube.com/watch?v=C_oTlJ5wFfc&t=151)) 
2. How are tropical cyclones named – (Geography) – ([19:48](https://youtube.com/watch?v=C_oTlJ5wFfc&t=1188))  
3. Currency Chest & Money Supply – (Economy) – ([32:26](https://youtube.com/watch?v=C_oTlJ5wFfc&t=1946)) 
4. How UNESCO grants World Heritage Site tag – (Culture) – ([43:15](https://youtube.com/watch?v=C_oTlJ5wFfc&t=2595)) 
#EwayBill #FASTAG #RFID #NETC #Cyclones #WMO #RSMC #IMD #Galeforce #CurrencyChest #BroadMoney #NarrowMoney #FiatMoney #WorldHeritageCommittee #UNESCO #MarathaMilitaryArchitecture 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr Mangal Singh,  #UPSC #Currentaffairs #IndianExpressExplained #ExplainedAnalysis In this series of Indian Express Explained Current Affairs for UPSC, we will take up important articles from Indian Express Explained section in a weekly manner. 

►WEEKLY EXPLAINED NOTES 
PDF - http://bit.ly/EXPLAINED-May-Week4-PDF
Word - http://bit.ly/EXPLAINED-May-Week4-Word

►WEEKLY EXPLAINED ANALYSIS 
1. Central Bank Digital currency - various associated issues – (Economy) – ([02:35](https://youtube.com/watch?v=ddmpuNfqQec&t=155)) 
2. Social Media and Safe Harbour – (Polity & Governance) – ([36:15](https://youtube.com/watch?v=ddmpuNfqQec&t=2175))  
3. Rising prices of edible oil – (Economy) – ([44:47](https://youtube.com/watch?v=ddmpuNfqQec&t=2687)) 

#DigitalCurrency #Blockchain #crptocurrency #Safeharbour #edibeloil #vegetableoil #CentralBanks #Distributedledger #Money  

/IAS - ELEARN



 

1. Is president of India a rubber stamp _ Best Public Administration Coaching for UPSC _ Rau's IAS-0KqIxkjbIjI ([00:00](https://youtube.com/watch?v=0KqIxkjbIjI&t=0))
Presented by: Basava Uppin, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link – Mains Weekly Question
https://elearn.rauias.com/mains-daily-questions/highlight-the-major-concerns-and-issues-raised-by-developed-countries-with-respect-to-intellectual-p/6063106a9a7064553be3b4f5/
►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass:    
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS
GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-01-04-21-PDF
Word - http://bit.ly/DNS-NOTES-01-04-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Weekly Mains Assignment Question ([0:45](https://youtube.com/watch?v=0fJIYfRvQZw&t=45))
2. 15th Finance Commission Recommendations – 2nd Report (2021-26) – Polity & Governance ([01:43](https://youtube.com/watch?v=0fJIYfRvQZw&t=103))
       1. Need for Finance Commission
       2. Horizontal and Vertical Distribution of Taxes
       3. Defence Modernisation Fund
       4. NDRMF and SDRMF
       5. Grants to local Bodies- Analysis  
3.   Govt. sharply cuts rates on small savings instruments – Indian Economy  ([34:19](https://youtube.com/watch?v=0fJIYfRvQZw&t=2059))
4.  Government of NCT of Delhi (Amendment) Act, 2021 – Polity & Governance ([46:08](https://youtube.com/watch?v=0fJIYfRvQZw&t=2768))
5. Panel submits report on farm laws to SC- Indian Economy ([50:57](https://youtube.com/watch?v=0fJIYfRvQZw&t=3057))
6. World Bank's Forecasts on India's GDP growth – Indian Economy ([55:13](https://youtube.com/watch?v=0fJIYfRvQZw&t=3313))
7. Question for the day ([59:05](https://youtube.com/watch?v=0fJIYfRvQZw&t=3545))

#FinanceCommission #NSSF #Farmlaws


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1143/ 
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registratio... 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass:  


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-Notes-01-07-21-PDF
Word - https://bit.ly/DNS-Notes-01-07-21-Word
►TODAY’S THE HINDU ANALYSIS
1. DNS Weekly Answer Writing Announcement – ([0:55](https://youtube.com/watch?v=LYzdK2uAqRg&t=55))
2. India’s 1991 liberalisation leap and lessons for today – Economy – ([01:27](https://youtube.com/watch?v=LYzdK2uAqRg&t=87))
(1991 Liberalisation, Situation prior to Liberalisation, RCEP)
3. ‘Centre’s digital agri proposals raise concern’ – Economy – ([18:01](https://youtube.com/watch?v=LYzdK2uAqRg&t=1081))
(IDEA, India Digital Ecosystem of Agriculture, Concerns for Indian Agriculture)
4. Non-food bank credit growth eases to 5.9%: RBI – Economy – ([25:50](https://youtube.com/watch?v=LYzdK2uAqRg&t=1550))
(Loan by RBI, Food and Non-Food Credit)
5. Women’s Indian Association - Archives – History & Culture – ([27:39](https://youtube.com/watch?v=LYzdK2uAqRg&t=1659))
(Women Indian Association, Annie Besant, Sarojini Naidu, Franchise for Women)
6. Question for the Day – ([32:56](https://youtube.com/watch?v=LYzdK2uAqRg&t=1976))
#1991Reforms #TradeLiberalisation #ForeignExchange #Loan #ControlSystem #RCEP #IDEA #DigitalEcosystemAgriculture  #FoodCredit #NonFoodCredit #RBI WomenIndianAssociation

► Prelims Compass Link 
1. Indian Polity & Governance - Amazon Link: https://amzn.to/3gChrUJ

2. Science & Technology - Amazon Link: https://amzn.to/3q2lldH

3. Budget & Economic Survey - Amazon Link: https://amzn.to/3wys01Z

4. Government Schemes - Amazon Link: https://amzn.to/3q4jIfS

5. International Relations - Amazon Link: https://amzn.to/3gDvyJw

6. Indian Economy - Amazon Link: https://amzn.to/3qesmsh

7. History & Culture of India - Amazon Link: https://amzn.to/3iS2bpp

8. Environment, Ecology & Biodiversity - Amazon Link: https://amzn.to/2U3VIgI


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1230/
 /IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Vaibhav Mishra of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021
►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
WORD - http://bit.ly/DNS-NOTES-01-06-21-WORD
PDF - http://bit.ly/DNS-NOTES-01-06-21-PDF
 
►TODAY’S THE HINDU ANALYSIS
1. Recognising caste-based violence against women Page 07 - (Social Justice) - ([02:12](https://youtube.com/watch?v=tSMTiF1AKX4&t=132))
2. It’s time to deﬁne limits to sedition Page 01 - (Polity and Governance) - ([16:45](https://youtube.com/watch?v=tSMTiF1AKX4&t=1005))
3. The Indian model of coexistence Page 07 - (International Relations) - ([34:52](https://youtube.com/watch?v=tSMTiF1AKX4&t=2092))
4. Defence Ministry notiﬁes 108 negative imports Page 08 - (Security) - ([50:21](https://youtube.com/watch?v=tSMTiF1AKX4&t=3021))
5. A new born rhino with its mother at the Pobitora Wildlife Sanctuary in Morigaon district of Assam on Monday Page 01- (Environment) - ([52:47](https://youtube.com/watch?v=tSMTiF1AKX4&t=3167))
6. QOD - ([55:08](https://youtube.com/watch?v=tSMTiF1AKX4&t=3308))


#Atrocities #scheduledcaste #scheduledtribe #Preventionofatrocities #1989 #Sedition #kanhaiyakumar #Kedarnathsingh #Israel #plaestine #gaza #westbank #Negativelist #pobitora 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1200/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Faizan Khan + Basava Uppin + Vaibhav Mishra
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-01-03-2021-PDF
Word - http://bit.ly/DNS-Notes-01-03-2021-Word

►TODAY’S THE HINDU ANALYSIS
1. ISRO puts Brazil’s Amazonia-1, 18 other satellites into orbit – (International Relation + Science & Technology + Environment & Ecology) – ([00:38](https://youtube.com/watch?v=sXKHJuNFkXM&t=38))
    (India-Brazil relation; Earth Observation Satellite; Amazon Forest) 
2. PM calls for water conservation drive- (Environment & Ecology) – ([19:14](https://youtube.com/watch?v=sXKHJuNFkXM&t=1154))
(Water use Efficiency) 
3. Spurring a reawakening with National Science Day - (Science & Technology) - ([24:35](https://youtube.com/watch?v=sXKHJuNFkXM&t=1475))
(STI Policy)
4. More about Big Government than Big Tech (Reference) - (Polity & Governance) -([38:46](https://youtube.com/watch?v=sXKHJuNFkXM&t=2326))
5. Question for the day – (International Relation) – ([39:14](https://youtube.com/watch?v=sXKHJuNFkXM&t=2354))

#India-Brazil-Rrelation #EarthObservationSatellite #Amazon Forest #WaterUseEfficiency #STIPolicy


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-981/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mr. Basava Uppin and Mr. Vaibhav Mishra of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs
 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
WORD: http://bit.ly/DNS-NOTES-01-05-21-WORD
PDF: http://bit.ly/DNS-NOTES-01-05-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. Regulation of Drug Prices - (Economy) - ([01:20](https://youtube.com/watch?v=_-5BOn2dq98&t=80))
 a. Pharmaceutical Pricing Regime in India
 b. Exceptions to Patents under TRIPS
2. Critical Analysis of GST regime: Benefits and Challenges - (Economy) - ([24:55](https://youtube.com/watch?v=_-5BOn2dq98&t=1495))
3. Kyrgyzstan, Tajikistan seek to ease cross-border tensions - (Geography) - ([44:57](https://youtube.com/watch?v=_-5BOn2dq98&t=2697))
4. The rising sun in India-Japan relations - (International Relations) - ([46:56](https://youtube.com/watch?v=_-5BOn2dq98&t=2816))
5. March core sector sees rebound on base effect - (Economy) - ([49:53](https://youtube.com/watch?v=_-5BOn2dq98&t=2993))
6. QOD - ([52:55](https://youtube.com/watch?v=_-5BOn2dq98&t=3175))

#Drugprices #DGCA #DPCO #GST #Kok-Tash #indiajapan 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1170/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass:    
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-02-04-21-PDF
Word - http://bit.ly/DNS-NOTES-02-04-21-WORD
►TODAY’S THE HINDU ANALYSIS
1. An Act of colourable legislation – Polity & Governance – ([00:51](https://youtube.com/watch?v=jJneEjJYko0&t=51))
(i) Doctrine of Pith & Substance – (03:53)
(ii) Doctrine of Colourable Legislation – (05:34)
(iii) SC Judgments - Basic Structure – (07:38)
2. Public Enterprises Selection Board – Polity & Governance – ([21:15](https://youtube.com/watch?v=jJneEjJYko0&t=1275))
(PESB, Functions, Composition)
3. BIMSTEC meet skirts Myanmar violence – International Relations – ([27:06](https://youtube.com/watch?v=jJneEjJYko0&t=1626))
(Joint Initiatives taken under BIMSTEC, About BIMSTEC)
4. Question for the Day – ([31:49](https://youtube.com/watch?v=jJneEjJYko0&t=1909))
5. Summary – ([32:27](https://youtube.com/watch?v=jJneEjJYko0&t=1947))
#PithandSubstance #Doctrine #HarmoniousConstruction #ColourableLegislation #BasicStrcuture #ShankariPrasad #SajjanSingh #Golaknath #Kesavanandabharati #PESB #BIMSTEC 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1144/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Basava Uppin of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021  
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-NOTES-02-07-21-PDF
Word - https://bit.ly/DNS-NOTES-02-07-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. What lies ahead for IBC? – Indian Economy ([0:37](https://youtube.com/watch?v=JNRfDTyoCkM&t=37))
A. Working Mechanism of SARFAESI Act, 2002
B. Provisions of IBC, 2016
C. Hits and Misses of IBC 2016 
2. RBI's Financial Stability Report – Indian Economy ([31:09](https://youtube.com/watch?v=JNRfDTyoCkM&t=1869))
A. Terms related to Banking- NPA, PCR, SMA
B. BASEL III Norms and CAR
C. Banking Stability Indicator
3. ‘Framework for Inter State Water Disputes – Polity & Governance ([47:13](https://youtube.com/watch?v=JNRfDTyoCkM&t=2833))
4. Government of NCT Delhi (Amendment) Act, 2021 – Polity & Governance ([54:34](https://youtube.com/watch?v=JNRfDTyoCkM&t=3274))
     YouTube link: https://www.youtube.com/watch?v=LjeS42tEKJk&t=571s 
 7. Question for the Day ([55:06](https://youtube.com/watch?v=JNRfDTyoCkM&t=3306))

#IBC #SARFAESI #NPAs #TBS #BASELNorms #InterstateWater

 
https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1231/ 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 

#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
WORD - http://bit.ly/DNS-NOTES-02-06-21-WORD
PDF - http://bit.ly/DNS-NOTES-02-06-21-PDF


►TODAY’S THE HINDU ANALYSIS

1. Breaking the cycle of child labour is in India’s hands – Social Issues - ([00:37](https://youtube.com/watch?v=uunkHV8zr_0&t=37))
(Child Labour in India)
2. Tenuous revival – Indian Economy - ([12:21](https://youtube.com/watch?v=uunkHV8zr_0&t=741))
(GDP Estimates)
3. Indemnity of manufacturers (Lead article)  – Polity & Governance – ([22:26](https://youtube.com/watch?v=uunkHV8zr_0&t=1346))
(liability waiver)
4. The economic toolkit revealed – Indian Economy – ([26:51](https://youtube.com/watch?v=uunkHV8zr_0&t=1611))
(Monetary Policy Vs Fiscal Policy)
5. Question for the Day - (Polity & Governance) – ([29:54](https://youtube.com/watch?v=uunkHV8zr_0&t=1794))

#ChildlabourinIndia #GDPEstimates #IndemnityofManufacturers # MonetaryPolicyVsFiscalPolicy

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-02-03-21-PDF
Word - http://bit.ly/DNS-Notes-02-03-21-Word

►TODAY’S THE HINDU ANALYSIS
1. Chinese cyber attack foiled & Chinese may have targeted power systems, ports - US Firm – Security - ([00:41](https://youtube.com/watch?v=mcqCPD0QuAI&t=41))
 (Cyber Attack, Red Echo, ShadowPad, Impacts)
2. IIT-Delhi researchers develop tech to recycle e-waste – Science & technology – ([07:48](https://youtube.com/watch?v=mcqCPD0QuAI&t=468))
(E-Waste Recycling, Pyrolysis, Syngas, Valuable Metals)
3. The vital but delicate task of reviving the Iran Deal – International Relations – ([14:31](https://youtube.com/watch?v=mcqCPD0QuAI&t=871))
(Iran Nuclear Deal, JCPOA, INSTEX, Challenge for Biden)
4. US trade report flags issues from 'Make in India' policy – International Relations – ([24:23](https://youtube.com/watch?v=mcqCPD0QuAI&t=1463))
(Make in India, 2021 President’s Trade Agenda and 2020 Annual Report)
5. Question for the Day – ([27:26](https://youtube.com/watch?v=mcqCPD0QuAI&t=1646))
#Cyberattack #RedEcho #Mumbaipoweroutage #ShadowPad #Malware #E-Waste #Recycling #Pyrolysis #Syngas #JCPOA #IranNuclearAgreement #INSTEX #2021President’sTradeAgenda #2020AnnualReport


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1116/ 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Faizan Khan, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass:    
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA
 
Link for Prelims Compass: Budget and Economic Survey - https://www.amazon.in/Prelims-Compass-2021-Budget-Economic/dp/B09175Q9YJ/ref=sr_1_1?crid=VLC4RKVC9YT2&dchild=1&keywords=raus+compass+2021&qid=1617445501&sprefix=raus+%2Caps%2C740&sr=8-1

Link for QIP Registration - https://elearn.rauias.com/live-courses/qip-prelims-2021-revision-classes-for-prelims/60093fc9b5807b05134a154e/

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-03-04-21-PDF
Word - http://bit.ly/DNS-Notes-03-04-21-Word

►TODAY’S THE HINDU ANALYSIS
1. Compass & QIP announcement - ([00:20](https://youtube.com/watch?v=GFRnscqgDT0&t=20))
2. ‘Millet-based products set to gain increasing acceptance’– (GS-I +II + III) – ([01:11](https://youtube.com/watch?v=GFRnscqgDT0&t=71))
(Importance of millet)
3. Disquiet over policy for rare diseases– (Polity & Governance) – ([21:44](https://youtube.com/watch?v=GFRnscqgDT0&t=1304))
(National Policy on Rare Diseases)
4.First farm-based solar power plant comes up in Rajasthan– (Polity & Governance) – ([28:59](https://youtube.com/watch?v=GFRnscqgDT0&t=1739))
    (PN-KUSUM)) 
5. Question for the day – (Polity & Governance) – ([33:25](https://youtube.com/watch?v=GFRnscqgDT0&t=2005))

#ImportanceOfMillet #NationalPolicyOnRareDiseases #PM-KUSUM


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1107/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Description:
Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registratio... 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass:  


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-03-06-21-PDF
Word - http://bit.ly/DNS-NOTES-03-06-21-WORD
►TODAY’S THE HINDU ANALYSIS
1. DNS Mains Answer Writing – ([00:59](https://youtube.com/watch?v=JXw6H061hao&t=59))
2. Adverse Changes, Federalism Imperilled – Polity & Governance – ([01:42](https://youtube.com/watch?v=JXw6H061hao&t=102))
(Federalism, Indian Federalism, Increasing Centralisation, Fiscal Federalism)
3. Caste categories for NREGS Pay – Polity & Governance – ([22:12](https://youtube.com/watch?v=JXw6H061hao&t=1332))
(NREGS Payment, Concerns on bifurcation)
4. Raise Ethanol Blending in Surplus States – Environment – ([26:56](https://youtube.com/watch?v=JXw6H061hao&t=1616))
(Ethanol Blending, Target of 2023, Benefits of Blending)
5. Exports Surge 67% narrowing trade gap – Economy - ([31:28](https://youtube.com/watch?v=JXw6H061hao&t=1888))
(Merchandise Trade – 2020-21)
6. Question for the Day – ([34:47](https://youtube.com/watch?v=JXw6H061hao&t=2087))
#Federalsim #IndianFederalism #Article1 #SeventhSchedule # FiscalFederalism #NREGS #MGNREGA #EthanolBlending #MerchandiseTrade

https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1202/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Pallavi Sarda, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-03-03-21-PDF
Word - http://bit.ly/DNS-NOTES-03-03-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Recalibrating relations with EU - (International Relations) – ([00:35](https://youtube.com/watch?v=Kni-qQf9cac&t=35))
2. India and Japan back in new Sri Lanka port project  - (International Relations) - ([17:06](https://youtube.com/watch?v=Kni-qQf9cac&t=1026)) 
(Decentralization)
3. Reviving the Iran deal  - (International Relations) - ([23:55](https://youtube.com/watch?v=Kni-qQf9cac&t=1435))
4. Himalayan serow spotted in Assam - (Environment) - ([31:25](https://youtube.com/watch?v=Kni-qQf9cac&t=1885))
5. QOD - ([34:02](https://youtube.com/watch?v=Kni-qQf9cac&t=2042))
#Himalayanserow #vulnerablespecies #iranculeardeal #jcpoa #srilanka #india #japan #brexit #eu # uk #ImpactonIndia #port #containerterminal

https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1111/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mrs. Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link for optional counselling session: Open Counselling Session (rauias.com)



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-03-05-21-PDF
Word -  http://bit.ly/DNS-NOTES-03-05-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Optional counselling announcement – ([00:20](https://youtube.com/watch?v=SW6-TcG7VGs&t=20))
2. Left, TMC, BJP buck anti-incumbency; DMK bags T.N – (Society) - ([02:28](https://youtube.com/watch?v=SW6-TcG7VGs&t=148))
(Regionalism)
3. A second wave of the virus and another exodus – (Economy) ([22:49](https://youtube.com/watch?v=SW6-TcG7VGs&t=1369))
(Internal migration, reverse migration)
4. Contempt not initiated, HC tells Centre – (Polity) – ([33:23](https://youtube.com/watch?v=SW6-TcG7VGs&t=2003))
(Contempt of court)
5. Question for the Day – ([39:36](https://youtube.com/watch?v=SW6-TcG7VGs&t=2376))
#contemptofcourt #civilcontempt #criminalcontempt #internalmigration #reversemigration #regionalism #indiansociety #gs1 # migration

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1171/
 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass:    
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF Link: http://bit.ly/DNS-Notes-04-04-2021-PDF
Word Link: http://bit.ly/DNS-Notes-04-04-2021-Word

►TODAY’S THE HINDU ANALYSIS
1. Changes in the DRS System – Science & Technology - ([00:35](https://youtube.com/watch?v=ToUfwIJ4W1k&t=35)) 
(Technology in Cricket)
2. H1B Visa Issue – Economy – ([13:20](https://youtube.com/watch?v=ToUfwIJ4W1k&t=800))
(Impact of Expiry of Trump Era Order)
3. Question for the Day – ([29:05](https://youtube.com/watch?v=ToUfwIJ4W1k&t=1745))
Reference – (28:08)
4. Inflation Targeting – Economy
5. Digital currency – Economy 

#H1BVisa #DRSSytem #Inflation #DigitalCurrency #Cricket #HawkEye #Snickometer  


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1146/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-NOTES-04-07-2021-PDF
Word - https://bit.ly/DNS-NOTES-04-07-2021-WORD

►TODAY’S THE HINDU ANALYSIS
1. Social structure differs in Asian and African elephants, find researchers – Environment & Ecology – ([00:35](https://youtube.com/watch?v=0UbNXsZMncA&t=35))
(Ecological niche of elephants)
2. Why did Tirath Singh Rawat have to resign? – Polity & Governance – ([12:40](https://youtube.com/watch?v=0UbNXsZMncA&t=760))
(6-month-window to become a member of the assembly)
3. Can reducing cess levies ease high fuel prices? – Economy – ([24:16](https://youtube.com/watch?v=0UbNXsZMncA&t=1456))
(High fuel price)
4. What lies ahead for Afghanistan after U.S. exit? – International Relations - ([30:33](https://youtube.com/watch?v=0UbNXsZMncA&t=1833))
(U.S-AfghanExit)
5. Speed Test - ([35:19](https://youtube.com/watch?v=0UbNXsZMncA&t=2119))
6. Question for the Day – Environment & Ecology – ([39:33](https://youtube.com/watch?v=0UbNXsZMncA&t=2373))

# EcologicalNicheOfElephants #6-month-windowtoBecomeaMember #ReducingCessLeviesToEaseHighFuelPrices #U.S-AfghanExit

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 
►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021 
►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021


►DOWNLOAD DNS NOTES
PDF Link: http://bit.ly/DNS-Notes-04-05-21-PDF
Word Link: http://bit.ly/DNS-Notes-04-05-21-Word

►TODAY’S THE HINDU ANALYSIS
1. Equalization Levy – (Economy) –  ([02:26](https://youtube.com/watch?v=nCyHTbWyYx4&t=146))
2. Black carbon and Brown carbon – (Environment)  – ([19:31](https://youtube.com/watch?v=nCyHTbWyYx4&t=1171))
3. SDG India Index - NITI Ayog – (Social Issues) - ([29:45](https://youtube.com/watch?v=nCyHTbWyYx4&t=1785))
4. NASA's Venus Missions– (Science & Technology) – ([36:18](https://youtube.com/watch?v=nCyHTbWyYx4&t=2178))
5. Sedition Law – (Polity & Governance) – ([39:45](https://youtube.com/watch?v=nCyHTbWyYx4&t=2385))
6. Question for the Day – ([50:32](https://youtube.com/watch?v=nCyHTbWyYx4&t=3032))
References – (48:05)
7. Universal Health Coverage 
8. Social Media Regulations 
9. PM Gareeb Kalyan Anna Yojana 
10. Purchasing Managers’ Index  

#EqualizationLevy #SDG #NITIAyog #NASA #Venus #Sedition #PMI #Universalhealth #SocialMedia 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1203/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mangal Singh, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link – UPSC Notification - Civil Services Exam 2021 
http://bit.ly/UPSC-notification2021

►Link – Mains Weekly Question
https://elearn.rauias.com/mains-daily-questions/discuss-the-multi-sectoral-dimensions-of-malnutrition-in-india-and-ways-to-address-them-250-words/6040c52212b7a20532e1be99/
►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA


►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-04-03-21-PDF
Word - http://bit.ly/DNS-NOTES-04-03-21-WORD
►TODAY’S THE HINDU ANALYSIS
1.    Base Erosion and Profit shifting  - (Economy ) – ([02:13](https://youtube.com/watch?v=oQj7uDG9IWI&t=133)) 
a) Double taxation avoidance agreement
b) Treaty Shopping 
c) Round tripping
d) Transfer Mispricing 
(Cairn Energy Arbitration issue) 
2. Similipal Biosphere Reserve   – Environment  - ([22:40](https://youtube.com/watch?v=oQj7uDG9IWI&t=1360))
3. Question for the day – (Environment) – ([28:13](https://youtube.com/watch?v=oQj7uDG9IWI&t=1693))
References - (26:48)
4. Brexit - (Polity & Governance)
5. Mars Mission – (Environment)
6. Sugarcane pricing Policy – (Economy)
#BEPS #Transferpricing #Cairndispute #Similipal #Biosphere 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1118/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh and Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-04-05-21-PDF
Word - http://bit.ly/DNS-NOTES-04-05-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Optional Subjects related announcement – ([00:39](https://youtube.com/watch?v=cA2Py6shLHo&t=39))
2. ‘OPEC share slid as India’s oil imports shrank 11.8%’ – International Relation – ([02:14](https://youtube.com/watch?v=cA2Py6shLHo&t=134))
(OPEC+)
3. Factory output growth decelerates: PMI – Economy – ([06:40](https://youtube.com/watch?v=cA2Py6shLHo&t=400))
(Purchasing Managers’ Index)
4. 7 Myanmar refugees can approach UNHCR – International Relation – ([11:20](https://youtube.com/watch?v=cA2Py6shLHo&t=680))
(Principle of non-refoulement)
5. A ‘One Health’ approach that targets people, animals – (Social issues) - ([21:19](https://youtube.com/watch?v=cA2Py6shLHo&t=1279))
(One-Health Approach)
6. Question for the Day – ([26:26](https://youtube.com/watch?v=cA2Py6shLHo&t=1586))

# OPEC+ #PurchasingManagers’Index #One-HealthApproach #PrincipleOfNon refoulement

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass:    
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-05-04-21-PDF
Word - http://bit.ly/DNS-Notes-05-04-21-Word
►TODAY’S THE HINDU ANALYSIS
1. Going beyond Tamil Nadu’s ‘freebies’ narrative + In Tamil Nadu, environment is good politics (Election Manifestos) – Polity & Governance – ([00:55](https://youtube.com/watch?v=n0D3E3Q210Q&t=55))
(Importance of Election Manifestos, Part VIII – MCC, Environment & Farmers’ Rights)
2. The Kerala Model at the crossroads – Social Issues – ([18:34](https://youtube.com/watch?v=n0D3E3Q210Q&t=1114))
(Kerala Model of Development, KIIFB)
3. A Roadmap for Tolerance - Racial Discrimination – Social Justice – ([24:34](https://youtube.com/watch?v=n0D3E3Q210Q&t=1474))
(UNESCO, International Day for elimination of Discrimination)
4. Global Gender Gap Report 2021 - (Data Point) – (Social Justice) - ([35:12](https://youtube.com/watch?v=n0D3E3Q210Q&t=2112))
5. Summary – ([37:36](https://youtube.com/watch?v=n0D3E3Q210Q&t=2256))
6. Question for the Day – ([41:43](https://youtube.com/watch?v=n0D3E3Q210Q&t=2503))
#ElectionManifesto #FarmersRights #Environment #PartVIIIMCC #RPA126 #KeralaModel #KIIFB #RacialInjustice #Discrimination #Prejudice #UNESCO #GlobalGenderGapReport #GenderGapReport2021 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1147/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Basava Uppin of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

Link to register for Optional Orientation Class: https://www.rauias.com/optional-session/

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021 
►Link for 2021 Indian Economy Prelims Compass: http://bit.ly/Prelims-Compass-Economy-2021 
►Link for 2021 Polity & Governance Prelims Compass: http://bit.ly/Prelims-Compass-Polity-2021 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021  

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-05-06-21-PDF
Word - http://bit.ly/DNS-Notes-05-06-21-Word

►TODAY’S THE HINDU ANALYSIS
1. RBI's Monetary Policy – Indian Economy ([0:37](https://youtube.com/watch?v=tf-1MMkCnIM&t=37))
A. Regulatory Forbearance and its impact
B. Liquidity Adjustment Facility (LAF)
C. NACH  
2. Two Cheers- Gini Coefficient and Palma Ratio– Indian Economy ([34:04](https://youtube.com/watch?v=tf-1MMkCnIM&t=2044))
3. Saving biodiversity, securing earth’s future– Environment & Biodiversity ([42:01](https://youtube.com/watch?v=tf-1MMkCnIM&t=2521))
4. The time to limit global warming is melting away Environment & Biodiversity ([52:02](https://youtube.com/watch?v=tf-1MMkCnIM&t=3122))
5. Question for the Day (01:00:03)

#MonetaryPolicy #Socialmedia #Regulatoryforbearance #Inequality #Biodiversity 

 
https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1204/ 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Vaibhav Mishra & Mr. Naweed Akhter, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-05-03-2021-PDF
Word - http://bit.ly/DNS-Notes-05-03-2021-Word

►TODAY’S THE HINDU ANALYSIS
1. Will West Bengal tilt towards the right this election? - Governance - ([00:36](https://youtube.com/watch?v=_HTawZG2hZk&t=36))
 (Land Reforms)
2. Bengaluru, Shimla most liveable cities ([16:57](https://youtube.com/watch?v=_HTawZG2hZk&t=1017)) – Social Issues – ([16:57](https://youtube.com/watch?v=_HTawZG2hZk&t=1017))
(Ease of Living Index)
3. OTT Platforms will not have to register - Governance – ([26:56](https://youtube.com/watch?v=_HTawZG2hZk&t=1616))
(OTT Platform Regulation, Self-regulation)
4. With Biden, India may need a new template + Biden Govt. to deepen global alliances 
 – International Relations – ([33:09](https://youtube.com/watch?v=_HTawZG2hZk&t=1989))
(India-US ties under Biden, Present Indian Diaspora in US Administration)
5. IISc in Top 100 List in Natural Sciences Category – Social Issues – ([41:56](https://youtube.com/watch?v=_HTawZG2hZk&t=2516))
(QS World University Rankings, Top 3 IITs in Global Rankings)
6. India wants Chabahar Port on key corridor – International Relations - ([46:57](https://youtube.com/watch?v=_HTawZG2hZk&t=2817))
(International North South Transport Corridor, Eurasian Connectivity)
7. Question for the Day – ([49:25](https://youtube.com/watch?v=_HTawZG2hZk&t=2965))
#LandReforms #EaseofLivingIndex #OTTPlatforms #2+2Dialogue #InterimNationalSecurityStrategicGuidance #QSWorldUniversityRankings #ChabaharPort #InternationalNorth SouthTransportCorridor #EurasianConnectivity


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1119/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►https://www.rauias.com/optional-session



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
http://bit.ly/DNS-NOTES-05-05-21-WORD
http://bit.ly/DNS-NOTES-05-05-21-PDF

►TODAY’S THE HINDU ANALYSIS
1.Govt. gives nod for 5G trial; Chinese tech giants left out – Science and technology - 5G technology -  ([02:21](https://youtube.com/watch?v=qJ9DsL2i4J0&t=141))
2. G7 seeks common front on China - international relations - International organisations - ([16:38](https://youtube.com/watch?v=qJ9DsL2i4J0&t=998))
3. MFIs flag rural borrower distress to RBI- Indian economy - Micro Finance Institution -  ([24:12](https://youtube.com/watch?v=qJ9DsL2i4J0&t=1452))
4. scheme for revision - PM Kisan Sampada scheme - ([35:37](https://youtube.com/watch?v=qJ9DsL2i4J0&t=2137))
5. Question for the Day – ([44:47](https://youtube.com/watch?v=qJ9DsL2i4J0&t=2687))

#5G #LTE #telecomserviceprovider #internet #G7 #chinaandG7 #IndiaandG7 #MFI #RBI #PMkisansampadayojana #small loans #SHGs 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mrs. Pallavi Sarda of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass:    
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-06-04-21-PDF
Word - http://bit.ly/DNS-NOTES-06-04-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Announcement for Prelims Compass –([00:31](https://youtube.com/watch?v=HFCusuXnSWs&t=31)) 
2. Govt. amends IBC for MSME resolution - (Economy) - ([01:17](https://youtube.com/watch?v=HFCusuXnSWs&t=77)) 
3. The pillars of an equitable post-COVID India – (Social Justice, Economic inequality) ([12:19](https://youtube.com/watch?v=HFCusuXnSWs&t=739))
4. Lavrov-Jaishankar talks today–International Relations– ([29:19](https://youtube.com/watch?v=HFCusuXnSWs&t=1759))
(India-Russia Relation)
5. Railways completes arch closure of Chenab bridge– ([34:25](https://youtube.com/watch?v=HFCusuXnSWs&t=2065))
6. Question for the Day – ([37:04](https://youtube.com/watch?v=HFCusuXnSWs&t=2224))
#IndiaandRussia #inequality #msme #chenab #brahmaputra #postcovid 


https://www.rauias.com/daily-news-headlines/the-indian-express-edition-new-delhi-591/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-NOTES-06-07-21-PDF
Word - https://bit.ly/DNS-NOTES-06-07-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Crafting a unique partnership with Africa – International Relations – ([00:30](https://youtube.com/watch?v=LM1ctr5u7n8&t=30))
(India-Africa Partnership in agriculture)
2. 12 MLAs suspended for a year from Maharashtra Assembly – Polity & Governance – ([10:15](https://youtube.com/watch?v=LM1ctr5u7n8&t=615))
(Suspension of MLAs by speaker)
3. Will a national judiciary work? – Polity & Governance – ([17:32](https://youtube.com/watch?v=LM1ctr5u7n8&t=1052))
(All India Judicial Service)
4. Question for the Day – Polity & Governance – ([27:02](https://youtube.com/watch?v=LM1ctr5u7n8&t=1622))

#India-AfricaPartnershipInAgriculture #SuspensionOfMLAs #AllIndiaJudicialService #AIJS

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Basava Uppin, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-06-03-21-PDF
Word - http://bit.ly/DNS-NOTES-06-03-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Patching the gaps in India’s cybersecurity – Internal Security ([0:38](https://youtube.com/watch?v=p7erOMFLnLM&t=38))
     ( Cyber Security: Steps taken, Challenges and Strategies needed) 
2. ‘OPEC+ move to hit recovery’ – International Relations  ([20:58](https://youtube.com/watch?v=p7erOMFLnLM&t=1258))
3. Technology for air-to-air missiles tested- SFDR Technology– Defence Technology ([27:15](https://youtube.com/watch?v=p7erOMFLnLM&t=1635))
4. Secrecy shrouds proposed cryptocurrency Bill: IAMAI– Indian Economy ([30:54](https://youtube.com/watch?v=p7erOMFLnLM&t=1854))
5. Pochampally : India’s Cultural Heritage- Art and Culture ([33:12](https://youtube.com/watch?v=p7erOMFLnLM&t=1992))
6. Question for the day ([35:51](https://youtube.com/watch?v=p7erOMFLnLM&t=2151))

#CyberSecurity #OPEC+ #SFDR #CBDC

https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1120/ 
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj and Mr Vaibhav Mishra of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►https://www.rauias.com/optional-session


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021
►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-06-05-21-PDF
WORD- http://bit.ly/DNS0-NOTES-06-5-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Maratha Quota unconstitutional, violates right to equality, says SC Page 01- (Polity and Governance) - ([02:18](https://youtube.com/watch?v=S10N67FdL0U&t=138))
2. Small Businesses, MSMEs to get relief Page 14 - (Economy) - ([11:30](https://youtube.com/watch?v=S10N67FdL0U&t=690))
3. Cabinet clears IDBI bank strategic disinvestment Page 14 - (Economy) - ([21:29](https://youtube.com/watch?v=S10N67FdL0U&t=1289))
4. Services activity eased to three-month low in April Page 14 - (Economy)- ([34:52](https://youtube.com/watch?v=S10N67FdL0U&t=2092))
5. Scheme for revision (add-on) - ([41:28](https://youtube.com/watch?v=S10N67FdL0U&t=2488))
6. QOD - ([48:16](https://youtube.com/watch?v=S10N67FdL0U&t=2896))

#MarathaQuota #SEBC #IDBI #strategic disinvestment #Scheme #MSME 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS
GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1174/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Vaibhav Mishra of        
#Hindunewspaperanalysis​ #RauIASDNS​ #UPSC​ #Currentaffairs​ 



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass:    
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
http://bit.ly/DNS-NOTES-07-04-21-WORD
http://bit.ly/DNS-NOTES-07-04-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. Reworking net-zero for climate justice Page 08 - (Environment) - ([00:46](https://youtube.com/watch?v=Rrw3tAGT8HA&t=46))
2. The start of a more authoritarian era Page 09 - (International Relations) - ([16:47](https://youtube.com/watch?v=Rrw3tAGT8HA&t=1007)) 
3. Vigilance officers to be transferred every 3 years Page 01 - (Polity and Governance) - ([28:29](https://youtube.com/watch?v=Rrw3tAGT8HA&t=1709))
4. Justice Ramana will be the next CJI Page 01 - (Polity and Governance) - ([34:41](https://youtube.com/watch?v=Rrw3tAGT8HA&t=2081))
5. QOD - ([38:12](https://youtube.com/watch?v=Rrw3tAGT8HA&t=2292))


https://www.rauias.com/daily-news-headlines/the-indian-express-edition-new-delhi-592/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-...​

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI​
▪ History: https://youtu.be/lEY9ozd-8FI​
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjd...​
▪ Geography: https://youtu.be/l2fYquMkdoY​


​  



​

Presented by: Mr. Vaibhav Mishra  of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021
►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
https://bit.ly/DNS-NOTES-07-07-21-PDF
https://bit.ly/DNS-NOTES-07-07-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Fresh stirrings on federalism as a new politics 06 - (Polity and Governance) - ([00:45](https://youtube.com/watch?v=vuIgLAQM1GM&t=45)) 
2. U.S. left Afghan airbase without notice’ 11 - (International Relations) - ([19:15](https://youtube.com/watch?v=vuIgLAQM1GM&t=1155))
3. Crimes against women saw over 63% rise in 2021 so far 03 - (Social Issues) - ([29:28](https://youtube.com/watch?v=vuIgLAQM1GM&t=1768)) 
4. Oil soars to multi-year highs after OPEC+ talks collapse 12  - (International Relations) - ([33:14](https://youtube.com/watch?v=vuIgLAQM1GM&t=1994))
5. Amid Cabinet reshuffle buzz, eight States get new Governors 01- (Polity and Governance) - ([37:04](https://youtube.com/watch?v=vuIgLAQM1GM&t=2224))
6. Vacancies send a wrong signal 07 - (Polity and governance) - ([40:37](https://youtube.com/watch?v=vuIgLAQM1GM&t=2437))
7. QOD - ([42:42](https://youtube.com/watch?v=vuIgLAQM1GM&t=2562))


/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1236/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Vaibhav Mishra of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021
►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-07-06-21-PDF
WORD - http://bit.ly/DNS-NOTES-07-06-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. India's Borders (LOC, LAC, AGPL) Page 01 – (Security) - ([00:43](https://youtube.com/watch?v=pVI_45aqrW8&t=43))
2. Primary Education in our Country Page 01- (Social Issues) - ([12:03](https://youtube.com/watch?v=pVI_45aqrW8&t=723))
3. Education in Constitution of India – (Social Issues) - ([27:31](https://youtube.com/watch?v=pVI_45aqrW8&t=1651))
4. Reference: ([35:33](https://youtube.com/watch?v=pVI_45aqrW8&t=2133))
          RBI policy framework statement Page 06       https://www.youtube.com/watch?v=tf-1MMkCnIM&t=2280s
   Sedition Page 06 
https://www.youtube.com/watch?v=tSMTiF1AKX4&t=1006s
5. QOD - ([38:23](https://youtube.com/watch?v=pVI_45aqrW8&t=2303))
#LOC #LAC #AGPL #SIACHEN #LADKAH #POK #EDUCATION #ASER #PRATHAM #Constitution 


/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Faizan Khan, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA
 
►Link UPSC MAINS 2020 GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4


►DOWNLOAD DNS NOTES

PDF - http://bit.ly/DNS-Notes-07-03-21-PDF
Word - http://bit.ly/DNS-Notes-07-03-21-Word

►TODAY’S THE HINDU ANALYSIS

1.  Slowing currents– (Physical geography) – ([00:39](https://youtube.com/watch?v=WdLWpOascIo&t=39))
(Cause and impact of slowing Gulf Stream)
2. A Three-member expert panel takes stock of turtle conservation – (Polity & Governance + Ecology) – ([19:39](https://youtube.com/watch?v=WdLWpOascIo&t=1179))
(Olive Ridley Turtle)
3. Now, a help desk for transgenders  – (Social Issues) – ([25:42](https://youtube.com/watch?v=WdLWpOascIo&t=1542))
    (Community policing) 
4. Only NRI quota seats for OCI cardholders  – (Polity & Governance) – ([30:41](https://youtube.com/watch?v=WdLWpOascIo&t=1841))
    (Overseas Citizen of India)
5. Speed Test - ([36:55](https://youtube.com/watch?v=WdLWpOascIo&t=2215))
6. Question for the day – (Physical Geography) – ([40:27](https://youtube.com/watch?v=WdLWpOascIo&t=2427))


#SlowingGulfStream #OliveRidleyTurtle #CommunityPolicing #OverseasCitizenOfIndia


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1121/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mr. Basava Uppin of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

Link to register for Optional Orientation Class: https://www.rauias.com/optional-session/


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021 
►Link for 2021 Indian Economy Prelims Compass: http://bit.ly/Prelims-Compass-Economy-2021 
►Link for 2021 Polity & Governance Prelims Compass: http://bit.ly/Prelims-Compass-Polity-2021 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021  

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-07-05-21-PDF
Word - http://bit.ly/DNS-Notes-07-05-21-Word

►TODAY’S THE HINDU ANALYSIS
1. MTS Question ([02:27](https://youtube.com/watch?v=TtQZCXAILzE&t=147))
1. Waiver for Patents to deal with CoVID-19 – Indian Economy ([03:11](https://youtube.com/watch?v=TtQZCXAILzE&t=191))
a. India’s Proposal
b. Reasons for Opposition
c. Pros and Cons of Waiver on Patents  
2. Centre Transfers Revenue Deficit Grants to States– Polity & Governance ([30:57](https://youtube.com/watch?v=TtQZCXAILzE&t=1857))
3. Approval for Sputnik Light Vaccines- Science and Technology ([35:00](https://youtube.com/watch?v=TtQZCXAILzE&t=2100))
4. Tensions between China and Australia- International Relations ([43:34](https://youtube.com/watch?v=TtQZCXAILzE&t=2614))
    Link: https://www.youtube.com/watch?v=V9EVxnj7TfU&t=292s 
5. Question for the Day  ([44:17](https://youtube.com/watch?v=TtQZCXAILzE&t=2657))

#PatentWaiver #Sputniklight #FinanceCommission


https://www.rauias.com/uncategorized/the-hindu-edition-new-delhi-1175/ 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass:    
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►Rau's IAS - Online learning platform - elearn.rauias.com

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-08-04-21-PDF
Word - http://bit.ly/DNS-Notes-08-04-21-Word

►TODAY’S THE HINDU ANALYSIS
1. Lok Adalats - Effectiveness – Polity & Governance - ([01:02](https://youtube.com/watch?v=sb2sXpwVkoQ&t=62)) 
(Speedy Justice vs Quality of Justice)
2. Carbon Neutrality targets - India's Objections– Environment – ([17:03](https://youtube.com/watch?v=sb2sXpwVkoQ&t=1023))
3. RBI Monetary Policy Announcement – Economy – ([26:19](https://youtube.com/watch?v=sb2sXpwVkoQ&t=1579))
4. Women's Right to Abortion – Social Justice – ([40:23](https://youtube.com/watch?v=sb2sXpwVkoQ&t=2423))
5. Provisions for Transgender persons – Social Justice ([48:58](https://youtube.com/watch?v=sb2sXpwVkoQ&t=2938))
6. Question for the Day – ([53:21](https://youtube.com/watch?v=sb2sXpwVkoQ&t=3201))

#Carbon #LOKADALAT #RBI #Monetary #Abortion #Transgender  


https://www.rauias.com/daily-news-headlines/the-indian-express-edition-new-delhi-593/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registratio... 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass:  


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-08-06-21-PDF
Word - http://bit.ly/DNS-NOTES-08-06-21-WORD
►TODAY’S THE HINDU ANALYSIS
1. Protect LGBTQIA+ Community: HC – Polity & Governance – ([00:56](https://youtube.com/watch?v=eWNsSndc6XE&t=56))
(LGBTQIA Community, Transgenders Act, Navtej Singh Johar, Section 377 IPC)
2. LPA & Indian Ocean Dipole – Geography – ([16:46](https://youtube.com/watch?v=eWNsSndc6XE&t=1006))
(LPA, Indian Dipole, El Nino)
3. Draft Rules - Live Streaming of Court Proceedings – Polity & Governance – ([23:37](https://youtube.com/watch?v=eWNsSndc6XE&t=1417))
(e-Committee of Supreme Court, Exceptions for Live Proceedings)
4. China hosts ASEAN Foreign Ministers – International Relations - ([28:35](https://youtube.com/watch?v=eWNsSndc6XE&t=1715))
(QUAD, ASEAN, China)
5. Towards a stronger mental health strategy – (Social Justice) – ([31:24](https://youtube.com/watch?v=eWNsSndc6XE&t=1884))
(Mental Illness, Mental Healthcare Act, Advance Directives)
6. Question for the Day – ([38:00](https://youtube.com/watch?v=eWNsSndc6XE&t=2280))
#LGBTQIA #NavtejJohar #Section377 #LAP #IOD #NegativeDipole #LiveStreaming #e-Committee #QUAD #ASEAN #China #MentalHealth 

https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1207/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Vaibhav Mishra, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF -  http://bit.ly/DNS-Notes-08-03-21-PDF
Word - http://bit.ly/DNS-Notes-08-03-21-Word

►TODAY’S THE HINDU ANALYSIS
1. China gives green light for first downstream dams on Brahmaputra Page 01 - (International Relations) - ([00:38](https://youtube.com/watch?v=9tbH_bS-uNY&t=38))
2. Health first, fiscal prudence later Page 07 - (Polity and Governance) - ([11:54](https://youtube.com/watch?v=9tbH_bS-uNY&t=714))
3. Railways and a question of transparency Page 07 - (Economy) - ([23:19](https://youtube.com/watch?v=9tbH_bS-uNY&t=1399))
4. Janaushadhi is helping the poor, says Modi Page 10 - (Polity and Governance) - ([32:24](https://youtube.com/watch?v=9tbH_bS-uNY&t=1944))
5. Commemorating change, changing commemoration Page 06 - (History)-([40:04](https://youtube.com/watch?v=9tbH_bS-uNY&t=2404))
6. QOD - ([43:36](https://youtube.com/watch?v=9tbH_bS-uNY&t=2616))


https://www.rauias.com/daily-news-headlines/the-indian-express-edition-new-delhi-567/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Description:
Presented by: Mr. JATIN BHARDWAJ of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►https://www.rauias.com/optional-session



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
http://bit.ly/DNS-NOTES-08-05-21-WORD
http://bit.ly/DNS-NOTES-08-05-21-PDF

►TODAY’S THE HINDU ANALYSIS
1.‘ IPR regime in India: brief revision – international affairs – International trade – ([02:15](https://youtube.com/watch?v=LRKS6ZT-uB8&t=135))
2. Digital divide amid pandemic – governance and polity - ([26:09](https://youtube.com/watch?v=LRKS6ZT-uB8&t=1569))
3. Rent issues to the migrant workers – issues related to urbanisation – Indian society- ([38:30](https://youtube.com/watch?v=LRKS6ZT-uB8&t=2310))
 4. Scheme for revision (add-on) - ([43:41](https://youtube.com/watch?v=LRKS6ZT-uB8&t=2621))
5. Question for the Day – ([53:08](https://youtube.com/watch?v=LRKS6ZT-uB8&t=3188))

#IPR #compulsorylicensing #AtalBhoojalyojana #jalshaktiabhiyan #jaljeevanmission #WTO #patents #trademarks #geographicalindication #Designs #plantvarieties #CIPAM #evergreening


/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass:    
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►Rau's IAS - Online learning platform - elearn.rauias.com

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS
GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-09-04-21-PDF
Word - http://bit.ly/DNS-NOTES-09-04-21-WORD
►TODAY’S THE HINDU ANALYSIS
1. ‘No ad hoc judges in place of regular recommendations’– Polity & Governance – ([00:56](https://youtube.com/watch?v=oORYb7_JvUM&t=56))
(Ad Hoc Judges, Article 224A, Article 127, Article 128)
2. RBI’s first purchase under G-SAP 1.0 – Economy – ([12:35](https://youtube.com/watch?v=oORYb7_JvUM&t=755))
(G-SAP, Open Market Operations, Yield Curve)
3. Plough to plate, hand held by the Indian state – Economy – ([18:46](https://youtube.com/watch?v=oORYb7_JvUM&t=1126))
(Problems with Indian Agriculture, State Intervention for Agriculture)
4. Biden govt. restores aid to Palestinians – (International Relations) - ([28:13](https://youtube.com/watch?v=oORYb7_JvUM&t=1693))
(Fund provided for relief in Palestine)
5. All options open on South China Sea – (International Relations) - ([29:46](https://youtube.com/watch?v=oORYb7_JvUM&t=1786))
(Whitsun Reef, South China Sea, Spratley Islands)
6. U.K. to set up £43 million fund for migrants from Hong Kong – (International Relations) - ([31:51](https://youtube.com/watch?v=oORYb7_JvUM&t=1911))
(UK to help migrants from Hong Kong)
7. Revision – ([34:31](https://youtube.com/watch?v=oORYb7_JvUM&t=2071))
8. Question for the Day – ([38:53](https://youtube.com/watch?v=oORYb7_JvUM&t=2333))
#AdhocJudges #HighCourt #SupremeCourt #Article224A #GSAP #OMO #BondYield #YieldCurve #Agriculture #SouthChinaSea #WhitsunReef #SpratleyIslands #Palestine #UNRWA #USAID #HongKong #UK #Fundformigrants 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/ 

/IAS - ELEARN 



 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Title: 
THE HINDU Analysis, 09 July 2021 (Daily Current Affairs for UPSC IAS) – DNS


Description:
Presented by: Mr. Basava Uppin of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021  ►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 


►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-Notes-09-07-21-PDF
Word - https://bit.ly/DNS-Notes-09-07-21-Word




►TODAY’S THE HINDU ANALYSIS
1 New Phase- 2nd ARC Report on Structure of Government- Polity & Governance (00:41)
A. Ministries and Departments
B. Formation of Cabinet Committees
C. Limitations and Recommendations    
2. Agri Infra Fund and Farmers' Trade and Commerce Act, 2020– Indian Economy ([28:50](https://youtube.com/watch?v=AvK7EGqGxZI&t=1730))
3. ‘Tracking Fugitives– Internal Security ([37:27](https://youtube.com/watch?v=AvK7EGqGxZI&t=2247))
4. A blip- 4 Years of GST– Indian Economy ([42:55](https://youtube.com/watch?v=AvK7EGqGxZI&t=2575))
5. Should only Elected legislators be eligible for Chief Minister?- Reference ([45:19](https://youtube.com/watch?v=AvK7EGqGxZI&t=2719))
YouTube Link: https://www.youtube.com/watch?v=0UbNXsZMncA&t=939s 
6. Question for the Day ([46:01](https://youtube.com/watch?v=AvK7EGqGxZI&t=2761))


#StructureofGovernment #MinistriesandDepartments #2ndARCReport #AgriInfraFund #GST


 
https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1238/ 


/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/


 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY



  




Tags: 
daily news and analysis,daily news analysis upsc,upsc online,upsc daily current affairs,best youtube channel for upsc,current affairs 2020,newspaper analysis for upsc,daily news analysis the hindu,best daily news analysis for upsc,daily gk newspaper analysis,current affairs channel,daily newspaper analysis,daily news analysis in English,current affairs today,the hindu newspaper analysis,dnsraus,rausdns,rausias,rauias, GSMainsAnalysis,
Presented by: Mr. Basava Uppin of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021  
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-09-06-2021-PDF
Word - http://bit.ly/DNS-NOTES-09-06-2021-WORD

►TODAY’S THE HINDU ANALYSIS
1. Building trust in governance - Polity & Governance – ([00:37](https://youtube.com/watch?v=z6RwuOrUqD8&t=37))
    E-Governance: Benefits, Challenges and Solutions 
2. ‘High number of Cong. MLAs joined BJP’ – Polity & Governance – ([22:22](https://youtube.com/watch?v=z6RwuOrUqD8&t=1342))
     Anti-defection- Provisions, abuse and Recommendations
3. Shifting the Tax Burden – Indian Economy – ([33:51](https://youtube.com/watch?v=z6RwuOrUqD8&t=2031))
4. This time for Male – International Relations – ([38:10](https://youtube.com/watch?v=z6RwuOrUqD8&t=2290))
5. Bring genomic sequencing into the pandemic fight – reference ([43:17](https://youtube.com/watch?v=z6RwuOrUqD8&t=2597))
    Link: https://www.youtube.com/watch?v=m3wXhyzu5mU&t=1853s 
6. Prelims based MCQs
    (a) Defence Offset Policy ([44:18](https://youtube.com/watch?v=z6RwuOrUqD8&t=2658))
    (b) World Bank Report (47:31)
    (c) New Election Commissioner Appointed (49:39)
7. Question for the Day ([51:00](https://youtube.com/watch?v=z6RwuOrUqD8&t=3060))

#E-Governance #Anti-defection #OffsetPolicy #ElectionCommission #Tax-collection

 
https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1208/ 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mangal Singh, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-09-March-pdf
WORD - http://bit.ly/DNS-NOTES-09-March-Word
►TODAY’S THE HINDU ANALYSIS
1. Afghan peace process  - (International Relations ) – ([00:30](https://youtube.com/watch?v=bLuvXRCEolo&t=30)) 
(US Policy on Afghan War under new President) 
2. Maratha Quota issue   – (Polity & governance)  - ([21:43](https://youtube.com/watch?v=bLuvXRCEolo&t=1303))
3. Electoral Bonds - (Polity & governance)  - ([29:54](https://youtube.com/watch?v=bLuvXRCEolo&t=1794))
4. Question for the day – (Environment) – ([42:48](https://youtube.com/watch?v=bLuvXRCEolo&t=2568))
References - (42:21)
5. NSIL – ISRO 
#Afghan #Maratha #Quota #reservation #Electoral #Bonds # 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1123/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Vaibhav Mishra of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021
►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
WORD - http://bit.ly/DNS-NOTES-09-05-21-WORD
PDF - http://bit.ly/DNS-NOTES-09-05-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. Announcement for Optional Courses: ([0:40](https://youtube.com/watch?v=1hWsPDJKt1E&t=40))
2. E.U., India relaunch FTA talks, sign connectivity partnership  Page 06 – (International Relations) – ([02:26](https://youtube.com/watch?v=1hWsPDJKt1E&t=146))
2. Hope soars as Second World War airport in Assam takes commercial wings Page 08 – (Infrastructure) – ([29:30](https://youtube.com/watch?v=1hWsPDJKt1E&t=1770))
3. FCRA hurdle may block foreign COVID aid to hospitals, NGOs Page 08 – (Polity and Governance) – ([39:44](https://youtube.com/watch?v=1hWsPDJKt1E&t=2384))
4. Probe into digging, construction activities in Kaziranga Page 06 – ([45:51](https://youtube.com/watch?v=1hWsPDJKt1E&t=2751))(Environment) - (48:10)
5. QOD: ([48:13](https://youtube.com/watch?v=1hWsPDJKt1E&t=2893))
References: 
a. IP rights and Vaccines (Page 11)
https://www.youtube.com/watch?v=TtQZCXAILzE&t=305s
b. The SC ruling on identifying backward classes (Page 11)
https://www.youtube.com/watch?v=S10N67FdL0U
c. India's Model BIT 
https://www.youtube.com/watch?v=_ja_v0V6dbQ&t=481s

#INDIAEU #BTIA #AVIATIONSECTOR #FCRA #Kaziranga 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS
GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-10-04-2021-PDF
Word - http://bit.ly/DNS-Notes-10-04-2021-Word

►TODAY’S THE HINDU ANALYSIS
1.‘ India protests U.S. naval exercise + Nod required for exercises: India – International Relations – ([00:38](https://youtube.com/watch?v=wNc6ejvVoHs&t=38))
(UNCLOS & Sovereignty in EEZ)
2. Net direct tax receipts rise 5% – Economy – ([15:08](https://youtube.com/watch?v=wNc6ejvVoHs&t=908))
(Direct tax collection)
3. India does have a refugee problem – Polity & Governance– ([21:38](https://youtube.com/watch?v=wNc6ejvVoHs&t=1298))
(Refugee Policy of India)
4. Speed Test – ([28:34](https://youtube.com/watch?v=wNc6ejvVoHs&t=1714))
5. Question for the Day – Polity & Governance - ([33:50](https://youtube.com/watch?v=wNc6ejvVoHs&t=2030))

#UNCLOS #MarineZones #EEZ #Indo-USDefenceRelation #TaxBuoyancy #DirectTax #TaxReform #India’sRefugeePolicy 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mrs. Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link for optional counselling session: Open Counselling Session (rauias.com)



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-NOTES-10-07-21-PDF
Word - https://bit.ly/DNS-NOTES-10-07-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Terrain, facilities will affect delimitation in J&K, says ECI – (Polity) - ([00:40](https://youtube.com/watch?v=IzhbdUAPbqs&t=40))
2. Cooperation Ministry will usurp States’ rights: Oppn. (Polity) - ([17:09](https://youtube.com/watch?v=IzhbdUAPbqs&t=1029))
3. A perception shift in relations between Sri Lanka and China? (IR) - ([22:41](https://youtube.com/watch?v=IzhbdUAPbqs&t=1361))
4. ‘NGT bench in Delhi not superior to zonal benches’ (Polity) - ([29:49](https://youtube.com/watch?v=IzhbdUAPbqs&t=1789))
5. QOD – ([38:23](https://youtube.com/watch?v=IzhbdUAPbqs&t=2303))
#ngt #cooperataionministry #cooperatives #delimitation

/IAS - ELEARN 
▪ eLearn - All our content is available on our learning platform - https://elearn.rauias.com 


►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1239/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 

#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
WORD -  http://bit.ly/DNS-Notes-10-06-21-Word
PDF -  http://bit.ly/DNS-Notes-10-06-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. At home – Social Justice - ([00:33](https://youtube.com/watch?v=ciVOpPSeWOY&t=33))
(Child adoption in India)
2. The promise and perils of digital justice delivery – Polity & Governance - ([08:48](https://youtube.com/watch?v=ciVOpPSeWOY&t=528))
(Digital justice delivery)
3. No decision yet on Indian consulate in Addu Atoll: Solih – International Relation – ([16:10](https://youtube.com/watch?v=ciVOpPSeWOY&t=970))
(liability waiver)
4. Speed Test -  ([20:56](https://youtube.com/watch?v=ciVOpPSeWOY&t=1256))
5. Question for the Day - (Polity & Governance) – ([23:27](https://youtube.com/watch?v=ciVOpPSeWOY&t=1407))

#ChildAdoptioninIndia #DigitalJusticedelivery #IndianConsulateInAdduAtoll 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-10-03-2021-PDF
Word - http://bit.ly/DNS-NOTES-10-03-2021-WORD

►TODAY’S THE HINDU ANALYSIS
1. Over 72% rise in UAPA cases registered in 2019 – Polity & Governance - ([00:44](https://youtube.com/watch?v=dWXT_-a7Mlg&t=44))
 (UAPA, Article 19, Terrorist Organisation, Unlawful Activities, Amendment to UAPA)
2. Cyber Volunteer Programme rolled out – Security – ([12:54](https://youtube.com/watch?v=dWXT_-a7Mlg&t=774))
(Indian Cyber Crime Coordination Centre, Cybercrime Grievance Portal, MHA)
3. Enabling the Business of Agriculture (EBA) 2019" Report – Economy – ([17:11](https://youtube.com/watch?v=dWXT_-a7Mlg&t=1031))
(EBA Report, 2019, World Bank, India’s Rank – 49/101)
4. Panel flags Centre’s meagre pensions – Economy – ([23:27](https://youtube.com/watch?v=dWXT_-a7Mlg&t=1407))
(National Social Assistance Programme, NSAP)
5. In need of full time heads – Security – ([27:02](https://youtube.com/watch?v=dWXT_-a7Mlg&t=1622))
(CAPF, Challenges to Security Organisations, Overcoming Challenges)
6. Question for the Day – ([33:06](https://youtube.com/watch?v=dWXT_-a7Mlg&t=1986))
#UAPA #Article19 #ConstitutionalAmendments #16CA #CyberVolunteerProgramme #Cybercrime # CybercrimeGrievancePortal #MHA #EBAReport2019 #WorldBank #NSAP #NationalSocialAssistanceProgramme


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1124/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj & Mr. Vaibhav Mishra of   
     
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS
GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-10-05-21-PDF
WORD - http://bit.ly/DNS-NOTES-10-05-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Outreach & Overreach Page 06 - ([02:24](https://youtube.com/watch?v=QdGV36-0ZHg&t=144))
2. Back in the shortage economy Page 07 - ([20:14](https://youtube.com/watch?v=QdGV36-0ZHg&t=1214))
3. China Rocket debris falls in Indian Ocean near Maldives - Page 09 - ([25:23](https://youtube.com/watch?v=QdGV36-0ZHg&t=1523))
4. Scheme for revision:  Schemes under Ministry of Minority Affairs -([33:15](https://youtube.com/watch?v=QdGV36-0ZHg&t=1995)) 
5. QOD - ([37:02](https://youtube.com/watch?v=QdGV36-0ZHg&t=2222))

#judiciary #Activism #Overreach #Chinese rockets #Schemesforminorities 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1178/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Vaibhav Mishra of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►DOWNLOAD DNS NOTES
WORD - http://bit.ly/DNS-NOTES-11-04-21-WORD
PDF- http://bit.ly/DNS-NOTES-11-04-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. The ‘armed struggle’ that goes nowhere Page 11 - (Internal Security) - ([01:19](https://youtube.com/watch?v=HOw9hqdbPbw&t=79))
2. Another hint of ‘new physics’, this time from Fermilab Page 10 - (Science and Technology) - ([23:13](https://youtube.com/watch?v=HOw9hqdbPbw&t=1393))
3. Indus and Ganges river dolphins are two different species Page 10 - (Environment) - ([33:04](https://youtube.com/watch?v=HOw9hqdbPbw&t=1984))
4. IAF to adopt new process to lease refuelling aircraft Page 08 - (Science and Technology) - ([42:43](https://youtube.com/watch?v=HOw9hqdbPbw&t=2563))
5. QOD - ([44:54](https://youtube.com/watch?v=HOw9hqdbPbw&t=2694))

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


 https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1152/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-NOTES-11-07-21-PDF
Word - https://bit.ly/DNS-NOTES-11-07-21-Word

Answer of Yesterday’s QoD (10-07-21) is (d)

►TODAY’S THE HINDU ANALYSIS
1. Sikkim blossoms: State home to 27% of India’s flowering plants – Environment & Ecology – ([00:29](https://youtube.com/watch?v=a5_Pwo5wmvI&t=29))
(Sikkim – its relief, climate & vegetation)
2. As Taliban advance, India pulls out Kandahar consulate staff – International Relation – ([06:45](https://youtube.com/watch?v=a5_Pwo5wmvI&t=405))
(Places in News from Afghanistan)
3. Cuba develops conjugate vaccine– Science & Technology – ([08:18](https://youtube.com/watch?v=a5_Pwo5wmvI&t=498))
(Conjugate Vaccine)
4. Keen to empower co-ops: Shah – Polity & Governance – ([18:15](https://youtube.com/watch?v=a5_Pwo5wmvI&t=1095))
(Cooperatives)
5. Speed Test - ([27:32](https://youtube.com/watch?v=a5_Pwo5wmvI&t=1652))
6. Question for the Day – Science & Technology – ([27:49](https://youtube.com/watch?v=a5_Pwo5wmvI&t=1669))

#Sikkimhas27%ofFlowers # PlacesInNewsFromAfghanistan #Coperatives #ConjugateVaccine

SPEED TEST (Question)

1. Sikkim is the only state that borders with only one other state. (T/F)
2. Choose the odd one out: (a) Barsey Rhododendron Wildlife Sanctuary; (b) Kyongnosla Alpine  Wildlife Sanctuary; (c) Fambong Lho Wildlife Sanctuary; (d) Siju Wildlife Sanctuary.
3. Teesta river is tributary of Brahmaputra. (T/F)
4. Jelep la pass is at the 'tri-junction' of India, China and Myanmar. (T/F)
5. Doklam plateau is disputed among India, China and Bhutan. (T/F)
6. Choose the odd one out – (a) Herat; (b) Taiz; (c) Mazar-e-Sharif; (d) Bamiyan
7. Amu Darya flows into Afghanistan from Uzbekistan. (T/F)
8. Sub-unit vaccine are much more potent than inactivated vaccines. (T/F)
9. Inactivated vaccines are used for Hepatitis B. (T/F)
10. ‘Cooperative Societies’ has been transferred recently to Union List in Seventh Schedule. (T/F)
11. Urban cooperatives directly under the control of RBI. (T/F)
12. Right to form co-operative societies is a fundamental right. (T/F)

SPEED TEST (Answer)
1. False – Meghalaya too has border with only Assam
2.  (d) Siju Wildlife Sanctuary. This is in Meghalaya. Rest are in Sikkim.
3. True - Teesta river is tributary of Brahmaputra. Teesta Meets Brahmaputra in Bangladesh. 
4. False. Jelep la pass is at the 'tri-junction' of India, China and Bhutan. 
5. False. Doklam plateau is disputed between China and Bhutan.
6. Taiz. Taiz is in Yemen. Rest are in Afghanistan. 
7. False. Amu Darya flows into Afghanistan from Turkmenistan. Althogh the river forms part of Afghanistan's northern border with Tajikistan, Uzbekistan, and Turkmenistan.
8. False. Sub-unit vaccine are less potent than inactivated vaccines. 
9. False - Sub-unit vaccine are used for Hepatitis B.
10. False. The statement is completely imaginary.  
11. True. Urban cooperatives directly under the control of RBI.
12. True. Right to form co-operative societies is a fundamental right. 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registratio... 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass:  


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-11-06-21-PDF
Word - http://bit.ly/DNS-NOTES-11-06-21-WORD
►TODAY’S THE HINDU ANALYSIS
1. Official Secrets Act & RTI – Polity & Governance – ([00:57](https://youtube.com/watch?v=S_jotYRn3N8&t=57))
(Secret Information, Prohibited Place, Official Secrets Act, RTI)
2. Ecosystem Restoration – Environment – ([16:54](https://youtube.com/watch?v=S_jotYRn3N8&t=1014))
(Ecosystem Restoration, UNEP, FAO, Ecosystem)
3. CHIME Radio Telescope – Science & Technology – ([28:36](https://youtube.com/watch?v=S_jotYRn3N8&t=1716))
(CHIME Radio Telescope, Fast Radio Bursts, Magnetars)
4. Prelims Pointers
(i) Pakke Tiger Reserve – Environment – ([32:05](https://youtube.com/watch?v=S_jotYRn3N8&t=1925))
(ii) Sahel Region – Geography – (34:21)
(iii) New Atlantic Charter – World History – (35:59)
5. Question for the Day – ([38:18](https://youtube.com/watch?v=S_jotYRn3N8&t=2298))
#OfficialSecret #SecretInformation #OSA #RTIvOSA #Ecosystem #Restoration #UNEP #FAO #CHIME #FRB #Magnetars #Pakke #TigerReserve #Sahel #AtlanticCharter

https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1210/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Basava Uppin, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link – Mains Weekly Question
 https://elearn.rauias.com/mains-daily-questions/only-increasing-the-age-of-marriage-for-girls-will-not-solve-their-underlying-developmental-issues-e/6048ab0f7737d42685d14f86/

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF -http://bit.ly/DNS-NOTES-11-03-21-PDF
Word - http://bit.ly/DNS-NOTES-11-03-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. A case for a revamped, need-based PDS– Indian Economy  ([01:36](https://youtube.com/watch?v=jm33wN3UnC8&t=96))
     ( PDS: Functioning, Limitations, Steps Taken and Strategies needed) 
2.   RBI's PCA Framework – Indian Economy  ([25:25](https://youtube.com/watch?v=jm33wN3UnC8&t=1525))
3.  India's largest Floating Solar Power Plant – Environment ([36:16](https://youtube.com/watch?v=jm33wN3UnC8&t=2176))
4. Cabinet approves Pradhan Mantri Swasthya Suraksha Nidhi (PMSSN) – Social Issues ([43:03](https://youtube.com/watch?v=jm33wN3UnC8&t=2583))
5. Regulation Redux : Environment Impact Assessment ([47:22](https://youtube.com/watch?v=jm33wN3UnC8&t=2842))
    Link: https://www.youtube.com/watch?v=9vQD_s4hUYY&t=1332s 
6. Question for the day ([50:57](https://youtube.com/watch?v=jm33wN3UnC8&t=3057))

#PDS #PCA #FloatingSolarPlant #Healthcare


https://www.rauias.com/daily-news-headlines/the-hindu-express-edition-new-delhi/ 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Vaibhav Mishra of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021
►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
WORD: http://bit.ly/DNS-NOTES-11-05-21-WORD
PDF: http://bit.ly/DNS-NOTES-11-05-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. Decoding inequality in a digital world (EDIT) Page 06 - (Social Issues) - ([02:27](https://youtube.com/watch?v=_iNzqMxT0zQ&t=147))
2. On the edge (Column) + Over 300 Palestinians hurt in Jerusalem holy site clashes Pages 06, 13 - (International Relations) - ([12:07](https://youtube.com/watch?v=_iNzqMxT0zQ&t=727))
3. A national health service in India Page 06 - (Social Issues) - ([28:11](https://youtube.com/watch?v=_iNzqMxT0zQ&t=1691))
4. Naval ships bring in medical supplies Page 08 - (Security) - ([30:02](https://youtube.com/watch?v=_iNzqMxT0zQ&t=1802))
5. QOD - ([31:23](https://youtube.com/watch?v=_iNzqMxT0zQ&t=1883))

#Healthinindia #Israelpalestine #Naval #Allindiaservices 



/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS
GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Indian Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-12-04-21-PDF
Word - http://bit.ly/DNS-NOTES-12-04-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. QIP 2021 Announcement – ([00:43](https://youtube.com/watch?v=DwYy3Y08C9M&t=43))
2.Data Protection Bill - Polity & Governance –  ([01:14](https://youtube.com/watch?v=DwYy3Y08C9M&t=74))
(Key Features and Concerns)
3. Backchannel Diplomacy Between India and Pak – International Relations  – ([19:04](https://youtube.com/watch?v=DwYy3Y08C9M&t=1144))
(Efficacy of Backchannel diplomacy)
4. Nuclear Facilities of Iran – (International Relations) - ([28:24](https://youtube.com/watch?v=DwYy3Y08C9M&t=1704))
(Places in news)
Reference – (30:28)
5. India – US Maritime issue  – (International Relations) 
6. Question for the Day – ([30:41](https://youtube.com/watch?v=DwYy3Y08C9M&t=1841))

#DataProtection #Iran #Diplomacy #IndiaPAK #Fiduciary 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1153/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Vaibhav Mishra of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021
►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-12-06-21-PDF
WORD - http://bit.ly/DNS-NOTES-12-06-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Increasing Chinese influence in India's Neighbourhood - (International Relations) - ([00:44](https://youtube.com/watch?v=tzpO5R3rW8E&t=44))
2. Modification to India's Vaccine Policy - (Social Issues) - ([13:54](https://youtube.com/watch?v=tzpO5R3rW8E&t=834))
3. Coup in Myanmar - (International Relations) - ([21:28](https://youtube.com/watch?v=tzpO5R3rW8E&t=1288))
4. Index of Industrial Production - (Economy) - ([31:15](https://youtube.com/watch?v=tzpO5R3rW8E&t=1875))
5. International Court of Justice - (International Relations) - ([35:29](https://youtube.com/watch?v=tzpO5R3rW8E&t=2129))
6. QOD - ([41:43](https://youtube.com/watch?v=tzpO5R3rW8E&t=2503))

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1211/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Faizan Khan, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  

About – Pre–Pareekshan - http://bit.ly/Pre-Pareekshan-2021
Pre–Pareekshan Registration Link - https://elearn.rauias.com/home-page/ 




Video - https://youtu.be/KfmZdTpd3RA
 
►Link UPSC MAINS 2020 GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4


►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-12-03-2021-PDF
Word - http://bit.ly/DNS-Notes-12-03-2021-Word

►TODAY’S THE HINDU ANALYSIS
1. Announcement of PRE-PAREEKSHAN - ([00:15](https://youtube.com/watch?v=ZEBGqaFBijE&t=15)) 
2. EC files FIR over fake news on hacking of EVMs – (Polity & Governance) – ([02:20](https://youtube.com/watch?v=ZEBGqaFBijE&t=140))
(ECI EVM’s are unhackable)
3. India, Japan space agencies review ties – (Science & Technology) – ([31:12](https://youtube.com/watch?v=ZEBGqaFBijE&t=1872))
(Lunar Mission to the South Pole of the moon)
4. Lingaraja temple  – (Art & Culture) – ([37:05](https://youtube.com/watch?v=ZEBGqaFBijE&t=2225))
    (Kalinga Architecture) 
5. Speed Test - ([40:55](https://youtube.com/watch?v=ZEBGqaFBijE&t=2455))
6. Question for the day – (Science & Technology) – ([42:57](https://youtube.com/watch?v=ZEBGqaFBijE&t=2577))


#EVMsAreUnhackable #Lunar Mission  #SouthPoleOfMoon #Kalinga Architecture


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1107/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-12-05-21-PDF
Word - http://bit.ly/DNS-NOTES-12-05-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Optional Subject Selection orientation announcement – ([00:15](https://youtube.com/watch?v=FpwP5xzwqXY&t=15))
2. Maratha quota: Uddhav writes to President – Polity & Governance – ([02:26](https://youtube.com/watch?v=FpwP5xzwqXY&t=146))
(Reservation issues)
3. What’s happening in Jerusalem? – International Relation – ([12:07](https://youtube.com/watch?v=FpwP5xzwqXY&t=727))
(Israel, Palestine and issues around Jerusalem)
4. MFI sector urges RBI backing for emergency credit support – Economy – ([28:39](https://youtube.com/watch?v=FpwP5xzwqXY&t=1719))
(Financial distress in MFI)
5. Evaluate the Ladakh crisis, keep China at bay – (International Relations) - ([40:35](https://youtube.com/watch?v=FpwP5xzwqXY&t=2435))
(Tackling China)
6. Question for the Day (Polity & Governance) – ([52:40](https://youtube.com/watch?v=FpwP5xzwqXY&t=3160))

#MarathaReservation #FinancialDistressInMFI #IsraelPalestineAndJerusalem # TacklingChina

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bharadwaj of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS
►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021
 ►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
http://bit.ly/DNS-NOTES-13-04-21-PDF
http://bit.ly/DNS-NOTES-13-04-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Retail inflation quicken to 5.52% - (Indian economy)- ([1:20](https://youtube.com/watch?v=lK41hHOBkVQ&t=80))
2. India's South Asian Opportunity– (India's relation with its neighbours) - ([19:59](https://youtube.com/watch?v=lK41hHOBkVQ&t=1199))
3. Chandra Appointed Chief election commissioner – Indian Polity-  (constitutional bodies) - ([29:40](https://youtube.com/watch?v=lK41hHOBkVQ&t=1780))
4. Military exercise in Bangladesh ends- (International relations and national security)- ([35:20](https://youtube.com/watch?v=lK41hHOBkVQ&t=2120))
5. QOD - ([37:51](https://youtube.com/watch?v=lK41hHOBkVQ&t=2271))

#CPI #WPI #IIP #Inflation #SouthAsia #electioncommission #militaryexercises #retailinflation #coreinflation #HeadlineInflation #india-Pakistan


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1154/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Vaibhav Mishra , 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

Link to our official Telegram Channel: https://t.me/rausias1953
Pre–Pareekshan Registration Link - https://elearn.rauias.com/home-page/

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA

 ►DOWNLOAD DNS NOTES
PDF: http://bit.ly/DNS-NOTES-13-03-21-PDF
WORD: http://bit.ly/DNS-NOTES-13-03-21-WORD 

►TODAY’S THE HINDU ANALYSIS
Pre-Parikshan Announcement - (00:46)
1. Rajasthan Information Commission penalises five officials for negligence Page 01 - (Ethics, Polity and Governance) - ([02:11](https://youtube.com/watch?v=JR30F_ngY8s&t=131))
Historical Build up of RTI- ([02:11](https://youtube.com/watch?v=JR30F_ngY8s&t=131))
Basic Provisions - (08:27)
Issues in implementation - (18:22) 
2. Stop influx from Myanmar: Centre Page 10 - (International Relations) - ([32:49](https://youtube.com/watch?v=JR30F_ngY8s&t=1969))
3. Bureaucrats cannot be State Election Commissioners: SC Page 01 -(Polity and Governance) - ([42:33](https://youtube.com/watch?v=JR30F_ngY8s&t=2553))
4. Quad leaders for ‘open, free’ Indo-Pacific Page 01 - (International Relations) - ([45:49](https://youtube.com/watch?v=JR30F_ngY8s&t=2749))
5. Casting the Asian dice on a West Asia board Page 06 - (Security) - ([51:40](https://youtube.com/watch?v=JR30F_ngY8s&t=3100))
6.  The new media rules are a tightening noose Page 06 - (Polity and Governance) - ([54:19](https://youtube.com/watch?v=JR30F_ngY8s&t=3259))
7. QOD - ([57:23](https://youtube.com/watch?v=JR30F_ngY8s&t=3443))

#Myanmar #RTI #righttoinformation #stateelections #Panchayati Raj #desertflag 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1126/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mrs. Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link for Mains (Weekly) Answer Practice: 
https://elearn.rauias.com/mains-daily-questions/in-spite-of-various-initiatives-taken-by-the-government-for-skilling-youths-instead-of-benefits-the-/609d08558f042c050874785e/ 

►Link for optional counselling session: Open Counselling Session (rauias.com)



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF -  http://bit.ly/DNS-NOTES-13-05-21-PDF
Word -  http://bit.ly/DNS-NOTES-13-05-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Most NGOs don’t have SBI account – (Social justice) – ([03:00](https://youtube.com/watch?v=dbqL_NqoDRg&t=180))
2. Child marriages may go unnoticed amid lockdown – (Social justice) - ([19:55](https://youtube.com/watch?v=dbqL_NqoDRg&t=1195))
3. A bullet train to hunger – (Society) ([28:15](https://youtube.com/watch?v=dbqL_NqoDRg&t=1695))
4. Question for the Day – ([38:39](https://youtube.com/watch?v=dbqL_NqoDRg&t=2319))
#ngo #nonprofit #nongovernmental #socialsecurity #child #marriage #childmarriage #cybersecurity #prison #reform

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1181/
 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►https://www.rauias.com/optional-session



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
WORD FILE: http://bit.ly/DNS-NOTES-13-06-21-WORD
PDF FILE: http://bit.ly/DNS-NOTES-13-06-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. Operation Olivia to rescue of Olive Ridley – threatened species - environment and ecology - ([00:43](https://youtube.com/watch?v=-3psPjbrbLo&t=43))
2. Appeal for CBI probe into rosewood felling in Kerala – Indian flora and fauna – Environment and ecology - ([06:21](https://youtube.com/watch?v=-3psPjbrbLo&t=381))
3. Bitcoin push (India’s status) – Indian economy and polity -  ([09:48](https://youtube.com/watch?v=-3psPjbrbLo&t=588))
 4. Revised subsidies to spur EV demand – Major government schemes for climate change - ([16:36](https://youtube.com/watch?v=-3psPjbrbLo&t=996))
5. Scheme for revision (National Mission on Clean Ganga) – ([21:09](https://youtube.com/watch?v=-3psPjbrbLo&t=1269))

6. Question for the Day – ([24:04](https://youtube.com/watch?v=-3psPjbrbLo&t=1444))

#operationolivia #oliveridley #odishabeach #eastcoastofindia #indiancoastguard #CBI #wayanad #rosewood #kerala #sitonrosewood #bitcoins #elsalvador #cryptocurrency #bitcoinsinindia #electricvehicles #fame1 #fame2 #NEMMP #NMCG #Namamigange #ganga

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021


►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-14-04-21-PDF
Word - http://bit.ly/DNS-NOTES-14-04-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. QIP announcement – ([00:15](https://youtube.com/watch?v=7Lb-m8sxWtU&t=15))
2. BIMSTEC needs to reinvent itself – International Relation – ([01:10](https://youtube.com/watch?v=7Lb-m8sxWtU&t=70))
(BIMSTEC – India’s strategic importance)
3. Last two rhinos translocated under IRV 2020 – Economy – ([10:10](https://youtube.com/watch?v=7Lb-m8sxWtU&t=610))
(Rhino India Vision 2020)
4. Festive Moment – Economy – ([15:10](https://youtube.com/watch?v=7Lb-m8sxWtU&t=910))
(Bihu Festival)
5. Speed Test – ([16:39](https://youtube.com/watch?v=7Lb-m8sxWtU&t=999))
6. Question for the Day – ([20:00](https://youtube.com/watch?v=7Lb-m8sxWtU&t=1200))

# BIMSTECIndia’sStrategicImportance #RhinoIndiaVision2020 #Bihu

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 

►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021 

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021


►DOWNLOAD DNS NOTES

PDF Link: http://bit.ly/DNS-Notes-14-06-21-PDF
Word Link: http://bit.ly/DNS-Notes-14-06-21-Word

►TODAY’S THE HINDU ANALYSIS
1. Emerging challenges of Cyber Security – (Security) –  ([00:55](https://youtube.com/watch?v=xhlSBX9APls&t=55))

2. G7 - Summit – (International Relations) – ([16:25](https://youtube.com/watch?v=xhlSBX9APls&t=985))
a) G7 members 
b) Outcomes of the summit - B3W project 
c) D 10 - Coalition 
d) Opportunities and Challenges for India from D 10 

3. Bio Weapons - emerging threat to security – (Security) – ([27:27](https://youtube.com/watch?v=xhlSBX9APls&t=1647))

4. Rare Earth Metals – (Science & Technology) – ([37:09](https://youtube.com/watch?v=xhlSBX9APls&t=2229))

5. Tree of coffee discovered in Andaman – (Environment) – ([40:21](https://youtube.com/watch?v=xhlSBX9APls&t=2421))

6. Question for the Day – ([41:52](https://youtube.com/watch?v=xhlSBX9APls&t=2512))



#G7 #Bioweapon #D10 #CyberSecurity #B3W #Andaman #Coffee #rareearth 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1213/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter & Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Pre–Pareekshan Registration Link - https://elearn.rauias.com/home-page/

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNSS-NOTES-14-03-21-PDF
Word - http://bit.ly/DNS-NOTESS-14-03-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Pre-Pareekshan Announcement – ([00:26](https://youtube.com/watch?v=H4V0S6gcTLQ&t=26))
1. Reaffirm Places of Worship Act - SC tells Centre – Polity & Governance - ([02:48](https://youtube.com/watch?v=H4V0S6gcTLQ&t=168))
 (Secularism, Places of Worship Act, 1991, Cut-off date – 15th August, 1947)
2. Martian Blueberries find a parallel on Earth – Science & Technology – ([14:39](https://youtube.com/watch?v=H4V0S6gcTLQ&t=879))
(Martian Blueberries, Hematite Rocks, Kutch)
3. Drinking water quality testing, monitoring & surveillance’ framework – Governance – ([19:50](https://youtube.com/watch?v=H4V0S6gcTLQ&t=1190))
(Testing Water Quality, Jal Jeevan Mission, NABL)
4. Raman thermometry & Transmission Line Monitoring – Science & Technology – ([24:20](https://youtube.com/watch?v=H4V0S6gcTLQ&t=1460))
(Raman thermometry, Transmission Lines’ Health)
5. Gregarious bamboo flowering in Wayanad poses threat – Environment – ([37:38](https://youtube.com/watch?v=H4V0S6gcTLQ&t=2258))
(Waynad, Continuous Flowering, Sporadic Flowering, Gregarious Flowering)
6. Question for the Day – ([44:40](https://youtube.com/watch?v=H4V0S6gcTLQ&t=2680))
#WorshipAct #Secularism #Cutoffdate #15August1947 #MartianBlueberries #HematiteRocks #Kutch #WaterQualityTest #NABL #Ramanthermometry #Waynad #Bambooflowering 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1127/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►https://www.rauias.com/optional-session

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021
►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
http://bit.ly/DNS-NOTES-14-05-2021-WORD
http://bit.ly/DNS-NOTES-14-05-2021-PDF

►TODAY’S THE HINDU ANALYSIS
1. Reforming Medical education– education in India - ([02:27](https://youtube.com/watch?v=_mDql8rywzw&t=147))
2. Assam NRC authority seeks re-verification of citizens’ list- Migration and Citizenship – Indian society and polity- ([08:37](https://youtube.com/watch?v=_mDql8rywzw&t=517))
3. App to view SC proceeding launched – Indian Judicial reforms -  ([23:41](https://youtube.com/watch?v=_mDql8rywzw&t=1421))
4. Scheme for revision (Bhartiya Jan Aushadi Pariyojana) - ([34:44](https://youtube.com/watch?v=_mDql8rywzw&t=2084))
5. Question for the Day – ([41:53](https://youtube.com/watch?v=_mDql8rywzw&t=2513))

#Medicaleducaiton #NRC #NPR #Citizenship #Supremecourt #newapps #PMBJAP #Judicialreforms ##assam #constitutionofIndia

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS
GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 

►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021 

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021


►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-15-04-21-PDF
Word - http://bit.ly/DNS-NOTES-15-04-21-WORD
►TODAY’S THE HINDU ANALYSIS
1.International Covenant on Economic, social and cultural rights – (Polity & Governance) –  ([01:51](https://youtube.com/watch?v=Fk_yd2J_0n8&t=111))
2. Section 377 – LGBT Community – (Social issues)  – ([10:39](https://youtube.com/watch?v=Fk_yd2J_0n8&t=639))
(Various Judgements)
3. RCEP  – (International Relations) - ([16:55](https://youtube.com/watch?v=Fk_yd2J_0n8&t=1015))
(Why India did not Join?)
4. S – 400 Missile defense system  – (International Relations) – ([40:11](https://youtube.com/watch?v=Fk_yd2J_0n8&t=2411))
5. NATO – (International Relations) – ([46:31](https://youtube.com/watch?v=Fk_yd2J_0n8&t=2791))
Reference – (50:39)
6. Stagflation - Economy
7. UNCLOS – International Relations 
8. Question for the Day – ([51:16](https://youtube.com/watch?v=Fk_yd2J_0n8&t=3076))

#RCEP #S-400 #NATO #Afghanistan #LGBT #Section377 #HumanRights 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1156/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mrs. Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link for optional counselling session: Open Counselling Session (rauias.com)



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF -  http://bit.ly/DNS-Notes-15-06-21-PDF
Word -  http://bit.ly/DNS-Notes-15-06-21-Word

►TODAY’S THE HINDU ANALYSIS
1.   ‘China, India, Pak. expanding nuclear arsenal’ – (International relations) –([00:40](https://youtube.com/watch?v=-JGeN5o69rg&t=40))
2. India’s nuclear security challenge – (Mains Question discussion) – ([07:40](https://youtube.com/watch?v=-JGeN5o69rg&t=460))
3. G7 accommodates Indian stand on need for Internet curbs  – (International relations) - ([22:40](https://youtube.com/watch?v=-JGeN5o69rg&t=1360))
4. Unlocking war histories with a purpose – (Culture) – ([29:48](https://youtube.com/watch?v=-JGeN5o69rg&t=1788))
5. NCPCR warns portals on illegal adoption – (Social Justice) – ([35:45](https://youtube.com/watch?v=-JGeN5o69rg&t=2145))
6. Question for the Day – ([37:28](https://youtube.com/watch?v=-JGeN5o69rg&t=2248))
#sipri #nuclear #ncpcr #G7 #mains #upsc 

/IAS - ELEARN 
▪ eLearn - All our content is available on our learning platform - https://elearn.rauias.com 


►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/category/daily-news-headlines/ 

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Naweed Akhtar, Basava Uppin and Vaibhav Mishra 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

About – Pre–Pareekshan - http://bit.ly/Pre-Pareekshan-2021
Pre–Pareekshan Registration Link - https://elearn.rauias.com/home-page/


First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA
 
►Link UPSC MAINS 2020 GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4


►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-15-03-21-PDF
Word - http://bit.ly/DNS-NOTES-15-03-21-WORD

►TODAY’S THE HINDU ANALYSIS

1. Announcement of PRE-PAREEKSHAN - ([00:15](https://youtube.com/watch?v=W9GxHPxWRr4&t=15)) 
2. Centre likely to allow residents to fill their NPR details online – (Polity & Governance) – ([03:49](https://youtube.com/watch?v=W9GxHPxWRr4&t=229))
(Issues surrounding NRC & NPR)
3. Forestalling a cyber–Pearl Harbour – (Polity & Governance + Science & Technology) – ([28:43](https://youtube.com/watch?v=W9GxHPxWRr4&t=1723))
(Cyber Security)
3. Summit spirit – (International Relation) – ([55:38](https://youtube.com/watch?v=W9GxHPxWRr4&t=3338))
    (Quad) 
4.Salutary reminder  – (Indian Economy – (01:04:09)
    (Price Stability  - CPI & WPI)
5. Question for the day – (International Relation) – (01:12:20)


#NRC  #NPR #Lunar Mission  #CyberSecurity #QUAD #CPI #WPI


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1107/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registratio... 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021
►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-15-05-21-PDF
Word - http://bit.ly/DNS-NOTES-15-05-21-WORD
►TODAY’S THE HINDU ANALYSIS
1. Optional Announcement - ([00:31](https://youtube.com/watch?v=RX1-GzAHIa0&t=31)) 
2. Need for Universal Health Coverage (UHC) for India – Social Justice – ([02:36](https://youtube.com/watch?v=RX1-GzAHIa0&t=156))
(Need for Universal Health Coverage, Principles & Architect for UHC)
3. PVTGs of Odisha – Polity & Governance – ([27:24](https://youtube.com/watch?v=RX1-GzAHIa0&t=1644))
(75 PVTG, Ministry of Tribal Affairs, Central Sponsored Scheme)
4. US slider turtles pose a threat in Northeast – Ecology & Biodiversity – ([36:42](https://youtube.com/watch?v=RX1-GzAHIa0&t=2202))
(Red-eared Sliders, US, Turtles, Invasive Species)
5. Exports surge in April on low-base effect – Economy – ([39:58](https://youtube.com/watch?v=RX1-GzAHIa0&t=2398))
(Base Effect)
6. Question for the Day – ([44:29](https://youtube.com/watch?v=RX1-GzAHIa0&t=2669))
#UHC #UniversalHealthCoverage #PVTG #Odisha #TribalAffairs #Odisha #CentrallySponsoredScheme #RedearedSliders #US #Turtles #InvasiveSpecies #BaseEffect


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1183/ 
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Basava Uppin of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021  ►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-16-06-21-PDF
Word - http://bit.ly/DNS-NOTES-16-06-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Not Cooking with Gas – Governance ([00:31](https://youtube.com/watch?v=knLPC56_X0A&t=31))
A. Energy Poverty in India
B. PM Ujjwala Yojana: Benefits, Challenges and way forward 
2. The Invisible Tax– Economy ([15:14](https://youtube.com/watch?v=knLPC56_X0A&t=914))
3. Needed Full disclosure on Electoral Bonds – Governance ([32:23](https://youtube.com/watch?v=knLPC56_X0A&t=1943))
4. America's Mistakes in forever War– International Relations ([47:12](https://youtube.com/watch?v=knLPC56_X0A&t=2832))
5. Embracing Cryptocurrency – Economy – reference
     YouTube link: https://www.youtube.com/watch?v=-3psPjbrbLo&t=634s ( Status of Bitcoin in India)
6. Question for the Day ([49:46](https://youtube.com/watch?v=knLPC56_X0A&t=2986))

#EnergyPoverty #UjjwalaYojana #Inflation #ElectoralBonds #IndoAfghanTies

 
https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1215/ 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Nagendra Pratap, Faizan Khan and Basava Uppin, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 



About – Pre–Pareekshan - http://bit.ly/Pre-Pareekshan-2021

Pre–Pareekshan Registration Link - https://elearn.rauias.com/home-page/

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA
 
►Link UPSC MAINS 2020 GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4


►DOWNLOAD DNS NOTES
PDF -  http://bit.ly/DNS-Notes-16-03-21-PDF
Word - http://bit.ly/DNS-Notes-16-03-21-Word

►TODAY’S THE HINDU ANALYSIS
1. Announcement of PRE-PAREEKSHAN - ([00:15](https://youtube.com/watch?v=xLsVq5LTCkM&t=15)) 
2. ‘Bee fences’ to ward off elephant attacks – (Environment & Ecology) – ([02:22](https://youtube.com/watch?v=xLsVq5LTCkM&t=142))
(Techniques to avoid man-animal conflict)
3. Differentiated Banks in India – (Indian Economy) – ([12:50](https://youtube.com/watch?v=xLsVq5LTCkM&t=770))
(Payment Banks and Differentiated Banks)
4.‘India’s arms imports down by 33%’ – (International Relation) – ([24:13](https://youtube.com/watch?v=xLsVq5LTCkM&t=1453))
    (Arms Import) 
5.SC bats for Great Indian Bustard  – (Indian Economy) – ([29:25](https://youtube.com/watch?v=xLsVq5LTCkM&t=1765))
    (Great Indian Bustard )
6. The limits of POCSO - (Polity & Governance) - ([35:30](https://youtube.com/watch?v=xLsVq5LTCkM&t=2130))
(Suggestion for amendments in POSCO act)
7. Question for the day – (Environment & Ecology) – ([45:24](https://youtube.com/watch?v=xLsVq5LTCkM&t=2724))


#BeeFences  #India’sArmsImports #POCSO #GreatIndianBustard 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1107/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF – http://bit.ly/DNS-Notes-16-05-21-PDF
Word - http://bit.ly/DNS-Notes-16-05-21-Word

►TODAY’S THE HINDU ANALYSIS
1. Optional Subject Selection orientation announcement – ([00:15](https://youtube.com/watch?v=9rItSX_zsf0&t=15))
2. Mars landing gives China’s space programme a leg-up – Science & Technology - ([02:22](https://youtube.com/watch?v=9rItSX_zsf0&t=142))
(Mars Missions)
3. Two killed in Kerala as cyclone intensifies – Physical Geography – ([10:10](https://youtube.com/watch?v=9rItSX_zsf0&t=610))
(Increasing Cyclone formation in Arabian Sea)
4. New approach combines biologics, antibody-drug conjugates – Science & Technology – ([22:51](https://youtube.com/watch?v=9rItSX_zsf0&t=1371))
(Protein–Antibody Conjugates)
5. Question for the Day (Physical Geography) – ([30:15](https://youtube.com/watch?v=9rItSX_zsf0&t=1815))

# MarsMissions #MoreCyclonesInArabianSea # ProteinAntibodyConjugates

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bharadwaj of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-16-04-21-PDF
WORD - http://bit.ly/DNS-NOTES-16-04-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Centre to push judges appointments- Indian Polity-  ([01:24](https://youtube.com/watch?v=iRQ1xbn0Xro&t=84)) 
(Indian Judiciary)
2. Annuity clause goes for NPS subscribers – Indian economy- Government schemes - ([06:47](https://youtube.com/watch?v=iRQ1xbn0Xro&t=407))
3. UNFPA's population report launched – International reports – ([11:52](https://youtube.com/watch?v=iRQ1xbn0Xro&t=712))
4. WPI inflation quicken to a record 7.4% – Indian economy – inflation rates and indices- ([16:33](https://youtube.com/watch?v=iRQ1xbn0Xro&t=993))
5. Centre eases guidelines for OCI cardholders – Indian polity- International relations - Indian diaspora - ([17:09](https://youtube.com/watch?v=iRQ1xbn0Xro&t=1029))
6. India restore eVisa for 156 countries – Indian polity - International relations - Indian diaspora – ([22:40](https://youtube.com/watch?v=iRQ1xbn0Xro&t=1360))
7. Multipolarity scripted by the middle powers - international relations - ([25:56](https://youtube.com/watch?v=iRQ1xbn0Xro&t=1556))
8. Question for the Day – ([35:45](https://youtube.com/watch?v=iRQ1xbn0Xro&t=2145))

#judgesappointment #supremecourt #Highcourts #NPS #OCI #UNFPA #population #WPI #eVisa #QUAD #inflation 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1143/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mrs Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF -  http://bit.ly/DNS-NOTES-17-04-21-PDF
Word - http://bit.ly/DNS-NOTES-17-04-21-Word
►TODAY’S THE HINDU ANALYSIS
1. Meth, gold and arms — a force to stop them all – (Security)– ([00:38](https://youtube.com/watch?v=NZvHhwkADG0&t=38))
(Kaladan multi-modal project, northeast insurgency issues and solutions)
2. India likely to receive normal monsoon: IMD–(Geography) – ([20:11](https://youtube.com/watch?v=NZvHhwkADG0&t=1211))
(Indian ocean diapole, La Nina , El Nino)
3. Exiting Afghanistan– (International Relations) – ([32:00](https://youtube.com/watch?v=NZvHhwkADG0&t=1920))
(Problems with Indian Agriculture, State Intervention for Agriculture)
4. Question for the Day – ([33:48](https://youtube.com/watch?v=NZvHhwkADG0&t=2028))
 
#insurgents #northeastindia #Myanmar #kaladamproject #connectivity #afghanistan #security #neighbourhoodfirst #southasia 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1158/
 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Faizan Khan of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1   
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020
   


Video - https://youtu.be/KfmZdTpd3RA

► Prelims Compass Link
1. Indian Polity & Governance
(Amazon Link: https://amzn.to/3gChrUJ)
 
2. Science & Technology
(Amazon Link: https://amzn.to/3q2lldH)
 
3. Budget & Economic Survey
(Amazon Link: https://amzn.to/3wys01Z)
 
4. Government Schemes
(Amazon Link: https://amzn.to/3q4jIfS)
 
5. International Relations
(Amazon Link: https://amzn.to/3gDvyJw)
 
6. Indian Economy
(Amazon Link: https://amzn.to/3qesmsh)
 
7. History & Culture of India
(Amazon Link: https://amzn.to/3iS2bpp)
 
8. Environment, Ecology & Biodiversity
(Amazon Link: https://amzn.to/2U3VIgI)

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-17-06-21-PDF
Word – http://bit.ly/DNS-NOTES-17-06-21-WORD

►TODAY’S THE HINDU ANALYSIS
1.DNS Weekly Answer Writing accouchement – ([00:15](https://youtube.com/watch?v=3XN-lGC9nDs&t=15))  
2. Energy inefficiency can short circuit cooling India – Economy + Environment - ([01:10](https://youtube.com/watch?v=3XN-lGC9nDs&t=70)) 
(Energy Efficiency)
3. Conditions not favorable for monsoon to cover North: IMD – Physical Geography – ([19:38](https://youtube.com/watch?v=3XN-lGC9nDs&t=1178))
(Advancement of Monsoon to North & N-W India)
4. Chennai zoo loses another lion to virus – Science & Technology – ([25:12](https://youtube.com/watch?v=3XN-lGC9nDs&t=1512))
(Vulnerability of Lions to Corona virus)
5. Acquitted but not forgotten – Polity & Governance – ([28:54](https://youtube.com/watch?v=3XN-lGC9nDs&t=1734))
(Right to be forgotten)
6. Question for the Day – Physical Geography - ([36:22](https://youtube.com/watch?v=3XN-lGC9nDs&t=2182))

# EnergyEfficiency # AdvancementOfMonsoonToNorth&N-WIndia #VulnerabilityOfLionsToCoronaVirus #RightToBeForgotten


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1143/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►About – Pre–Pareekshan - http://bit.ly/Pre-Pareekshan-2021
►Pre–Pareekshan Registration Link - https://elearn.rauias.com/home-page/

►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS
GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-17-03-2021-PDF
Word - http://bit.ly/DNS-Notes-17-03-2021-Word

►TODAY’S THE HINDU ANALYSIS
1. Pre-Pareekshan Announcement – ([00:26](https://youtube.com/watch?v=HxKYBwHV9YA&t=26))
2. Responsible AI - the need for ethical guard rails – Governance + Technology - ([02:48](https://youtube.com/watch?v=HxKYBwHV9YA&t=168)) 
(i) Applications of AI – (08:49)
(ii) Challenges which needs to be addressed – (15:25)  
(iii) Reports - UN/UNESCO/NITI Aayog – (21:54)
 (Artificial Intelligence, Reports of NITI Aayog, UN & UNESCO)
3. U.K. turns to Indo-Pacific in post-Brexit foreign policy – International Relations – ([29:14](https://youtube.com/watch?v=HxKYBwHV9YA&t=1754))
(India-UK ties, Indo-Pacific, QUAD Plus)
4. Delhi remains most polluted Capital – Environment – ([36:16](https://youtube.com/watch?v=HxKYBwHV9YA&t=2176))
(World Air Quality Report, IQ Air)
5. Census interim data by 2024 (Reference) – Polity & Governance – ([38:27](https://youtube.com/watch?v=HxKYBwHV9YA&t=2307))
(Raman thermometry, Transmission Lines’ Health)
6. Question for the Day – ([40:47](https://youtube.com/watch?v=HxKYBwHV9YA&t=2447))
#AI #NITIAayog #AI-Report #ResponsibleAI #UsesofAI #Ethics-AI #India-UK #QuadPlus #AirIQReport #AirIQ #Census2021 #Census  


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1130/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registratio... 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass:  


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-17-05-21-PDF
Word - http://bit.ly/DNS-NOTES-17-05-21-WORD
►TODAY’S THE HINDU ANALYSIS
1. Optional Announcement - ([00:31](https://youtube.com/watch?v=jcB5_Wcblsc&t=31)) 
2. Compulsory Licensing in India & under TRIPS – Science & Technology – ([02:40](https://youtube.com/watch?v=jcB5_Wcblsc&t=160))
(Compulsory Licencing, TRIPS, Indian Patents Act)
3. Ways to improve Female LFPR – Economy – ([19:54](https://youtube.com/watch?v=jcB5_Wcblsc&t=1194))
(Female Labour Force Participation Rate)
4. Disengagement at Ladakh is paved with disruptions – International Relations – ([33:49](https://youtube.com/watch?v=jcB5_Wcblsc&t=2029))
(India-China Border disengagement)
5. Restructuring Tribunals by establishing National Tribunal Commission – Polity & Governance – ([42:21](https://youtube.com/watch?v=jcB5_Wcblsc&t=2541))
(2021 Ordinance, National Tribunal Commission)
6. Question for the Day – ([52:05](https://youtube.com/watch?v=jcB5_Wcblsc&t=3125))
#CompulsoryLicencing #CL #TRIPS #IndianPatentsAct #LFPR #Disengagement #Tribunals #NTC #NationalTribunalCommission #2021Ordinance 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1185/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021

►New Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021


►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-18-04-21-PDF
Word - http://bit.ly/DNS-nOTES-18-04-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. QIP Announcement – ([00:20](https://youtube.com/watch?v=b7X1J6HE76g&t=20))
2. Speed Test – ([01:14](https://youtube.com/watch?v=b7X1J6HE76g&t=74))
3.‘ ARIES facility will host the support centre for Aditya-L1– Science & Technology – ([01:47](https://youtube.com/watch?v=b7X1J6HE76g&t=107))
(Aditya-L1)
4. Jaishankar to visit Abu Dhabi today – International Relations – ([14:57](https://youtube.com/watch?v=b7X1J6HE76g&t=897))
(UAE Brokered India-Pakistan Trade deal)
5. Copyright war – Polity & Governance – ([24:17](https://youtube.com/watch?v=b7X1J6HE76g&t=1457))
(Fair use of copyrights)
6. Question for the Day – ([27:17](https://youtube.com/watch?v=b7X1J6HE76g&t=1637))

# Aditya-L1 # UAEBrokeredIndia-PakistanTradeDeal # FairUseOfCopyrights

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registratio... 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass:  


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-PDF-18-06-21
Word - http://bit.ly/DNS-NOTES-WORD-18-06-21
►TODAY’S THE HINDU ANALYSIS
1. Judicial Pushback for draconian legal regime – UAPA – Polity & Governance – ([00:57](https://youtube.com/watch?v=T_qzBM3CHuY&t=57))
(Granting Bail to an accused charged under UAPA, Section 43D(5) of UAPA)
2. Measures Needed to overcome Economic Recession – Economy – ([12:25](https://youtube.com/watch?v=T_qzBM3CHuY&t=745))
(Cyclical Fiscal Policy, Modern Monetary Theory, Conventional Macro-Economics, Deficit Financing)
3. Use of Technology for India's Health Sector – Science & Technology – ([27:18](https://youtube.com/watch?v=T_qzBM3CHuY&t=1638))
(Blockchain Technology, 5G, AI, IoMT, Cloud & Quantum Computing – Health Sector)
4. Deepor Beel - Important Bird Area – Environment – ([33:58](https://youtube.com/watch?v=T_qzBM3CHuY&t=2038))
(Important Bird Areas, Birdlife International, Deepor Beel)
5. Chinese astronauts dock with new space station – Science & Technology – ([39:03](https://youtube.com/watch?v=T_qzBM3CHuY&t=2343))
(Shenzou-12 Spaceship, Tianhe Space Station, Gobi Desert, Jiuquan Launch Centre)
6. Question for the Day – ([40:36](https://youtube.com/watch?v=T_qzBM3CHuY&t=2436))
#UAPA #Bail #Rights #PrivateInvestment #Countercyclicalfiscalpolicy #MMT #ModernMonetaryTheory #ConventionalMacroEconomics #DeficitFinancing #DisruptiveTechnology #HealthSctor #ImportantBirdAreas #BirdlifeInternational #DeeporBeel #Shenzou12 #Tianhe #SpaceStation #GobiDesert #JiuquanLaunchCentre

► Prelims Compass Link 
1. Indian Polity & Governance - Amazon Link: https://amzn.to/3gChrUJ

2. Science & Technology - Amazon Link: https://amzn.to/3q2lldH

3. Budget & Economic Survey - Amazon Link: https://amzn.to/3wys01Z

4. Government Schemes - Amazon Link: https://amzn.to/3q4jIfS

5. International Relations - Amazon Link: https://amzn.to/3gDvyJw

6. Indian Economy - Amazon Link: https://amzn.to/3qesmsh

7. History & Culture of India - Amazon Link: https://amzn.to/3iS2bpp

8. Environment, Ecology & Biodiversity - Amazon Link: https://amzn.to/2U3VIgI


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1217/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Vaibhav Mishra and Basava Uppin, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

Link to our telegram Channel: https://web.telegram.org/#/im?p=@rausias1953

Link to weekly mains assignment: https://elearn.rauias.com/mains-daily-questions/q-identify-the-reasons-for-the-increase-in-human-elephant-conflict-hec-and-suggest-measures-to-minim/6051be847737d42685d17d1f/

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF  - http://bit.ly/DNS-Notes-18-03-21-PDF
Word - http://bit.ly/DNS-Notes-18-03-21-Word


►TODAY’S THE HINDU ANALYSIS
*Announcements:*
1. Pre-Parkishan - ([00:38](https://youtube.com/watch?v=i_iRdw3cXcs&t=38))
2. Weekly Mains Assignment - ([02:03](https://youtube.com/watch?v=i_iRdw3cXcs&t=123))

*Prelims Analysis*
1. Aligning a missile deal with destination Manila Page 06 - (Science and Technology) - ([02:28](https://youtube.com/watch?v=i_iRdw3cXcs&t=148))
2. Quick decisive steps required to curb second peak Page 01 - (Science and Technology) - ([20:22](https://youtube.com/watch?v=i_iRdw3cXcs&t=1222))
3. Appropriation Bill gets the nod of Lok Sabha Page 09 - (Polity and Governance) - ([24:49](https://youtube.com/watch?v=i_iRdw3cXcs&t=1489))
4. Re-evaluating inflation targeting Page 07 - (Economy) - ([28:08](https://youtube.com/watch?v=i_iRdw3cXcs&t=1688))
5. A Gandhian Route in Myanmar Page 06 - (History) - ([31:37](https://youtube.com/watch?v=i_iRdw3cXcs&t=1897))
*Mains Analysis*
6. Re-evaluating inflation targeting Page 06 (Economy) - ([32:45](https://youtube.com/watch?v=i_iRdw3cXcs&t=1965))
7. QOD - ([50:27](https://youtube.com/watch?v=i_iRdw3cXcs&t=3027))


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1114/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mr. Jatin Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►https://www.rauias.com/optional-session



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
http://bit.ly/DNS-NOTES-18-05-2021-PDF
http://bit.ly/DNS-NOTES-18-05-2021-WORD

►TODAY’S THE HINDU ANALYSIS
1.‘ Tauktae cyclone in India – tropical cyclones – physical geography - ([02:27](https://youtube.com/watch?v=szcJTdcLk9A&t=147))
2. West Bengal decides to set up legislative council – Legislative council – Indian polity- ([16:42](https://youtube.com/watch?v=szcJTdcLk9A&t=1002))
3. Avoiding Breakdown (GST council) – Indian polity and economy -  ([32:33](https://youtube.com/watch?v=szcJTdcLk9A&t=1953))
 4. Scheme for revision (MGNREGS) - ([38:38](https://youtube.com/watch?v=szcJTdcLk9A&t=2318))
5. Question for the Day – ([46:48](https://youtube.com/watch?v=szcJTdcLk9A&t=2808))

#tauktae #cyclone #tropicalcyclone #legislativecouncil #westbengalLC #GSTcouncil #GST #MGNREGS #MGNREGA #Arabiansea 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bharadwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs
 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
http://bit.ly/DNS-NOTES-19-04-21-PDF
http://bit.ly/DNS-NOTES-19-04-21-WORD

►TODAY’S THE HINDU ANALYSIS
1.Lingaraja Temple of Bhubaneshwar – Art and culture – ([01:56](https://youtube.com/watch?v=k606hydwvJY&t=116))
2.Implementing Green contracts– Ecology and environment – ([07:33](https://youtube.com/watch?v=k606hydwvJY&t=453))
3.An Obituary for the IP appellate Board– polity and Governance – ([18:46](https://youtube.com/watch?v=k606hydwvJY&t=1126))
4. The ECI cannot be a super government - Polity and governance - Constitutional bodies -  ([28:53](https://youtube.com/watch?v=k606hydwvJY&t=1733))
(Fund provided for relief in Palestine)
5. Question for the Day – ([36:53](https://youtube.com/watch?v=k606hydwvJY&t=2213))

#Judges #HighCourt #SupremeCourt #Article324 #Lingarajatemple #Kaligaarchitecture #ordinance #greencontract #Escerts #electioncommissionofindia 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Vaibhav Mishra of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registratio... 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass:  


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
WORD: http://bit.ly/DNS-NOTES-16-09-21-WORD
PDF: http://bit.ly/DNS-NOTES-16-09-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. US-Russia Relations  - (International Relations) - ([00:44](https://youtube.com/watch?v=Jlb51A-S0uk&t=44))
   What led to the Biden-Putin summit? Page 13 
   Cold peace Page 06 
2. Delhi-Ghaziabad-Meerut RRTS to go for contactless ticketing Page 03 - (Social Issues) - ([14:53](https://youtube.com/watch?v=Jlb51A-S0uk&t=893))
3. Sale of illegal HTBT cotton seeds doubles Page 09 - (Science and Technology) - ([33:40](https://youtube.com/watch?v=Jlb51A-S0uk&t=2020))
4. In India, looking beyond the binary to a spectrum Page 06 - (Social issues) - ([39:53](https://youtube.com/watch?v=Jlb51A-S0uk&t=2393))
5. QOD - ([43:38](https://youtube.com/watch?v=Jlb51A-S0uk&t=2618))

#Biden #Putin #russia #USA #G7 #Urban Transpot #HTBT Cotton #LGBTQ 


► Prelims Compass Link 
1. Indian Polity & Governance - Amazon Link: https://amzn.to/3gChrUJ
2. Science & Technology - Amazon Link: https://amzn.to/3q2lldH
3. Budget & Economic Survey - Amazon Link: https://amzn.to/3wys01Z
4. Government Schemes - Amazon Link: https://amzn.to/3q4jIfS
5. International Relations - Amazon Link: https://amzn.to/3gDvyJw
6. Indian Economy - Amazon Link: https://amzn.to/3qesmsh
7. History & Culture of India - Amazon Link: https://amzn.to/3iS2bpp
8. Environment, Ecology & Biodiversity - Amazon Link: https://amzn.to/2U3VIgI


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1217/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Faizan Khan

#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-19-03-21-PDF
Word - http://bit.ly/DNSS-NOTES-19-03-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Equities fall as U.S. bond yields rise on Fed stance – (Indian Economy) – ([01:03](https://youtube.com/watch?v=A0u78dKqR6I&t=63))
    (Rising bond yields - Cause & Effect) 
2. SC to hear plea against sale of electoral bonds - (Polity & Governance) – ([09:30](https://youtube.com/watch?v=A0u78dKqR6I&t=570))
(Electoral Bonds) 
3. Places in News - (International Relation) - ([16:02](https://youtube.com/watch?v=A0u78dKqR6I&t=962))
(Erbil)
4. Rajya Sabha passes Bill to raise FDI limit in insurance sector - (Indian Economy) -([19:49](https://youtube.com/watch?v=A0u78dKqR6I&t=1189))
(FDI in different sectors)
5. Aadhaar as a hurdle - (Polity & Governance) - ([26:59](https://youtube.com/watch?v=A0u78dKqR6I&t=1619))
(Aadhar Mandatory for Social Services)
6. Speed Test - ([33:54](https://youtube.com/watch?v=A0u78dKqR6I&t=2034))
7. Question for the day – (International Relation) – ([36:11](https://youtube.com/watch?v=A0u78dKqR6I&t=2171))

#RisingBondYields #ElectoralBonds #AadharMandatoryForSocialServices #Erbil


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-981/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mr. Mangal Singh of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 

►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021 

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021


►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-19-05-21-PDF
Word - http://bit.ly/DNS-NOTES-19-05-21-WORD

►TODAY’S THE HINDU ANALYSIS

1. Ukrainian crisis  – (International Relations) –  ([02:06](https://youtube.com/watch?v=ZfmWkM5RW28&t=126))

2. Resource geopolitics and India's interest in the Arctic region 
– (International Relations)  – ([09:28](https://youtube.com/watch?v=ZfmWkM5RW28&t=568))

3. Fugitive economic offender – (Environment) - ([21:02](https://youtube.com/watch?v=ZfmWkM5RW28&t=1262))

4. TRIPS waiver – (Reference) – ([23:09](https://youtube.com/watch?v=ZfmWkM5RW28&t=1389))

5. Question for the Day – ([23:34](https://youtube.com/watch?v=ZfmWkM5RW28&t=1414))

#Ukraine #Arctic # TRIPS #Geopolitics #Crimea 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1186/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 

►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021 

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021


►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-20-04-21-PDF
Word - http://bit.ly/DNS-NOTES-20-04-21-WORD

►TODAY’S THE HINDU ANALYSIS

1. Repromulgation of Ordinance undermines parliamentary legislation  – (Polity & Governance) –  ([01:16](https://youtube.com/watch?v=4prVnUW2Cm4&t=76))

2. India - Russia Relations in the Changing geopolitical scenario  – (International Relations)  – ([19:24](https://youtube.com/watch?v=4prVnUW2Cm4&t=1164))

3. Artificial Intelligence and Children  – (Social Issues) - ([28:39](https://youtube.com/watch?v=4prVnUW2Cm4&t=1719))

4. Ken Betwa River Link– (Environment) – ([38:20](https://youtube.com/watch?v=4prVnUW2Cm4&t=2300))

5. Question for the Day – ([41:07](https://youtube.com/watch?v=4prVnUW2Cm4&t=2467))

#ArtificialIntelligence #Russia #India #Ordinance #KenBEtwa #Panna 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1161/
 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-20-06-21-PDF
Word - http://bit.ly/DNS-Notes-20-06-21-Word

►TODAY’S THE HINDU ANALYSIS
1.‘ Delta plus’ and an emerging public health threat – Science & Technology– ([00:38](https://youtube.com/watch?v=2fNbzVfi_5M&t=38))
(Delta plus variant)
2. Rajasthan women vow to nurture ‘green family’– Society + Environment – ([12:18](https://youtube.com/watch?v=2fNbzVfi_5M&t=738))
(Afforestation drive)
3. J&K delimitation panel begins work – Polity & Governance – ([20:17](https://youtube.com/watch?v=2fNbzVfi_5M&t=1217))
(Delimitation Commission)
4. NGT closes proceedings against Mekedatu dam project – (International Relations) – ([25:23](https://youtube.com/watch?v=2fNbzVfi_5M&t=1523))
(Mekedatu Dam Project)
5. Question for the Day – Polity & Governance – ([28:59](https://youtube.com/watch?v=2fNbzVfi_5M&t=1739))

# DeltaPlusVariant #AfforestationDrive # DelimitationCommission #MekedatuDamProject 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►About – Pre–Pareekshan - http://bit.ly/Pre-Pareekshan-2021
►Pre–Pareekshan Registration Link - https://elearn.rauias.com/home-page/
►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-20-03-21-PDF
Word - http://bit.ly/DNS-NOTES-20-03-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. IT (Intermediary Guidelines) Rules, 2011 - (WhatsApp) – Governance + Security - ([00:45](https://youtube.com/watch?v=LjeS42tEKJk&t=45)) 
(Intermediary, IT Intermediary Guideline Rules 2011, WhatsApp Privacy Policy)
2. Bill to amend GNCTD Act - (Delhi) – Polity & Governance – ([07:42](https://youtube.com/watch?v=LjeS42tEKJk&t=462))
(LG-CM, Delhi, Article 239AA, GNCTD Act, More powers to LG)
3. QUAD Partnership - Counter-challenge to China, New phase of US-China ties comes with tests for India – International Relations – ([11:57](https://youtube.com/watch?v=LjeS42tEKJk&t=717))
(QUAD, Osaka Track, 3Cs Working Groups, Green Climate Fund)
4. Houthis march on Yemen's Marib – World Geography – ([24:11](https://youtube.com/watch?v=LjeS42tEKJk&t=1451))
(Marib, Iran backed Houthis)
5. Govt. questions Global Hunger Index – Social Justice – ([25:51](https://youtube.com/watch?v=LjeS42tEKJk&t=1551))
(2020 Global Hunger Index, Welt Hunger Life, Concern Worldwide)
5. Question for the Day – ([31:11](https://youtube.com/watch?v=LjeS42tEKJk&t=1871))
#IntermediaryRules2011 #Intermediary #WhatsApp #QUAD #GCF #3Csworkinggroup #Climate #Technology #China #houthis #Marib #EastChinaSea #Senkaku #GHI #NHFS5 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1133/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mrs. Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link for Mains (Weekly) Answer Practice: https://elearn.rauias.com/mains-daily-questions/india-can-vaingloriously-claim-itself-as-the-worlds-largest-democracy-but-cant-hold-up-to-claim-to-b/60a62252bc38b71b58550cff/
►Link for optional counselling session: Open Counselling Session (rauias.com)



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF -  http://bit.ly/DNS-Notes-20-05-2021-PDF
Word -  http://bit.ly/DNS-Notes-20-05-2021-Word

►TODAY’S THE HINDU ANALYSIS
1. A thaw in India Pakistan trade relations– (International relations) – ([02:48](https://youtube.com/watch?v=lOioC_jmedg&t=168))
2. Centre raises subsidy on DAP fertilizers; no hike for farmers – (Economy) - ([18:06](https://youtube.com/watch?v=lOioC_jmedg&t=1086))
3. Assam tea gardens hit by COVID19, bad weather– (World Geography)  -([26:04](https://youtube.com/watch?v=lOioC_jmedg&t=1564))
4. Question for the Day – ([31:20](https://youtube.com/watch?v=lOioC_jmedg&t=1880))
#Indo-pak #Pakistan #fertilizers #subsidy #nutrientbasedsubsidy #npk #urea #tea #coffee #plantation 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1188/
 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mrs Pallvi Sarda of
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-21-04-21-PDF
Word - http://bit.ly/DNS-NOTES-21-04-21-WORD
►TODAY’S THE HINDU ANALYSIS

1. Low carbon future through sector led change – Environment and Economy– ([00:40](https://youtube.com/watch?v=2ziiGhuU8wo&t=40))
(Electricity sector emission control)
2. Govt. says container shortage resolved – Economy – ([13:23](https://youtube.com/watch?v=2ziiGhuU8wo&t=803))
(Logistic sector problems and measures taken to reform)
3. Russia says it will  launch its own space station –Science & Tech.– ([23:46](https://youtube.com/watch?v=2ziiGhuU8wo&t=1426))
(International space station)
Reference - (28:40)
4. Removing offending online content – Economy 
(Online/Social media/OTT regulations)
5. The long battle against the Maoists – Internal security
(Left wing extremism)
#electricitysector #climatecrisis #logisticsector #Online #ott #socialmedia #spacestation #extremism #LWE #Maoist #securityissues  

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1162/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 

►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021 

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►Links to buy hard copies of prelims compass 

1. Indian Polity & Governance (Amazon Link: https://amzn.to/3gChrUJ)
 
2. Science & Technology (Amazon Link: https://amzn.to/3q2lldH)
 
3. Budget & Economic Survey (Amazon Link: https://amzn.to/3wys01Z)
 
4. Government Schemes (Amazon Link: https://amzn.to/3q4jIfS)
 
5. International Relations (Amazon Link: https://amzn.to/3gDvyJw)
 
6. Indian Economy (Amazon Link: https://amzn.to/3qesmsh)
 
7. History & Culture of India (Amazon Link: https://amzn.to/3iS2bpp)
 
8. Environment, Ecology & Biodiversity: (Amazon Link: https://amzn.to/2U3VIgI)


►DOWNLOAD DNS NOTES

PDF Link:  http://bit.ly/DNS-NOTES-21-06-21-PDF
Word Link:  http://bit.ly/DNS-NOTES-21-06-21-WORD

►TODAY’S THE HINDU ANALYSIS

1. India - Russia relations in the emerging Geopolitical scenario  
– (International Relations) –  ([00:44](https://youtube.com/watch?v=E_9DjBbsvCg&t=44))

2. Live-in Relationships in India – (Social Issues) – ([16:27](https://youtube.com/watch?v=E_9DjBbsvCg&t=987))

3. Domestic Violence on the Rise – (Social Issues) – ([25:14](https://youtube.com/watch?v=E_9DjBbsvCg&t=1514))

4. Black Softshell Turtle – (Environment) – ([33:37](https://youtube.com/watch?v=E_9DjBbsvCg&t=2017))

5. Question for the Day – ([35:21](https://youtube.com/watch?v=E_9DjBbsvCg&t=2121))



#Russia #QUAD #Domestic #violence #live-in #relationship 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1213/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Faizan Khan and Vaibhav Mishra, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

About – Pre–Pareekshan - http://bit.ly/Pre-Pareekshan-2021

Pre–Pareekshan Registration Link - https://elearn.rauias.com/home-page/

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA
 
►Link UPSC MAINS 2020 GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4


►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-21-03-21-PDF
Word - http://bit.ly/DNS-Notes-21-03-21-Word

►TODAY’S THE HINDU ANALYSIS
1. ‘India, United States resolve to intensify defence cooperation – (International Relation) – ([00:41](https://youtube.com/watch?v=LSWUgPiZ-eM&t=41))
(Indo-U.S relation)
2. The emerging crisis of obtaining helium in India – (Science & Technology) – ([13:13](https://youtube.com/watch?v=LSWUgPiZ-eM&t=793))
(Uses of Helium)
3.NITI Aayog vision for Great Nicobar ignores tribal, ecological concerns – (Environment & Ecology) – ([22:27](https://youtube.com/watch?v=LSWUgPiZ-eM&t=1347))
    (Ecological, Tribal and geological vulnerability of Great Nicobar Island) 
4.Detecting the unified call of black holes  – (Indian Economy) – ([37:18](https://youtube.com/watch?v=LSWUgPiZ-eM&t=2238))
    (Gravitational waves and black hole )
5. Question for the day – (Science & Technology) – ([52:29](https://youtube.com/watch?v=LSWUgPiZ-eM&t=3149))

#Indo-U.SRelation  #UsesOfHelium #FragileGreatNicobar #GravitationalWaves #BlackHole 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1107/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mr. Vaibhav Mishra of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021
►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF -  http://bit.ly/DNS-NOTES-21-05-21-PDF 
Word -http://bit.ly/DNS-NOTES-21-05-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. The outdated nature of bureaucracy Page 07 - ([02:31](https://youtube.com/watch?v=rk-ltOekv4c&t=151))
2. Circumvention Page 06 - ([21:10](https://youtube.com/watch?v=rk-ltOekv4c&t=1270))
3. The fault line of poor health infrastructure Page 06 - ([31:38](https://youtube.com/watch?v=rk-ltOekv4c&t=1898))
4. Sri Lanka Parliament passes Bill on China-backed Port City Page 11 - ([40:26](https://youtube.com/watch?v=rk-ltOekv4c&t=2426))
5. World’s largest iceberg breaks off from Antarctica, says ESA Page 11 -([46:04](https://youtube.com/watch?v=rk-ltOekv4c&t=2764))
6. Declare mucormycosis an epidemic: Centre to States Page 01 - ([49:22](https://youtube.com/watch?v=rk-ltOekv4c&t=2962))
7. China completes Tibet highway Page 09 - ([53:11](https://youtube.com/watch?v=rk-ltOekv4c&t=3191))
8. QOD - ([54:37](https://youtube.com/watch?v=rk-ltOekv4c&t=3277))

#bureaucracy #Central directives #health #Stringofpearls #Srilanka #colomboport

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1189/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Basava Uppin of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021 
►Link for 2021 Indian Economy Prelims Compass: http://bit.ly/Prelims-Compass-Economy-2021 
►Link for 2021 Polity & Governance Prelims Compass: http://bit.ly/Prelims-Compass-Polity-2021 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021  

►DOWNLOAD DNS NOTES
http://bit.ly/DNS-NOTES-22-04-21-WORD
http://bit.ly/DNS-NOTES-22-04-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. Strengthening the process of choosing the police chief – (Polity & Governance)  ([01:18](https://youtube.com/watch?v=JqxfR8YfHoY&t=78))
A. Appointment and Removal of DGP: Norms Vs Practice
B. Police Reforms in India                     
2. Farm exports rose 16.9% in April-Feb. FY21 – (Indian Economy) ([20:32](https://youtube.com/watch?v=JqxfR8YfHoY&t=1232))
A. Need for Boosting Agri Exports
B. Present Status of Agri Exports
C. Constraints and Challenges
3. Exporters fret over delay in rebate rates (MEIS Vs RoDTEP Scheme)- Indian Economy ([39:39](https://youtube.com/watch?v=JqxfR8YfHoY&t=2379))
4. SC paves way for ad-hoc judges in HCs (Indian Polity) ([47:37](https://youtube.com/watch?v=JqxfR8YfHoY&t=2857))
5. India at 142nd rank on press freedom index (International Relations) ([54:54](https://youtube.com/watch?v=JqxfR8YfHoY&t=3294))
6. A fresh push for green hydrogen (Reference) ([57:09](https://youtube.com/watch?v=JqxfR8YfHoY&t=3429))
Link: https://www.youtube.com/watch?v=GVBc6reOq54&t=1656s
7. Question for the Day – ([58:10](https://youtube.com/watch?v=JqxfR8YfHoY&t=3490))

#BoostingAgriexports #PoliceReforms #MEIS #RODTEP #PressFreedomIndex


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1163/ 
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Faizan Khan and Vaibhav Mishra, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA
 
►Link UPSC MAINS 2020 GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4


►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-22-03-21-PDF
Word - http://bit.ly/DNS-Notes-22-03-21-Word

►TODAY’S THE HINDU ANALYSIS
1. ‘Junk inefficiency’– (Polity & Governance) – ([00:44](https://youtube.com/watch?v=1PzOWZdLcMg&t=44))
(Vehicle Scrappage Policy)
2. Iran deal could be rescued by the IAEA – (International Relation) – ([09:48](https://youtube.com/watch?v=1PzOWZdLcMg&t=588))
(Iran nuclear deal)
3. Rising poverty – (Social Justice) – ([22:57](https://youtube.com/watch?v=1PzOWZdLcMg&t=1377))
    (COVID-19 & Increase in poverty and inequality) 
4. Philippines accuses China of ‘incursion’ in disputed sea  – (International Relation) – ([31:46](https://youtube.com/watch?v=1PzOWZdLcMg&t=1906))
5. How to treat unpaid work (reference) - (Social Issues) - ([38:31](https://youtube.com/watch?v=1PzOWZdLcMg&t=2311))
    (Compensation for Care work of women)
6. Question for the day – (Polity & Governance) – ([39:20](https://youtube.com/watch?v=1PzOWZdLcMg&t=2360))


#VehicleScrappagePolicy  #IranNuclearDeal #RisingPoverty #SouthChinaSeaDispute #BlackHole 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1107/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mr. Basava Uppin of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

Link to register for Optional Orientation Class: https://www.rauias.com/optional-session/


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021 
►Link for 2021 Indian Economy Prelims Compass: http://bit.ly/Prelims-Compass-Economy-2021 
►Link for 2021 Polity & Governance Prelims Compass: http://bit.ly/Prelims-Compass-Polity-2021 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021  

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-22-05-21-PDF
Word - http://bit.ly/DNS-NOTES-22-05-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. RBI's Economic Capital Framework – Indian Economy ([02:20](https://youtube.com/watch?v=rJeAU5sCtIM&t=140))
A. Assets and Liabilities of RBI
B. Transfer of Reserves and Dividend to Government
C.  Bimal Jalan Committee Recommendations    
2. Sovereign Gold Bonds: What, Why and How? – Indian Economy ([27:23](https://youtube.com/watch?v=rJeAU5sCtIM&t=1643))
3. The AIDS fight offers a Vaccine pathway – Polity & Governance ([37:34](https://youtube.com/watch?v=rJeAU5sCtIM&t=2254))
4. Globally Important Agricultural Heritage Systems (GIAHS) - Environment ([42:05](https://youtube.com/watch?v=rJeAU5sCtIM&t=2525))
5. Particularly Vulnerable Tribal Groups (PVTGs) – Social Issues ([47:49](https://youtube.com/watch?v=rJeAU5sCtIM&t=2869))
6. WhatsApp Privacy Update- Reference – Polity & Governance ([49:17](https://youtube.com/watch?v=rJeAU5sCtIM&t=2957))
    YouTube link: https://www.youtube.com/watch?v=YTZtY0SH3cg&t=1896s 
7. Question for the Day ([51:53](https://youtube.com/watch?v=rJeAU5sCtIM&t=3113))

#EconomicCapitalFramework #SovereignGoldBonds #GIAHS #PVTGs


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1190/ 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jaten Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►https://www.rauias.com/optional-session



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
https://bit.ly/DNS-NOTES-22-06-2021-WORD
https://bit.ly/DNS-NOTES-22-06-2021-PDF

►TODAY’S THE HINDU ANALYSIS
1. UAPA being misused – Indian polity and governance- ([00:43](https://youtube.com/watch?v=7NoS9NxEQzk&t=43))
2. Indira Gandhi Canal – water resources – Environment and ecology - ([17:46](https://youtube.com/watch?v=7NoS9NxEQzk&t=1066))
3. Bt cotton shows benefits – Indian economy- Science and technology -  ([24:34](https://youtube.com/watch?v=7NoS9NxEQzk&t=1474))
4. International Yoga Day – important current events - International relations - ([33:07](https://youtube.com/watch?v=7NoS9NxEQzk&t=1987))
5. NHRC panel on Bengal Violence- Non-constitutional bodies – Quasi Judicial bodies – ([34:48](https://youtube.com/watch?v=7NoS9NxEQzk&t=2088))
6. Chief of Defence staff – National security architecture - ([38:35](https://youtube.com/watch?v=7NoS9NxEQzk&t=2315))
7. Question for the Day – ([43:23](https://youtube.com/watch?v=7NoS9NxEQzk&t=2603))

#UAPA #lawagainstterrorism #indiragandhicanal #rajasthan #harikebarrage #Internationalyogaday #yoga #UN #btcotton #mosanto #cottoncrop #chiefofdefensestaff #theatrecommand #NHRC

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  





Links for hard copies
Indian Polity & Governance
(Amazon Link: https://amzn.to/3gChrUJ)
Science & Technology
(Amazon Link: https://amzn.to/3q2lldH)
Budget & Economic Survey
(Amazon Link: https://amzn.to/3wys01Z)
Government Schemes
(Amazon Link: https://amzn.to/3q4jIfS)
International Relations
(Amazon Link: https://amzn.to/3gDvyJw)
Indian Economy
(Amazon Link: https://amzn.to/3qesmsh)
History & Culture of India
(Amazon Link: https://amzn.to/3iS2bpp)
Environment, Ecology & Biodiversity
(Amazon Link: https://amzn.to/2U3VIgI)
Presented by: Mr. Mangal Singh of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 

►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021 

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021


►DOWNLOAD DNS NOTES
WORD - http://bit.ly/DNS-NOTES-23-04-21-WORD
PDF - http://bit.ly/DNS-NOTES-23-04-21-PDF


►TODAY’S THE HINDU ANALYSIS

1. Issues with Tribunals in India  – (Polity & Governance) –  ([01:16](https://youtube.com/watch?v=4zOM4-aOOs4&t=76))

2. Digital Education initiatives  – (Social Issues )  – ([12:32](https://youtube.com/watch?v=4zOM4-aOOs4&t=752))

3.Leaders’ Climate Summit – (Environment) - ([18:23](https://youtube.com/watch?v=4zOM4-aOOs4&t=1103))

4. Ad Hoc Judges – (Reference) – ([30:47](https://youtube.com/watch?v=4zOM4-aOOs4&t=1847))

5. Question for the Day – ([32:07](https://youtube.com/watch?v=4zOM4-aOOs4&t=1927))

#Tribunals #Digital #Education #ClimateChange 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1164/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registratio... 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass:  


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-NOTES-23-06-21-PDF
Word - https://bit.ly/DNS-NOTES-23-06-21-WORD
►TODAY’S THE HINDU ANALYSIS
1. Increasing borrowing powers of states amid crisis - Polity & Governance + Economy – ([00:58](https://youtube.com/watch?v=Hfn11zddyZE&t=58))
(Borrowing Powers of States, Article 293, Additional Borrowings for states - GSDP)
2. Four Theatre Commands likely this year – Security – ([10:16](https://youtube.com/watch?v=Hfn11zddyZE&t=616))
(Theatre Command, Chief of Defence Staff)
3. World’s first GM Rubber sapling planted in Assam – Science & Technology – ([14:20](https://youtube.com/watch?v=Hfn11zddyZE&t=860))
(GM Rubber, Plantation Agriculture)
4. India’s apprehensions on us pull out from Afghanistan – International Relations – ([19:58](https://youtube.com/watch?v=Hfn11zddyZE&t=1198))
(Taliban & derailing of Peace Process, Increased violence, India’s Apprehensions in Afghanistan)
5. Eight rare Pygmy Hogs released in Manas National Park – Environment – ([25:38](https://youtube.com/watch?v=Hfn11zddyZE&t=1538))
(Pigmy Hogs, IUCN Status, Endangered)
6. E-Commerce Policy – Economy – ([27:09](https://youtube.com/watch?v=Hfn11zddyZE&t=1629))
(Concerns with E-commerce industry, unfair trade practice, Amendment in e-commerce rules)
7. Question for the Day – ([32:56](https://youtube.com/watch?v=Hfn11zddyZE&t=1976))
#Borrowings #Stateborrowing #Atmanirbhar #Citizencentricreforms #OneNationOneRationScheme #TheatreCommand #CDS #DAC #GMRubber #PlantationAgriculture #IndiaAfgjanistan #Taliban #PigmyHog #Ecommerce #FDIEcommerce 

► Prelims Compass Link 
1. Indian Polity & Governance - Amazon Link: https://amzn.to/3gChrUJ

2. Science & Technology - Amazon Link: https://amzn.to/3q2lldH

3. Budget & Economic Survey - Amazon Link: https://amzn.to/3wys01Z

4. Government Schemes - Amazon Link: https://amzn.to/3q4jIfS

5. International Relations - Amazon Link: https://amzn.to/3gDvyJw

6. Indian Economy - Amazon Link: https://amzn.to/3qesmsh

7. History & Culture of India - Amazon Link: https://amzn.to/3iS2bpp

8. Environment, Ecology & Biodiversity - Amazon Link: https://amzn.to/2U3VIgI


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1222/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-23-03-2021-PDF
Word - http://bit.ly/DNS-Notes-23-03-2021-Word

►TODAY’S THE HINDU ANALYSIS
1. States Reorganisation Act & Delhi – Polity & Governance - ([00:42](https://youtube.com/watch?v=jOVR_mz-ffw&t=42)) 
(States Reorganisation Act, 1956, Constitution 7th Amendment, Delhi)
2. Committee on In-House Procedure – Polity & Governance – ([12:18](https://youtube.com/watch?v=jOVR_mz-ffw&t=738))
(Impeachment, Misconduct, Judges Inquiry Act)
3. GCC & Kafala System – International Relations – ([21:16](https://youtube.com/watch?v=jOVR_mz-ffw&t=1276))
(GCC, Kafala System)
4. Sops under RODTEP to see delay – Economy – ([24:38](https://youtube.com/watch?v=jOVR_mz-ffw&t=1478))
(Withdrawal of MEIS Scheme, RODTEP, WTO)
5. Gandhi Peace Prize for Mujib and Sultan Qaboos – International Relations – ([28:37](https://youtube.com/watch?v=jOVR_mz-ffw&t=1717))
(Gandhi Peace Prize, Bangladesh, Oman)
5. Question for the Day – ([31:25](https://youtube.com/watch?v=jOVR_mz-ffw&t=1885))
#SRA #Reorganisation #Delhi #LegislativeAssembly #BalakrishnanCommittee #Committee #InHouseCommittee #SC #Impeachment #Misconduct #JudgesInquiryAct #MEISScheme #RODTEP #WTO #GandhiPeacePrize


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1136/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 

#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF – http://bit.ly/DNS-Notes-23-05-21-pdf
Word - Ihttp://bit.ly/DNS-Notes-23-05-21-Word

►TODAY’S THE HINDU ANALYSIS
1. Optional Subject Selection orientation announcement – ([00:15](https://youtube.com/watch?v=-HOifDmNmbc&t=15))
2. Identifying mutants – Science & Technology - ([02:21](https://youtube.com/watch?v=-HOifDmNmbc&t=141))
(Mutation in viruses)
3. BRICS Astronomy Working Group moots networking of existing telescopes – Science & Technology – ([24:30](https://youtube.com/watch?v=-HOifDmNmbc&t=1470))
(Astronomical study by telescope)
4. Gujarat emerges as a hotspot for Mucormycosis in India – Science & Technology – ([34:48](https://youtube.com/watch?v=-HOifDmNmbc&t=2088))
(Health & Disease - Mucormycosis)
5. How whiteflies came, saw and conquered India’s crops – Environment & Ecology – ([38:17](https://youtube.com/watch?v=-HOifDmNmbc&t=2297))
(Invasive Species)
6. Question for the Day (Science & Technology) – ([39:55](https://youtube.com/watch?v=-HOifDmNmbc&t=2395))
7. Speed Test is available on the e-learn platform

#MutationInViruses # Mucormycosis #AstronomicalStudyByTelescope #Whiteflies

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
http://bit.ly/DNS-NOTES-24-04-2021-PDF
http://bit.ly/DNS-NOTES-24-04-21-WORD

►TODAY’S THE HINDU ANALYSIS
1.Finance Ministry lifts cap to boost Capex– Indian economy – ([01:26](https://youtube.com/watch?v=5VBssygSv44&t=86))
(budget components)
2.'RBI intend is key to curb further rupee weakens'– Indian Economy – (currency and depreciation) - ([21:27](https://youtube.com/watch?v=5VBssygSv44&t=1287))
3. BRO reports glacier break in Uttarakhand– Indian Geography – (natural disaster, glacier) - ([29:42](https://youtube.com/watch?v=5VBssygSv44&t=1782))
4. RBI extends States' Ways and Means credit to sept.– Indian economy - (RBI and monetary policy) - ([36:36](https://youtube.com/watch?v=5VBssygSv44&t=2196))
5. Question for the Day – ([45:21](https://youtube.com/watch?v=5VBssygSv44&t=2721))

#budget #financeministry #capitalexpenditure #governmentborrowing #indiancurrency #depreciation #BRO #Glacier #uttarakhand #exchangerate #waysandmeansadvances #RBI #
/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 

►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021 

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►Links to buy hard copies of prelims compass 

1. Indian Polity & Governance (Amazon Link: https://amzn.to/3gChrUJ)
 
2. Science & Technology (Amazon Link: https://amzn.to/3q2lldH)
 
3. Budget & Economic Survey (Amazon Link: https://amzn.to/3wys01Z)
 
4. Government Schemes (Amazon Link: https://amzn.to/3q4jIfS)
 
5. International Relations (Amazon Link: https://amzn.to/3gDvyJw)
 
6. Indian Economy (Amazon Link: https://amzn.to/3qesmsh)
 
7. History & Culture of India (Amazon Link: https://amzn.to/3iS2bpp)
 
8. Environment, Ecology & Biodiversity: (Amazon Link: https://amzn.to/2U3VIgI)


► Weekly answer writing link – 
https://elearn.rauias.com/mains-daily-questions/do-you-agree-the-mahatma-gandhi-national-rural-employment-guarantee-program-has-lived-up-to-its-expe/60d399eb77e0ba61bec2684b/

►DOWNLOAD DNS NOTES

PDF Link:  https://bit.ly/DNS-NOTES-24-06-21-PDF
Word Link: https://bit.ly/DNS-NOTES-24-06-21-WORD



►TODAY’S THE HINDU ANALYSIS

1. Regional geopolitics and Kashmir Policy   – (International Relations) –  ([01:06](https://youtube.com/watch?v=Qsmf5xqTNqc&t=66))

2. UGC circular on online Higher education– (Social Issues) – ([17:23](https://youtube.com/watch?v=Qsmf5xqTNqc&t=1043))

3. Federalism in India – (Polity and Governance) – ([25:11](https://youtube.com/watch?v=Qsmf5xqTNqc&t=1511))

4. Gender technology Divide – (Social Issues) – ([33:08](https://youtube.com/watch?v=Qsmf5xqTNqc&t=1988))

5. Question for the Day – ([39:17](https://youtube.com/watch?v=Qsmf5xqTNqc&t=2357))



#Kashmir #Pakistan #Geopolitics #UGC #highereducation  #gender #DigitalDivide #federalism 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1213/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mangal Singh, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-24-03-21-PDF
Word - http://bit.ly/DNS-NOTES-24-03-21-WORD

►TODAY’S THE HINDU ANALYSIS
1.    India - Taiwan and One China Policy - (International Relations ) – ([00:30](https://youtube.com/watch?v=VzxWZWNyG9I&t=30)) 
(Leveraging India Taiwan relations to counter China) 
2. India abstains from UNHRC resolution on Sri Lanka – (International Relations)  - ([09:26](https://youtube.com/watch?v=VzxWZWNyG9I&t=566))
3. Energy Diplomacy - (International Relations) - ([24:01](https://youtube.com/watch?v=VzxWZWNyG9I&t=1441))
4. Pendency and delay in Indian judicial System - (Polity & Governance)  - ([32:25](https://youtube.com/watch?v=VzxWZWNyG9I&t=1945))
5. Question for the day – (Geography) – ([42:48](https://youtube.com/watch?v=VzxWZWNyG9I&t=2568))
#Taiwan #UNHRC #SriLanka #Neighborhoodfirst #Energy #Diplomacy #Softpower #pendency #Judiciary  


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1137/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mrs. Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link for optional counselling session: Open Counselling Session (rauias.com)



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-24-05-21-PDF
Word - http://bit.ly/DNS-Notes-24-05-21-Word

►TODAY’S THE HINDU ANALYSIS
1. The many benefits of an eco tax – (Environment) – ([01:51](https://youtube.com/watch?v=zJMi8o4jA1w&t=111))
(Green tax, Pollution tax, Environment tax)
2. 6 extremists killed in Assam encounter – (Security issues) - ([15:36](https://youtube.com/watch?v=zJMi8o4jA1w&t=936))
3. ‘Tuber Man’ wins India Biodiversity Award – (Environment)  -([26:24](https://youtube.com/watch?v=zJMi8o4jA1w&t=1584))
4. Very severe cyclone Yaas to hit north Odisha coast – (Geography) – ([30:26](https://youtube.com/watch?v=zJMi8o4jA1w&t=1826))
5. Question for the Day – ([33:25](https://youtube.com/watch?v=zJMi8o4jA1w&t=2005))
#Greentax  #Pollutiontax #Environmenttax #security #insurgency #yaascyclone #cyclone #indiabiodiversityaward 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1192/
 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Vaibhav Mishra of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021
►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF: http://bit.ly/DNS-NOTES-25-04-21-PDF
WORD: http://bit.ly/DNS-Notess-25-04-21-word

►TODAY’S THE HINDU ANALYSIS
1. Groundwater depletion may reduce winter cropping intensity by 20% in India Page 10 - (Geography and Environment) - ([01:17](https://youtube.com/watch?v=YdGsSfs5m1Q&t=77))
2. Chances of infection after vaccination Page 11 - (Science and Technology) - ([18:30](https://youtube.com/watch?v=YdGsSfs5m1Q&t=1110))
3. Reforms in the National Pension System page 13 - (Economy) - ([22:03](https://youtube.com/watch?v=YdGsSfs5m1Q&t=1323))
4. IISc teams develop oxygen concentrators, ventilators Page 10 - (Science and Technology) - ([28:04](https://youtube.com/watch?v=YdGsSfs5m1Q&t=1684))
5. Justice N.V. Ramana is CJI Page 06 - (Polity and Governance) - ([34:31](https://youtube.com/watch?v=YdGsSfs5m1Q&t=2071))
6. QOD - ([40:11](https://youtube.com/watch?v=YdGsSfs5m1Q&t=2411))


/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Basava Uppin of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021  
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-NOTES-25-06-21-PDF 
Word - https://bit.ly/DNS-NOTES-25-06-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Debate around Recusal of Judges- Pros and Cons – Governance ([00:37](https://youtube.com/watch?v=X5i2-mLKJfg&t=37))
2. The rural economy can jump-start a revival – Economy ([13:55](https://youtube.com/watch?v=X5i2-mLKJfg&t=835))
A. Structural Transformation in Rural Areas
B. Leveraging Secondary Agriculture
3. ‘Ensure GST cuts reach users’ - Anti-Profiteering – Economy ([30:24](https://youtube.com/watch?v=X5i2-mLKJfg&t=1824))
4. Staging a comeback, re-energising India’s Africa policy – International Relations ([40:35](https://youtube.com/watch?v=X5i2-mLKJfg&t=2435))
5. Russia, U.K. spar over Black Sea incident – International Relations ([46:00](https://youtube.com/watch?v=X5i2-mLKJfg&t=2760))
6. Protecting Prisoners' Rights- Prison Reforms (Reference) – Governance ([48:43](https://youtube.com/watch?v=X5i2-mLKJfg&t=2923))
     YouTube link: https://www.youtube.com/watch?v=PzUHQ7sY2uU 
 7. Question for the Day ([49:43](https://youtube.com/watch?v=X5i2-mLKJfg&t=2983))

#RecusalofJudges #RuralTransformation #Anti-profiteering #SecondaryAgriculture 

 
https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1224/ 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Faizan Khan & Vaibhav Mishra

#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-Notes-25-03-21-PDF
Word - http://bit.ly/DNS-Notes-25-03-21-Word

►TODAY’S THE HINDU ANALYSIS
1. Weekly Mains Answer Assignment  - ([00:25](https://youtube.com/watch?v=NnYyTYDATQo&t=25))
2. Tamil Nadu’s distinct growth path is in peril – (Polity & Governance) – ([01:13](https://youtube.com/watch?v=NnYyTYDATQo&t=73))
    (Model of Governance) 
3. Supreme Court flags concern over misuse of electoral bonds - (Polity & Governance) – ([15:26](https://youtube.com/watch?v=NnYyTYDATQo&t=926))
(Electoral Bonds) 
4. TB notiﬁcations fall due to pandemic disruptions - (Social Justice) - ([16:10](https://youtube.com/watch?v=NnYyTYDATQo&t=970))
(Tuberculosis)
5. Myanmar junta frees 600 detainees - (International Relation) - ([28:01](https://youtube.com/watch?v=NnYyTYDATQo&t=1681))
(India - Mayanmar relation)
6. Question for the day – (Science & Technology | Health & Disease) – ([34:36](https://youtube.com/watch?v=NnYyTYDATQo&t=2076))


#ModelofGovernance  #TrickleDownEffect  #CapacityApproach #Electoral Bonds


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-981/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mr. Jatin Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►https://www.rauias.com/optional-session



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-25-05-21-PDF
WORD - http://bit.ly/DNS-NOTES-25-05-21-WORD

►TODAY’S THE HINDU ANALYSIS
1.‘ Islanders seek withdrawal of land norms in Lakshadweep – Polity and governance - ([02:10](https://youtube.com/watch?v=qeDMgdNQ76U&t=130))
2. FDI flow crosses $81 billion in FY21– Foreign direct investment– Indian economy- ([15:18](https://youtube.com/watch?v=qeDMgdNQ76U&t=918))
3. High Powered panel meets to choose next CBI director– Central bureau of investigation -  polity and governance -  ([30:36](https://youtube.com/watch?v=qeDMgdNQ76U&t=1836))
4. Expanding the scope of POCSO - social justice - Governance in India - ([41:29](https://youtube.com/watch?v=qeDMgdNQ76U&t=2489))
5. Scheme for revision (MITRA) - ([50:37](https://youtube.com/watch?v=qeDMgdNQ76U&t=3037))
6. Question for the Day – ([54:11](https://youtube.com/watch?v=qeDMgdNQ76U&t=3251))

#CBI #POCSO #Lakshadweep #IDA #MITRA #textilesector #FDI #FPI #Administrator #IslandsofIndia ##CBIdirector #CVC #Lokpal 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mrs. Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-26-04-21-PDF 
Word - http://bit.ly/DNS-NOTES-26-04-21-WORD
►TODAY’S THE HINDU ANALYSIS
1.‘No volunteer list under cyber scheme–Security – ([00:45](https://youtube.com/watch?v=ooRBffFA4Ps&t=45))
(Cyber security issues in India and solutions)
2.Endeavor, leadership and the story of a nation– (World History , International Relations)- ([23:23](https://youtube.com/watch?v=ooRBffFA4Ps&t=1403))
(50 years of Bangladesh independence, Indo- Pakistan war of 1971, Indo-Bangladesh relationship)
3. What happened to Armenians in 1915?– (International Relations) - ([34:19](https://youtube.com/watch?v=ooRBffFA4Ps&t=2059))
(Genocide, Map markings)
4. Question for the Day – ([39:44](https://youtube.com/watch?v=ooRBffFA4Ps&t=2384))
#cyber #cybersecurity #indiacyberthreat #bangladeshgoldenjubliee #50yearsofbangladesh #indo-bangladesh #armenia #map #prelims #upsc

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/category/daily-news-headlines/
 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-Notes-26-06-21-PDF
Word - https://bit.ly/DNS-Notes-26-06-21-Word

►TODAY’S THE HINDU ANALYSIS
1 Prepare the ground for a ‘Naya J&K’  + Future State – International Relation – (00:36)
(Changed J&K Policy of centre)
2. Relevant advice for health care-givers, mothers-to-be – Social Issues – ([09:18](https://youtube.com/watch?v=WZ-D3SOZK2A&t=558))
(Health care of Mother-to-be)
3. IAC will be commissioned next year: Rajnath – Security + Science & Technology – ([18:01](https://youtube.com/watch?v=WZ-D3SOZK2A&t=1081))
(Aircraft Carriers)
4. Speed Test – ([21:25](https://youtube.com/watch?v=WZ-D3SOZK2A&t=1285))
5. Question for the Day – Science & Technology – ([26:17](https://youtube.com/watch?v=WZ-D3SOZK2A&t=1577))

#ChangedJ&KPolicy # HealthCareOfMother-to-be #AircraftCarriers #INSVikrant

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mangal Singh, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-26-03-21-PDF
WORD - http://bit.ly/DNS-NOTES-26-03-21-WORD
►TODAY’S THE HINDU ANALYSIS
1. India - Bangladesh - (International Relations) – ([00:45](https://youtube.com/watch?v=cy02KiYNQWU&t=45)) 
(Contours of Bilateral relations) 
2. Strategic choke Point – (International Relations) - ([16:42](https://youtube.com/watch?v=cy02KiYNQWU&t=1002))
3. South China Sea - (International Relations)  - ([22:13](https://youtube.com/watch?v=cy02KiYNQWU&t=1333))
4. Question for the day – (International Relations) – ([29:23](https://youtube.com/watch?v=cy02KiYNQWU&t=1763))

Reference – (28:52)
5. Electoral Bonds – (Polity & Governance)
6. Collegium System - (Polity & Governance)
#Bangladesh #SouthChinaSea #Chokepoints 

https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1125/

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Faizan Khan of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
WORD - http://bit.ly/DNS-NOTES-26-05-21-WORD
PDF - http://bit.ly/DNS-NOTES-26-05-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. Optional Subject Selection orientation announcement – ([00:15](https://youtube.com/watch?v=7rmzqHnbzgE&t=15))
2. Slowing the pace of India’s mucormycosis threat + Mucormycosis: avoid damp, dusty places – Science & Technology - ([02:22](https://youtube.com/watch?v=7rmzqHnbzgE&t=142))
(Mucormycosis)
3. New IT rules come into force today; will comply, says FB  + Rules and rulers – Polity & Governance - ([10:10](https://youtube.com/watch?v=7rmzqHnbzgE&t=610))
(IT Rules 2021)
4. CJI made ‘statement of law’ at CBI panel (Reference) – Polity & Governance– ([20:15](https://youtube.com/watch?v=7rmzqHnbzgE&t=1215))
(Health & Disease - Mucormycosis)
5. One-state solution, the way forward in Palestine (Reference) – International Relation – ([20:20](https://youtube.com/watch?v=7rmzqHnbzgE&t=1220))
(Israel-Palestine Issue)
6. Speed Test  - ([20:44](https://youtube.com/watch?v=7rmzqHnbzgE&t=1244))
7. Question for the Day - (Polity & Governance) – ([23:14](https://youtube.com/watch?v=7rmzqHnbzgE&t=1394))

#CycloneYaas #Mucormycosis #ITRules2021 #Israel-PalestineIssue

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Basava Uppin of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021 
►Link for 2021 Indian Economy Prelims Compass: http://bit.ly/Prelims-Compass-Economy-2021 
►Link for 2021 Polity & Governance Prelims Compass: http://bit.ly/Prelims-Compass-Polity-2021 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021  

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-27-04-21-PDF
Word - http://bit.ly/DNS-NOTES-27-04-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Healthcare Infrastructure in India – Polity and Governance  ([0:41](https://youtube.com/watch?v=7iS3mwVSap8&t=41))
      a. Constraints and Challenges
      b. Recommendations and Strategies
      c. Leveraging Private Sector Dominance in Healthcare  
2. Proposal of Global Minimum Corporate Tax – Indian Economy ([22:45](https://youtube.com/watch?v=7iS3mwVSap8&t=1365))
     A. Rationale
     B. India's stand
     C. Concerns and Challenges
3. Launch of InVIT by PGCIL - Indian Economy ([40:56](https://youtube.com/watch?v=7iS3mwVSap8&t=2456))
4. Question for the Day – ([46:44](https://youtube.com/watch?v=7iS3mwVSap8&t=2804))

#UniversalHealthCoverage #GlobalMinimumTax #MonetisationPipeline #InVIT


https://www.rauias.com/daily-news-headlines/the-hindu-express-edition-new-delhi-2/ 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Pallavi Sarda, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-27-03-21-PDF
Word - http://bit.ly/DNS-Notes-27-03-21-Word

►TODAY’S THE HINDU ANALYSIS
1. Dormant Parliament, fading business – (Polity & Governance) - ([00:53](https://youtube.com/watch?v=duGgEvBEXA0&t=53))
2. Japan to fund A&N, metro expansion- (International Relations) – ([14:04](https://youtube.com/watch?v=duGgEvBEXA0&t=844))
3. 40% of RTI rejections did not cite valid reason - (Polity & Governance) - ([23:23](https://youtube.com/watch?v=duGgEvBEXA0&t=1403)) 
(Decentralization)
4. Two die while cleaning septic tank- (Social Justice) - ([31:1](https://youtube.com/watch?v=duGgEvBEXA0&t=1861))
5. QOD – ([36:32](https://youtube.com/watch?v=duGgEvBEXA0&t=2192))
#Parliament #Andaman #nicobar #Manualscavenging #Japan #rightoinformation #loksabha #rajyasabha

https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1140/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mr. Naweed Akhter of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registratio... 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass:  


Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-27-05-21-PDF
Word - http://bit.ly/DNS-Notes-27-05-21-Word
►TODAY’S THE HINDU ANALYSIS
1. Optional Announcement - ([00:26](https://youtube.com/watch?v=poHNtdDUxmE&t=26)) 
2. DNS Mains Answer Writing – ([02:17](https://youtube.com/watch?v=poHNtdDUxmE&t=137))
3. Separate Dept. for the welfare of BTR – Polity & Governance – ([02:37](https://youtube.com/watch?v=poHNtdDUxmE&t=157))
(Bodoland Territorial Region, Sixth Schedule, Bodo Peace Accords)
4. Lakshadweep - Resentment against Administrator's decision – Polity & Governance – ([08:20](https://youtube.com/watch?v=poHNtdDUxmE&t=500))
(Reasons for Resentment against decisions of Administrator)
5. Strengthening Food Security – Economy – ([16:55](https://youtube.com/watch?v=poHNtdDUxmE&t=1015))
(National Food Security Act, One Nation One Ration, PDS)
6. National Mission on Cultural Mapping of India – Culture - ([28:18](https://youtube.com/watch?v=poHNtdDUxmE&t=1698))
(Vision, Mission, Components & Challenges)
7. Question for the Day – ([34:16](https://youtube.com/watch?v=poHNtdDUxmE&t=2056))
#BTR #BTC #BODO #SixthSchedule #Lakshadweep #Article239 #NFSA #PDS #FoodSecurity #ONOR #CulturalMappingMission 


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1195/
 /IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jaten Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►https://www.rauias.com/optional-session



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
https://bit.ly/DNS-NOTES-27-06-21-WORD
https://bit.ly/DNS-NOTES-27-06-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. Border security and management – Indian national security- ([00:42](https://youtube.com/watch?v=CeA9ix161Tg&t=42))
2. Rajaji and Corbett National Parks– national parks – Environment and ecology - ([16:07](https://youtube.com/watch?v=CeA9ix161Tg&t=967))
3. Great Barrier Reef– environment and ecology- coral reefs - ([23:04](https://youtube.com/watch?v=CeA9ix161Tg&t=1384))
4. Why bacteria develop Multi-drug resistance– Science and technology - ([34:14](https://youtube.com/watch?v=CeA9ix161Tg&t=2054))
5. Question for the Day – ([39:03](https://youtube.com/watch?v=CeA9ix161Tg&t=2343))

#nationalsecurity #indianborders #indianarmy #newindiancoppers #rajajinationalpark #jimcorbettnationalpark #uttarakhand #multidrugresistance #greatbarrierreef 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mrs. Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link for optional counselling session: Open Counselling Session (rauias.com)



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-NOTES-28-06-21-PDF
WORD - https://bit.ly/DNS-NOTES-28-06-21-WORD


►TODAY’S THE HINDU ANALYSIS
1. Another shade of grey – (International Relations) –([00:39](https://youtube.com/watch?v=4faVMPZgRE4&t=39))
2. FATF– (Mains Question discussion) – ([05:43](https://youtube.com/watch?v=4faVMPZgRE4&t=343))
3. Balancing speed and safety – (Governance) - ([17:36](https://youtube.com/watch?v=4faVMPZgRE4&t=1056))
4. Govt faces a tough choice on Interest rates – (Economics) – ([27:57](https://youtube.com/watch?v=4faVMPZgRE4&t=1677))
5.  Suspected drone attack on IAF Jammu station leaves 2 injured – (Security)– ([32:31](https://youtube.com/watch?v=4faVMPZgRE4&t=1951))
6. Question for the Day – ([40:00](https://youtube.com/watch?v=4faVMPZgRE4&t=2400))
#fatf # greylist #pakistan #road #smallsavings #drone #uav

/IAS - ELEARN 
▪ eLearn - All our content is available on our learning platform - https://elearn.rauias.com 


►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1227/ 

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Basava Uppin of        
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 

Link to register for Optional Orientation Class: https://www.rauias.com/optional-session/


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021 
►Link for 2021 Indian Economy Prelims Compass: http://bit.ly/Prelims-Compass-Economy-2021 
►Link for 2021 Polity & Governance Prelims Compass: http://bit.ly/Prelims-Compass-Polity-2021 
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021  

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-28-05-21-PDF
Word - http://bit.ly/DNS-Notes-28-05-21-Word

►TODAY’S THE HINDU ANALYSIS
1. WhatsApp vs Government: Tracing the Originator of Messages – Polity & Governance – ([02:00](https://youtube.com/watch?v=wXdyhmaABqc&t=120))
A. Arguments and Counter Arguments
B. Puttaswamy Judgement on Right to Privacy    
C. SC Observation on the issue  
2. Weathering storms: Disaster Insurance for Financing Disaster Management– Disaster Management ([25:37](https://youtube.com/watch?v=wXdyhmaABqc&t=1537))
3. Congress MP seeks revival of MPLADS– Polity & Governance ([40:57](https://youtube.com/watch?v=wXdyhmaABqc&t=2457))
4. Monoclonal Antibodies Therapy- Science & Technology ([47:02](https://youtube.com/watch?v=wXdyhmaABqc&t=2822))
5. Incorporating Limits- Hate Speech– Polity & Governance ([52:02](https://youtube.com/watch?v=wXdyhmaABqc&t=3122))
    YouTube link: https://www.youtube.com/watch?v=Zh3pEUxz76I&t=1372s 
6. Question for the Day ([54:11](https://youtube.com/watch?v=wXdyhmaABqc&t=3251))

#WhatsAppVsGovt #Socialmedia #DisasterInsurance #MPLADS #MonoclinalAntibodyTherapy #HateSpeech


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1196/ 

/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Naweed Akhter & Mr. Basava Uppin of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2 
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3 
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4
 
►First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS   



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-30-03-21-PDF
Word - http://bit.ly/DNS-NOTES-30-03-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Allogeneic BMT – Science & Technology - ([01:02](https://youtube.com/watch?v=g8VWM0jXKHg&t=62)) 
(Allogeneic Bone Marrow Transplant, Engraftment)
2. Glial Cells – Science & Technology – ([07:25](https://youtube.com/watch?v=g8VWM0jXKHg&t=445))
(Glial Cells, Neurons, Central Nervous System)
3. Sambhar Lake (Magazine) – Environment – ([13:43](https://youtube.com/watch?v=g8VWM0jXKHg&t=823))
(Sambhar Lake, Inland Salt Lake, Problems of Sambhar Lake)
4. Currents on Enceladus – Science & Technology – ([16:14](https://youtube.com/watch?v=g8VWM0jXKHg&t=974))
(Enceladus, Saturn, Cassini–Huygens)
5. Centre mulls unique ID for all plots of land by March 2022 – Governance – ([22:36](https://youtube.com/watch?v=g8VWM0jXKHg&t=1356))
(Unique ID for plot of land)
6. Pong Dam Wildlife Sanctuary – (Environment) – ([28:02](https://youtube.com/watch?v=g8VWM0jXKHg&t=1682))
(Himachal Pradesh, Pong Dam)
7. Question for the Day – ([29:59](https://youtube.com/watch?v=g8VWM0jXKHg&t=1799))
#Allogeneic #BMT #GlialCells #Neurons #Sambhar #Ramsar #Enceladus #Saturn #UniqueIDLand #PongDam #PongWildlife #Himachal  



/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF Link: http://bit.ly/DNS-Notes-28-04-21-PDF
Word link: http://bit.ly/DNS-Notes-28-04-21-Word

►TODAY’S THE HINDU ANALYSIS
1.‘A patently wrong’ intellectual property regime– international affairs - Polity & Governance – ([00:45](https://youtube.com/watch?v=xEQYtLpYPNM&t=45))
2. Leaders calls for House panels’ meet – Polity and governance - ([13:53](https://youtube.com/watch?v=xEQYtLpYPNM&t=833))
(parliamentary committee)
3. True name – (Armenian Genocide) – International affairs- World History - ([20:03](https://youtube.com/watch?v=xEQYtLpYPNM&t=1203))
4. SEBI tighten rules for for provisional debt rating - Indian Economy ([25:00](https://youtube.com/watch?v=xEQYtLpYPNM&t=1500))
5. Scheme for revision- ([30:17](https://youtube.com/watch?v=xEQYtLpYPNM&t=1817))
6. Question for the Day – ([35:00](https://youtube.com/watch?v=xEQYtLpYPNM&t=2100))

#IPR #patents #wto #compulsorylicensing #parliamentarycommittee #Loksabha #rajyasabha #Armeniangenocide #westasia #Turkey #SEBI #creditratingagency #PLIscheme

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mrs. Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-29-04-2021-PDF
Word -  http://bit.ly/DNS-NOTES-29-04-2021-WORD

►TODAY’S THE HINDU ANALYSIS
1. Reply to prisoners’ plea seeking parole’– (Polity) - ([01:18](https://youtube.com/watch?v=wjJKZRjNZr8&t=78))
• Police reform – (04:25)
• Court reform – (13:07)
• Prison reform – (23:21)
2. Antimicrobial resistance: the silent threat – (Science and tech.) - ([32:45](https://youtube.com/watch?v=wjJKZRjNZr8&t=1965))
3. Reference – ([41:39](https://youtube.com/watch?v=wjJKZRjNZr8&t=2499))
Centre notifies Act giving more powers to Delhi L-G - (Polity) 
(Govt of NCT of Delhi Act, 2021)
4. Question for the Day – ([42:25](https://youtube.com/watch?v=wjJKZRjNZr8&t=2545))
#criminalsystemreform #police #court #jail #prision #reform #antimicrobial #antibacterial #NCTofDelhiact

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1168/
 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 

►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 

►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021 

►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►Links to buy hard copies of prelims compass 

1. Indian Polity & Governance (Amazon Link: https://amzn.to/3gChrUJ)
 
2. Science & Technology (Amazon Link: https://amzn.to/3q2lldH)
 
3. Budget & Economic Survey (Amazon Link: https://amzn.to/3wys01Z)
 
4. Government Schemes (Amazon Link: https://amzn.to/3q4jIfS)
 
5. International Relations (Amazon Link: https://amzn.to/3gDvyJw)
 
6. Indian Economy (Amazon Link: https://amzn.to/3qesmsh)
 
7. History & Culture of India (Amazon Link: https://amzn.to/3iS2bpp)
 
8. Environment, Ecology & Biodiversity: (Amazon Link: https://amzn.to/2U3VIgI)


►DOWNLOAD DNS NOTES

PDF Link:  https://bit.ly/DNS-NOTES-29-06-2021-PDF
Word Link: https://bit.ly/DNS-NOTES-29-06-2021-WORD
 

►TODAY’S THE HINDU ANALYSIS

1. Sedition law Constitutional or Unconstitutional    – (Polity & Governance) –  ([00:44](https://youtube.com/watch?v=tMp4lTHIVuk&t=44))

2. Economic stimulus Package – (Economy) – ([22:54](https://youtube.com/watch?v=tMp4lTHIVuk&t=1374))

3. Functional Issues in the CAPFs – (Security) – ([40:59](https://youtube.com/watch?v=tMp4lTHIVuk&t=2459))

4. Prelims MCQs from Articles –– ([48:40](https://youtube.com/watch?v=tMp4lTHIVuk&t=2920))

5. Question for the Day – ([50:05](https://youtube.com/watch?v=tMp4lTHIVuk&t=3005))



#Sedition #CAPF #Economicstimulus #AtmanirbharBharat #6.28lakhcrore  

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1213/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Mangal Singh of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021
►Link for 2021 Indian History and Culture Prelims Compass: http://bit.ly/PrelimsCompassHistory2021 
►Link for Budget and Economic Survey Prelims Compass: http://bit.ly/PrelimsCompass-Budget-Survey2021 
►Link for Polity & Governance: http://bit.ly/Prelims-Compass-Polity-2021 
►Link for Indian Economy: http://bit.ly/Prelims-Compass-Economy-2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-Notes-29-05-21-PDF
Word - http://bit.ly/DNS-Notes-29-05-21-Word

►TODAY’S THE HINDU ANALYSIS
1. Nepal Political crisis and relations with India  – (International Relations) –  ([02:26](https://youtube.com/watch?v=MuRUQwJfMDw&t=146))
2. Rwandan Genocide  – (International Relations)  – ([16:46](https://youtube.com/watch?v=MuRUQwJfMDw&t=1006))
3. Capitalism and Human welfare – (Social Issues) - ([26:11](https://youtube.com/watch?v=MuRUQwJfMDw&t=1571))
4. RBI Surplus Reserve Transfer – (Reference) – ([29:40](https://youtube.com/watch?v=MuRUQwJfMDw&t=1780))
5. Question for the Day – ([30:07](https://youtube.com/watch?v=MuRUQwJfMDw&t=1807))

#Nepal #Madhesi #Rwanda #Capitalism #RBI #surplusreserves 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1197/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
http://bit.ly/DNS-NOTES-02-05-21-WORD
http://bit.ly/DNS-NOTES-02-05-21-PDF

►TODAY’S THE HINDU ANALYSIS
1.‘Record GST mop-up of 1.41 lakh crore in April– indian economy - taxation - ([00:49](https://youtube.com/watch?v=TrX3XNkjOS4&t=49))
2. USA clears sale of 6 P-8I patrol aircraft to India - defence deals - naval surveillance - ([17:57](https://youtube.com/watch?v=TrX3XNkjOS4&t=1077))
3. Progress noted at envoy meet on the Iran nuclear deal - international relation - nuclear deal -  ([25:41](https://youtube.com/watch?v=TrX3XNkjOS4&t=1541))
4. Cotton production estimated to be lower at 360 lakh bales - cropping pattern - cotton crop - ([34:57](https://youtube.com/watch?v=TrX3XNkjOS4&t=2097))
5. Scheme for revision (add-on) -  Ayushmaan Bharat - ([44:24](https://youtube.com/watch?v=TrX3XNkjOS4&t=2664))
6. Question for the Day – ([50:50](https://youtube.com/watch?v=TrX3XNkjOS4&t=3050))

#GST #USAplane #P8I #Indirecttaxes #IndiaIranrelation #JCPOA #cottoncrop #cottonexports #ayushmaanbharat #blacksoil #economicsurvey #RSBY

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Vaibhav Mishra of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
WORD: http://bit.ly/DNS-NOTES-30-04-21-WORD
PDF: http://bit.ly/DNS-NOTES-30-04-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. References  - ([01:17](https://youtube.com/watch?v=Cn9J41lKIlE&t=77))
2. Childhood vaccinations must not be delayed’ - ([06:13](https://youtube.com/watch?v=Cn9J41lKIlE&t=373))
3. China launches key module for its permanent space station - ([17:23](https://youtube.com/watch?v=Cn9J41lKIlE&t=1043))
4. QOD - ([18:46](https://youtube.com/watch?v=Cn9J41lKIlE&t=1126))

#UIP #universalimmunization #ICDS #ASHA #ANM #SPACESTATION 

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jaten Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►https://www.rauias.com/optional-session



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
https://bit.ly/DNS-NOTES-30-06-21-WORD
https://bit.ly/DNS-NOTES-30-06-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. One Nation one ration card scheme – Government schemes - ([00:44](https://youtube.com/watch?v=Wg2oWn5H3wE&t=44))
2. OPEC and India's oil imports– international relations – Indian economy - ([16:28](https://youtube.com/watch?v=Wg2oWn5H3wE&t=988))
3. Religious attitude in India– Indian society - ([24:46](https://youtube.com/watch?v=Wg2oWn5H3wE&t=1486))
4. Currency swap agreement (reference)– Indian economy - ([34:16](https://youtube.com/watch?v=Wg2oWn5H3wE&t=2056))
5. Question for the Day – ([35:19](https://youtube.com/watch?v=Wg2oWn5H3wE&t=2119))

#ONORC #PDS #OPEC #Indiaoilimport #religiousattitude #currencyswap #MigrationinIndia #censusofindia

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►https://www.rauias.com/optional-session



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-30-05-21-PDF
WORD - http://bit.ly/DNS-NOTES-30-05-21-WORD

►TODAY’S THE HINDU ANALYSIS
1. Chief Secretary of West Bengal – Indian Polity and governance - ([00:42](https://youtube.com/watch?v=rV_BiPz2QO8&t=42))
2. Indian Coast Guard – National Security – Security agencies - ([09:18](https://youtube.com/watch?v=rV_BiPz2QO8&t=558))
3. Citizenship Amendment Act, 2019 and powers under it – Indian polity and constitution -  ([17:51](https://youtube.com/watch?v=rV_BiPz2QO8&t=1071))
 4. Declining forest bird species in Western Himalaya – Environment and Ecology - ([26:51](https://youtube.com/watch?v=rV_BiPz2QO8&t=1611))
5. Scheme for revision (Swachh Bharat Abhiyaan) – ([33:25](https://youtube.com/watch?v=rV_BiPz2QO8&t=2005))
6. Question for the Day – ([43:46](https://youtube.com/watch?v=rV_BiPz2QO8&t=2626))

#ChiefSecretaryofBengal #chiefsecretary #WestBengal #PMModiinBengal #Stategovernance #indiancoastguards #nationalsecurity #exclusiveeconomiczones #sajag #westernHimalayas #biodiversity #Uttarakhand #himachalpradesh #swachhbharatabhiyaan #swachhbharatabhiyaan2.0 #swachhsarvekshan2021

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Pallavi Sarda, 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 


First Essay QIP Session (Open) - https://youtu.be/ekbFedX7LM4
GS Paper-1 - Answer Key - http://bit.ly/GS-I-Answer-Key-Prelims-2020 
y: http://bit.ly/GS-II-CSAT-Ans-Key-2020 DNS  



Video - https://youtu.be/KfmZdTpd3RA

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-31-03-2021-PDF
Word - http://bit.ly/DNS-NOTES-31-03-2021-WORD

►TODAY’S THE HINDU ANALYSIS
1. A road to progress – (Social Justice) - ([00:52](https://youtube.com/watch?v=MYm8DpR01o4&t=52))
2. Remoteness is no hindrance to academic excellence - (Social Justice) – ([14:16](https://youtube.com/watch?v=MYm8DpR01o4&t=856))
3. Jaishankar says India backs Afghan-Taliban dialogue - (International Relations) - ([25:48](https://youtube.com/watch?v=MYm8DpR01o4&t=1548)) 
4. Work with India on funding infra, Sitharaman urges NDB - (International Relations) - ([30:43](https://youtube.com/watch?v=MYm8DpR01o4&t=1843))
5. QOD – ([34:16](https://youtube.com/watch?v=MYm8DpR01o4&t=2056))
#women #feminization #agriculture #feminizationofagriculture #newdevelopmentbank #brics #bricsbank #Afghanistan #heartofasia #highereducation #education #socialjustice

https://www.rauias.com/daily-news-headlines/the-indian-express-edition-new-delhi-585/
/IAS - ELEARN 


▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
 ▪ Geography: https://youtu.be/l2fYquMkdoY


  



  

Presented by: Mrs. Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link for optional counselling session: Open Counselling Session (rauias.com)



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - http://bit.ly/DNS-NOTES-31-05-21-PDF
Word - http://bit.ly/DNS-NOTES-31-05-21-WORD

►TODAY’S THE HINDU ANALYSIS
1.  A veteran communist takes a last bow – (Society) –([02:06](https://youtube.com/watch?v=475eb-ct9-c&t=126))
2. Left Wing Extremism / Naxalism – (Mains Question discussion) – ([11:35](https://youtube.com/watch?v=475eb-ct9-c&t=695))
3. Commando training, more firepower for Kaziranga guards – (Environment) - ([28:09](https://youtube.com/watch?v=475eb-ct9-c&t=1689))
4. Sree Padmanabhaswamy Temple elephant dies – (Culture) – ([33:21](https://youtube.com/watch?v=475eb-ct9-c&t=2001))
5.  Accused denied anticipatory bail can be given protection’ – (Polity & Ethics) – ([35:20](https://youtube.com/watch?v=475eb-ct9-c&t=2120))
6. Question for the Day – ([37:29](https://youtube.com/watch?v=475eb-ct9-c&t=2249))
#naxalism #lwe #communism #rhino #kaziranga #Padmanabhaswamy #temple #bail

/IAS - ELEARN 
▪ eLearn - All our content is available on our learning platform - https://elearn.rauias.com 


►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1199/ 
 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►https://www.rauias.com/optional-session



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
https://bit.ly/DNS-NOTES-03-07-21-WORD
https://bit.ly/DNS-NOTES-03-07-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. Solar power and rural India – Indian economy- energy resources - ([00:42](https://youtube.com/watch?v=tM2sQDpYTWY&t=42))
2. 4000-year-old settlement found in Odisha – Indian history - ([21:29](https://youtube.com/watch?v=tM2sQDpYTWY&t=1289))
3. Digital divide in schools – Indian education system – education in India - ([25:43](https://youtube.com/watch?v=tM2sQDpYTWY&t=1543))
4. Brief bulletin – Indian economy - ([32:57](https://youtube.com/watch?v=tM2sQDpYTWY&t=1977))
5. Scheme of the Day – eNational agriculture Market – ([36:19](https://youtube.com/watch?v=tM2sQDpYTWY&t=2179))
5. Question for the Day – ([39:50](https://youtube.com/watch?v=tM2sQDpYTWY&t=2390))

#solarpowerinIndia #Balasore #odisha #excavationinodisha #digitaldivide #UDISEplus #exports #tradebalance #rajasthan #neweducationpolicy #vocationaltraining #eNAM  

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mrs. Pallavi Sarda of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►Link for optional counselling session: Open Counselling Session (rauias.com)



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian Economy Prelims Compass:
http://bit.ly/Prelims-Compass-Economy-2021
►Link for 2021 Polity & Governance Prelims Compass:
http://bit.ly/Prelims-Compass-Polity-2021
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
PDF - https://bit.ly/DNS-NOTES-05-07-2021-PDF
WORD - https://bit.ly/DNS-NOTES-05-07-2021-WORD



►TODAY’S THE HINDU ANALYSIS
1. How Chhattisgarh has stalled a historic judgment  – (Security)–([00:46](https://youtube.com/watch?v=BnxnxX7JgnE&t=46))
2. numeracy mission deadline pushed to 2027– (Social Justice) – ([08:15](https://youtube.com/watch?v=BnxnxX7JgnE&t=495))
3. Around 50% of the municipal schools not following the RTE Act mandate  – Mains Question discussion) - ([16:08](https://youtube.com/watch?v=BnxnxX7JgnE&t=968))
4. Question for the Day – ([27:19](https://youtube.com/watch?v=BnxnxX7JgnE&t=1639))
#rte #naxalism #samlajudam #nipunbharat

/IAS - ELEARN 
▪ eLearn - All our content is available on our learning platform - https://elearn.rauias.com 


►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1234/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►https://www.rauias.com/optional-session



►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registration-2021

►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021

►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-Survey2021

►DOWNLOAD DNS NOTES
http://bit.ly/DNS-NOTES-06-06-21-Word
http://bit.ly/DNS-NOTES-06-06-21-pdf

►TODAY’S THE HINDU ANALYSIS
1. First Human infection of H10N3 – Science and technology - ([00:42](https://youtube.com/watch?v=KZj14EpH0Pk&t=42))
2. Chinese enclave in Sri Lanka – National Security – international relation - ([07:56](https://youtube.com/watch?v=KZj14EpH0Pk&t=476))
3. Global Minimum Corporate Tax – Economy and international relation -  ([14:28](https://youtube.com/watch?v=KZj14EpH0Pk&t=868)) - https://www.youtube.com/watch?v=7iS3mwVSap8&t=1741s
4. Bose-Einstein Condensation – science and technology - ([18:29](https://youtube.com/watch?v=KZj14EpH0Pk&t=1109))
5. Scheme for revision (Soil Health Card) – ([25:34](https://youtube.com/watch?v=KZj14EpH0Pk&t=1534))

6. Question for the Day – ([31:05](https://youtube.com/watch?v=KZj14EpH0Pk&t=1865))

#ChinainSrilanka #H10N3virus #Colomboportcity #Globalminimumcorporatetax #BEC #boseeinsteincondesate #soilhealthcard #subatomicparticle #iitmadras #Birdflu

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-1150/

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjdLHipc0
▪ Geography: https://youtu.be/l2fYquMkdoY


  


Presented by: Mr. Jatin Bhardwaj of 
#Hindunewspaperanalysis #RauIASDNS #UPSC #Currentaffairs 
►https://www.rauias.com/optional-session

►Link to QIP Prelims 2021 Registration: http://bit.ly/QIP-Prelims-Registratio...
►Link for 2021 Indian History and Culture Prelims Compass: 
http://bit.ly/PrelimsCompassHistory2021
►Link for Budget and Economic Survey Prelims Compass: 
http://bit.ly/PrelimsCompass-Budget-S...

►DOWNLOAD DNS NOTES
https://bit.ly/DNS-NOTES-08-07-21-WORD
https://bit.ly/DNS-NOTES--8-07-21-PDF

►TODAY’S THE HINDU ANALYSIS
1. Challenging negative social norms– Indian society- women issues - ([01:04](https://youtube.com/watch?v=k2fZh0_KyrQ&t=64))
2. Delhi gets own Genome sequencing– Health system – Science and technology- (19::24)
3. Cabinet Rejig at Central Government– Indian Polity – constitution of India - ([29:24](https://youtube.com/watch?v=k2fZh0_KyrQ&t=1764))
4. Scheme of the Day (MUDRA)– Indian economy - ([36:24](https://youtube.com/watch?v=k2fZh0_KyrQ&t=2184))
5. Question for the Day – ([41:07](https://youtube.com/watch?v=k2fZh0_KyrQ&t=2467))

#socialreforms #sexualandreproductivehealth #righttohealth #indiansociety #womenissues #delhicurrentaffairs #genomesequencing #cabinetrejig #newcentralcabinet #cabinetreshuffle #MUDRAscheme #mudrabank

/IAS - ELEARN 



►Link - UPSC MAINS 2020 – ANALYSIS OF GS PAPERS

GS Paper 1 - http://bit.ly/UPSC-Mains-2020-GS-Paper1
GS Paper 2 - http://bit.ly/UPSC-Mains-2020-GS-Paper2
GS Paper 3 - http://bit.ly/UPSC-Mains-2020-GS-Paper3
GS Paper 4 - http://bit.ly/UPSC-Mains2020-GS-Paper4



Video - https://youtu.be/KfmZdTpd3RA


https://www.rauias.com/daily-news-hea...

 Video
▪ Ethics: https://youtu.be/WF-7FxMdvdI
▪ History: https://youtu.be/lEY9ozd-8FI
▪ Disaster Management: https://www.youtube.com/watch?v=tYgjd...
▪ Geography: https://youtu.be/l2fYquMkdoY


  


