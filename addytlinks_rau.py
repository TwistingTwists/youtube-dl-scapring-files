import re
import glob
import fire


def multireplace(string, replacements):
    """
    https://gist.github.com/bgusach/a967e0587d6e01e889fd1d776c5f3729
    Given a string and a replacement map, it returns the replaced string.
    :param str string: string to execute replacements on
    :param dict replacements: replacement dictionary {value to find: value to replace}
    :rtype: str
    """
    # Place longer ones first to keep shorter substrings from matching where the longer ones should take place
    # For instance given the replacements {'ab': 'AB', 'abc': 'ABC'} against the string 'hey abc', it should produce
    # 'hey ABC' and not 'hey ABc'
    substrs = sorted(replacements, key=len, reverse=True)

    # Create a big OR regex that matches any of the substrings to replace
    regexp = re.compile('|'.join(map(re.escape, substrs)))

    # For each match, look up the new string in the replacements
    return regexp.sub(lambda match: replacements[match.group(0)], string)


def get_replaced_text(wanted_file):
    # read the file and convert to SINGLE python string
    import re
    with open(wanted_file, 'r') as descriptionfile:
        gettext = descriptionfile.read()

    getTextReplaced = multireplace(gettext, {"\n": "~~"})
    return getTextReplaced


def parse_topics_timestamps(wanted_file):
    getTextReplaced = get_replaced_text(wanted_file)
    r = "(\d\.)(.*?)\((\d{1,}:\d{1,})\)"  # for rau ias
    # r = "(\d{1,2})(.*?)-.*?(\d{1,2}:\d{1,2})" # for shankarias

    all_timestamped_topics = re.findall(r, getTextReplaced)
    return all_timestamped_topics


def getTimeStampedTopic(all_timestamped_topics, yt_link):
    # add timestamps
    # convert 00:31
    # to [00:31](https://youtube.com/watch?v=THnJG3j1qt0&t=31)

    NewAllTimeStamped = []
    for [num, topic, time_string] in all_timestamped_topics:
        timeSeconds = sum(x * int(t)
                          for x, t in zip([60, 1], time_string.split(":")))
        # time = sum(x * int(t)for x, t in zip([3600, 60, 1], time_string.split(":"))) # for 00:34:24 format
        NewAllTimeStamped.append(
            [num, topic, "["+time_string+"]("+yt_link+"&t="+str(timeSeconds)+")"])
    return NewAllTimeStamped


def addlinksToFile(wanted_file):
    getTextReplaced = get_replaced_text(wanted_file)

    # get youtube link
    youtubeLink = wanted_file[-23:-12]
    pre = "https://youtube.com/watch?v="
    final_yt_link = "".join([pre, youtubeLink])

    # get time stamps and topics as [number, topic, timestamp]
    all_timestamped_topics = parse_topics_timestamps(wanted_file)

    # get 00:31 as [00:31](https://youtube.com/watch?v=THnJG3j1qt0&t=31)
    newTimestamped_topics = getTimeStampedTopic(
        all_timestamped_topics, final_yt_link)

    # replace timestamped string back in text
    dictReplace = {}
    for old, new in zip(all_timestamped_topics, newTimestamped_topics):
        dictReplace.update({old[2]: new[2]})

    hyperlinkedText = multireplace(getTextReplaced, dictReplace)
    print("done replacing!!")

    # clean up junk material in rau dns description
    cleaningDict = {
        "►UPSC PRELIMS 2020 COMPASS MAGAZINES ON AMAZON": "",
        "1. Polity - http://bit.ly/RauIAS-Prelims-Compass-Pol-Gov-20~~2. S&T - http://bit.ly/RauIAS-Prelims-Compass-Sc-n-Tech-2020~~3. History - http://bit.ly/RauIAS-Prelims-Compass-History-Culture-2020~~4. Economy - http://bit.ly/Prelims-Compass-Economy~~5. Environment, Ecology & Biodiversity - https://bit.ly/RauIAS-Prelims-Compass-Env-Eco-20~~6. Government Schemes – http://bit.ly/RauIAS-Prelims-Compass-Govt-Sch-20": "",
        "► CONNECT WITH Rau’s IAS~~▪ Facebook : https://www.facebook.com/RausIAS/~~▪ Instagram : https://www.instagram.com/rausias/": "",
        "►RAU’S IAS ONLINE FOR UPSC/IAS - ELEARN~~▪ eLearn - Not all our learning content is on Youtube. All our content is available on our learning platform - https://elearn.rauias.com~~▪ Intro Video - Elearn has been built specifically for UPSC aspirants to meet the learning and testing requirement of UPSC Civil Services Exam – https://www.youtube.com/watch?v=THnJG3j1qt0~~▪ Trending discussion on E-Learn: https://elearn.rauias.com/d/covid-19-and-its-impact-on-globalization/5e7f10077737d40a3f4eac5d/": "",
        "► CONNECT WITH US ": "",
        "► CONNECT WITH RAU’S IAS": "",
        "▪ Facebook : https://www.facebook.com/RausIAS/": "",
        "▪ Instagram : https://www.instagram.com/rausias/Presented": "",
        "Our Facebook Page: https://www.facebook.com/RausIAS/": "",
        "Connect with FOCUS Team: https://www.facebook.com/groups/focus.rauias/Presented": "",
        "Rau’s IAS Study Circle": "",
        "Click the Link for buying Mains Compass Economic Development -  http://bit.ly/Mains-Compass-Economic-Development": "",
        "Click the Link for buying Mains Compass Polity & Governance -": "",
        "http://bit.ly/Mains-Compass-Polity-and-Governance": "",

        "Click the Link for buying Mains Compass International Relations and Internal Security": "",
        "http://bit.ly/Mains-Compass-International-Relations-and-Internal-Security": "",

        "https://www.rauias.com/daily-news-headlines/the-hindu-edition-new-delhi-778/": ""





    }
    hyperlinkedText_cleaned = multireplace(hyperlinkedText, cleaningDict)

    # replace back ~~ with \n (for easier regex, done at the beginning)
    replaceBackDict = {"~~": "\n"}
    cleanedLinkedreplaced = multireplace(
        hyperlinkedText_cleaned, replaceBackDict)

    # write a new file with hyperlinked contents
    newFile = wanted_file[:-12]+"_linked.md"
    with open(newFile, 'w') as linkedfile:
        linkedfile.write(cleanedLinkedreplaced)
    print("written new file")  # w, newFile)


def addLinkstoDescriptions(pattern="*.description"):
    print(pattern)
    filenames = []
    for wanted_file in glob.glob(pattern):
        filenames.append(wanted_file)
        print(len(filenames), wanted_file)
        addlinksToFile(wanted_file)


if __name__ == "__main__":
    fire.Fire(addLinkstoDescriptions)
